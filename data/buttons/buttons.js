// Tableau de menu (menu de droite)
// Dans ce tableau d'objet nous avons deux objets qui sont eux aussi des tableaux d'objets
// les objets parcelle et pf représentent les options
// Dans ces options nous avons différentes fonctions qui peuvent être identique (Nom, icone) avec un fonctionnement différent
// Ex: parcelle_sol est le nom de la fonction dans le fichier parcellaire.js et Sol est le texte a afficher au survol du bouton
// Il faut toujours nommer les fonctions de ce type option_nomFonction
// Pour ajouter une image c'est juste le nom de la fonction
var BUTTONS = {
	parcelle: {
		parcelle_successions: 'Successions',
		parcelle_sol: 'Sol',
		parcelle_effluent: 'Effluents',
		parcelle_semis: 'Semis',
		parcelle_adventices: 'Adventices',
		parcelle_travail_sol: 'Travail du sol',
		parcelle_recoltes: 'Récoltes',
		parcelle_interculture: 'Interculture',
		parcelle_gest_prairies: 'Gestion des prairies'
	},
	pf: {
		parcelle_successions: 'Successions', // Permet de revenir sur les successions de la parcelle
		parcelle_sol: 'Sol',
		pf_adventices: 'Adventices',
		pf_recoltes: 'Récoltes',
	}
}