// Initialisation du plugin
app.plg.plugin = {
	// Initialisation du plugin parcellaire
	init: function() {
		var that = this

		// on affiche le parcellaire
		app.layersHide[this.name()] = false

		// inclusion du fichier de scripts au DOM
		var script = document.createElement('script')
		$(script).attr('src', ('{0}/buttons/buttons.js').format(app.dataDirectory))
		$('body').append(script)

		// chargement des patterns du parcellaire
		var parcelles = []
		var pr = app.modules.queries.get('assolement/{0}'.format(app.currentYear))
		pr.done(function(data) {
			data.DATA.forEach(function(d) {
				parcelles[d.Nom_Court] = d
			})
			that.drawLayer(parcelles)
		})
	},
	// Fonction de retour du nom du plugin
	name: function() {
		return 'parcellaire'
	},
	// Indique si le plugin possède un bouton
	button: function() {
		return false
	},
	// Construction de la couche
	drawLayer: function(parcelles) {
		var that = this

		var opacity = 0.8

		// si la couche n'existe pas, on créer la couche
		if (app.shapeFiles[that.name()] === undefined) {
			var shapeFilePath = ('{0}/{1}/couches/{2}/{3}_{4}').format(app.shapeFilesDirectory, that.name(), app.currentYear, that.name(), app.currentYear)
			if (app.currentYear !== new Date().getFullYear()) {
				var parcellaire = PARCELLAIRES.find(function(e) {
					return (e == app.currentYear)
				})
				shapeFilePath = ('{0}/{1}/couches/{2}/{3}_{4}').format(app.shapeFilesDirectory, that.name(), parcellaire, that.name(), app.currentYear)
			}

			var style = app.shapeStyle
			style.fillOpacity = 0.65

			app.shapeFiles[that.name()] = new L.Shapefile(shapeFilePath, {
				style: style,

				// Parcourtde tout les elements de la couche
				onEachFeature: function(feature, layer) {
					if (feature.geometry) {
						var pattern = null

						// recuperation de la parcelle courrante
						var parc = parcelles[feature.properties['NOM_COURT']]

						// si la parcelle existe, on lui attribut un pattern
						if (parc !== undefined) {
							if (parc.Trame === 'LigneH') {
								pattern = new L.StripePattern({
									color: 'rgb({0})'.format(parc.Couleur_A),
									fillColor: 'rgb({0})'.format(parc.Couleur_A),
									fillOpacity: opacity
								})
								pattern.addTo(app.map)
							}

							if (parc.Trame === 'Oblique') {
								pattern = new L.StripePattern({
									color: 'rgb({0})'.format(parc.Couleur_A),
									fillColor: 'rgb({0})'.format(parc.Couleur_A),
									angle: 90,
									fillOpacity: opacity
								})
								pattern.addTo(app.map)
							}

							if (parc.Trame === 'Croix') {
								var shape = new L.PatternPath({
									d: 'M10,10 l0,10 M5,15 l10,0',
									fill: true,
									color: 'rgb({0})'.format(parc.Couleur_A),
									fillColor: 'rgb({0})'.format(parc.Couleur_A),
									fillOpacity: opacity
								})

								var pattern = new L.Pattern({
									width: 20, 
									height: 25
								})
								pattern.addShape(shape)
								pattern.addTo(app.map)
							}

							if (parc.Trame === 'Cadrillage') {
								var shape = new L.PatternRect({
									width: 10,
									height: 10,
									rx: 0,
									ry: 0,
									fill: true,
									color: 'rgb({0})'.format(parc.Couleur_A),
									fillColor: 'rgb({0})'.format(parc.Couleur_A),
									fillOpacity: opacity
								})

								var pattern = new L.Pattern({
									width: 10, 
									height: 10
								})
								pattern.addShape(shape)
								pattern.addTo(app.map)
							}

							if (parc.Trame === 'Cercle') {
								var shape = new L.PatternCircle({
									x: 8,
									y: 8,
									radius: 5,
									fill: true,
									color: 'rgb({0})'.format(parc.Couleur_A),
									fillColor: 'rgb({0})'.format(parc.Couleur_A),
									fillOpacity: opacity
								})

								pattern = new L.Pattern({
									width: 25, 
									height: 25
								})
								pattern.addShape(shape)
								pattern.addTo(app.map)
							}

							// Mise a jour du pattern et de la couleur / opacite de l'element
							layer.setStyle(
								{
									fillPattern: pattern,
									fillColor: 'rgb({0})'.format((parc.Couleur_A.length === 0) ? '236,240,241' : parc.Couleur_A),
									fillOpacity: opacity
								}
							)

							// Changement d'opacite au passage de la souris
							$(layer).hover(function(e) {
								e.target.setStyle({ fillOpacity: 0.65 })
							}, function(e) {
								if (app.currentLayer !== e.target) {
									e.target.setStyle({ fillOpacity: 0.8 })
								}
							})
						} else {
							layer.setStyle({ fillOpacity: 0.2 })
							$(layer).hover(function(e) {
								e.target.setStyle({ fillOpacity: 0.65 })
							}, function(e) {
								if (app.currentLayer !== e.target) {
									e.target.setStyle({ fillOpacity: 0.2 })
								}
							})
						}

						// Evenement de clic sur l'element
						layer.addEventListener('mousedown', function(e) {							
							if (app.currentLayer != undefined) {
								app.currentLayer.setStyle({ fillOpacity: 0.8 })
							}

							e.target.setStyle({ fillOpacity: 0.65 })
							app.currentLayer = e.target

							// Recuperation des informations de l'element
							app.nom_court = feature.properties['NOM_COURT']

							var nom = feature.properties['PARCELLE']
							var pr = app.modules.queries.get('infos/{0}'.format(feature.properties['NOM_COURT']))
							pr.done(function(data)  {
								var surface = parseFloat(data.DATA.surface)

								$('#parcelle').attr('data', feature.properties['NOM_COURT'])
								if (surface != undefined) {
									$('#parcelle').text('{0} ({1}Ha)'.format(decodeURIComponent(escape(nom)), surface.toFixed(2))).fadeIn('slow')
								} else {
									$('#parcelle').text('{0}'.format(nom)).fadeIn('slow')
								}
								app.modules.actions.displayData()

								$('#systeme').removeClass('systeme')
								$('#systeme').text("{0} - {1}".format(data.DATA.systeme, data.DATA.SDC))

								// Recuperation du plugin
								var plugin = app.pluginsLoaded[that.name()]

								$('#openTab').fadeOut('slow')

								// Affichage des informations
								plugin.parcelle_successions()
								app.paramsParcelle = {
									nom: nom,
									surface: surface
								}
								plugin.createButtons()

								if (app.currentYear == new Date().getFullYear()) {
									app.parcelle = {
										nom_court: app.nom_court,
										nom: nom,
										surface: surface
									}
								}
							})

							return false;
						})

						// Evenemlent du passage de la souris
						layer.addEventListener('mouseover', function(e) {
							app.modules.actions.displayPopup()
							var p = document.createElement('p')
							$(p).text(("{0}").format(decodeURIComponent(escape((feature.properties['PARCELLE'] != undefined) ? feature.properties['PARCELLE'] : feature.properties['COMMENT']))))
							$('#infosPa').append(p)
							$('#modal').modal('open');
						})					
					}
				}
			})
			// Ajouts de la couche a la carte
			app.shapeFiles[that.name()].addTo(app.map)
		} else {
			// Suppresion de la couche sur la carte
			app.map.removeLayer(app.shapeFiles[that.name()])
		}
	},
	// Fontion de creation des boutons
	createButtons: function(params) {
		var that = this

		if ($('#carouselContainer').is(':visible')) {
			$('#carouselContainer').fadeOut('slow')
			$('#carouselContainer *').remove()
		}

		$('#buttons ul').remove()

		var ul = document.createElement('ul')

		// Creation des boutons
		$.each(BUTTONS[app.option], function(k, v) {
			var li = document.createElement('li')
			var img = document.createElement('img')

			if (k === 'successions') {
				$(li).attr('class', 'tooltipped active')
			} else {
				$(li).attr('class', 'tooltipped')
			}
			$(li).attr('data-position', 'left')
			$(li).attr('data-delay', '50')
			$(li).attr('data-tooltip', v)
			$(li).attr('id', 'btn-{0}'.format(k))

			// Ajouts d'un evenement de clic sur les boutons
			li.addEventListener('click', function() {
				if ($('#carouselContainer').is(':visible')) {
					$('#carouselContainer').fadeOut('slow')
					$('#carouselContainer *').remove()
				}

				if (k == 'successions') {
					if ($('#parcelle').text().indexOf('~') !== -1) {
						var text = $('#parcelle').text().split('~')[0]
						$('#parcelle').text('{0}'.format(text))
					}
				}

				app.map.removeLayer(app.shapeFiles[that.name()])
				app.shapeFiles[that.name()] = undefined
				that.init()

				that[k]()
			})

			// Attribution de l'image au bouton
			var image = k.split('_')
			image.shift()
			image = image.join('_')
			$(img).attr('src', '{0}/buttons/img/{1}.png'.format(app.dataDirectory, image))
			$(img).attr('alt', v)

			$(li).append(img)
			$(ul).append(li)
		}) 

		$('#buttons').append(ul)
		$('.tooltipped').tooltip({delay: 50})
	},
	// Fonction d'affichage des interventions
	interventions: function() {
		$('#interventions *').remove()

		var that = this

		var pr = app.modules.queries.get('interventions/{0}/{1}'.format(app.nom_court, app.currentYear))
		if (arguments.length > 0) {
			pr = app.modules.queries.get('interventions/{0}/{1}'.format(arguments[0], app.currentYear))
		}
		pr.done(function(data) {
			if (data.DATA == 'NOT_FOUND') {
				Materialize.toast('Aucunes interventions trouvées pour cette parcelle.', 3000)
			} else {
				if (app.paramsParcelle.surface !== undefined) {
					app.modules.actions.displayData('{0} ({1}Ha)'.format(app.paramsParcelle.nom, app.paramsParcelle.surface.toFixed(2)))
				} else {
					app.modules.actions.displayData('{0}'.format(app.paramsParcelle.nom))
				}

				var ul = document.createElement('ul')
				$(ul).attr('class', 'collapsible')
				$(ul).attr('data-collapsible', 'accordion')

				var li = document.createElement('li')
				var divHeader = document.createElement('div')
				$(divHeader).attr('class', 'collapsible-header')
				$(divHeader).text('Interventions')
				var divBody = document.createElement('div')
				$(divBody).attr('class', 'collapsible-body')
				var table = document.createElement('table')
				$(table).attr('class', 'bordered striped highlight')
				var tbody = document.createElement('tbody')

				var i = 0
				data.DATA.forEach(function(v, k) {
					if (v.DateI.includes(app.currentYear)) {
						var tr = document.createElement('tr')
						$(tr).attr('style', 'background: rgb({0}); cursor: pointer;'.format(v.Couleur))

						var date_intervention = document.createElement('td')
						var date = (new Date(v.DateI).toLocaleString()).split(' ')[0]
						$(date_intervention).text(date)
						$(tr).append(date_intervention)

						var nom_operation = document.createElement('td')
						$(nom_operation).text(v.Nom_Operation)
						$(tr).append(nom_operation)

						tr.addEventListener('click', function() {
							$('#recap *').remove()
							var h5 = document.createElement('h5')
							$(h5).text('{0} - {1}'.format(v.Nom_Operation, date))
							$('#recap').append(h5)

							var tableRecap = document.createElement('table')
							$(tableRecap).attr('class', 'bordered striped centered highlight')

							var tbodyRecap = document.createElement('tbody')

							var trNom_Materiel = document.createElement('tr')
							var td_materiel = document.createElement('td')
							$(td_materiel).text('Materiel')
							$(trNom_Materiel).append(td_materiel)

							var tdNom_Materiel = document.createElement('td')
							$(tdNom_Materiel).text(v.Nom_Materiel)
							$(trNom_Materiel).append(tdNom_Materiel)
							$(tbodyRecap).append(trNom_Materiel)

							var trNom_Tracteur = document.createElement('tr')
							var td_materiel = document.createElement('td')
							$(td_materiel).text('Tracteur')
							$(trNom_Tracteur).append(td_materiel)
							var tdNom_Tracteur = document.createElement('td')
							$(tdNom_Tracteur).text(v.Nom_Tracteur)
							$(trNom_Tracteur).append(tdNom_Tracteur)
							$(tbodyRecap).append(trNom_Tracteur)

							var trProfondeur = document.createElement('tr')
							var td_materiel = document.createElement('td')
							$(td_materiel).text('Profondeur (cm)')
							$(trProfondeur).append(td_materiel)

							var tdProfondeur = document.createElement('td')
							$(tdProfondeur).text(v.Profondeur)
							$(trProfondeur).append(tdProfondeur)
							$(tbodyRecap).append(trProfondeur)

							var trSurface_W = document.createElement('tr')
							var td_surface = document.createElement('td')
							$(td_surface).text('Surface travaillée / Surface totale')
							$(trSurface_W).append(td_surface)

							var tdSurface_W = document.createElement('td')
							if (parseInt(v.Surface_W) == 100) {
								$(tdSurface_W).text('{0} Ha / {1} Ha ({2} %)'.format(parseInt(v.surface).toFixed(0), parseInt(v.surface).toFixed(0), parseInt(v.Surface_W).toFixed(0)))
							} else {
								var surface_W = ((parseInt(v.Surface_W) * parseInt(v.surface)) / 100)
								$(tdSurface_W).text('{0} Ha / {1} Ha ({2}%)'.format(surface_W.toFixed(2), parseInt(v.surface).toFixed(0), parseInt(v.Surface_W).toFixed(0)))
							}
							$(trSurface_W).append(tdSurface_W)
							$(tbodyRecap).append(trSurface_W)

							if (v.commentaire != undefined) {
								var trCommentaires = document.createElement('tr')

								var tdCommentaires = document.createElement('td')
								$(tdCommentaires).attr('colspan', 2)

								var commentaires = document.createElement('textarea')
								$(commentaires).text('{0}'.format(v.commentaire))
								$(commentaires).attr('style', 'min-height: 100px;')
								$(commentaires).attr('readonly', true)
								$(tdCommentaires).append(commentaires)
								$(trCommentaires).append(tdCommentaires)

								$(tbodyRecap).append(trCommentaires)
							}

							$(tableRecap).append(tbodyRecap)
							$('#recap').append(tableRecap)
							$('#modalRecap').modal('open')
						})

						$(tbody).append(tr)
						i = (i + 1)
					}
				})
				if (i > 0) {
					data.DATA.splice(0, i)
					$(table).append(tbody)
					$(divBody).append(table)
					$(li).append(divHeader)
					$(li).append(divBody)
					$(ul).append(li)
				}

				var liHidden = document.createElement('li')
				var divHeaderHidden = document.createElement('div')
				$(divHeaderHidden).attr('class', 'collapsible-header')
				if (i == 0) {
					$(divHeaderHidden).text('Interventions')
				} else {
					var iHidden = document.createElement('i')
					$(iHidden).attr('class', 'fa fa-arrow-down')
					$(iHidden).attr('aria-hidden', 'true')
					$(divHeaderHidden).append(iHidden)
				}
				var divBodyHidden = document.createElement('div')
				$(divBodyHidden).attr('class', 'collapsible-body')
				var tableHidden = document.createElement('table')
				$(tableHidden).attr('class', 'bordered striped highlight')
				var tbodyHidden = document.createElement('tbody')
				data.DATA.forEach(function(v) {
					var tr = document.createElement('tr')
					$(tr).attr('style', 'background: rgb({0}); cursor: pointer;'.format(v.Couleur))

					var date_intervention = document.createElement('td')
					var date = (new Date(v.DateI).toLocaleString()).split(' ')[0]
					$(date_intervention).text(date)
					$(tr).append(date_intervention)

					var nom_operation = document.createElement('td')
					$(nom_operation).text(v.Nom_Operation)
					$(tr).append(nom_operation)

					tr.addEventListener('click', function() {
							$('#recap *').remove()
							var h5 = document.createElement('h5')
							$(h5).text('{0} - {1}'.format(v.Nom_Operation, date))
							$('#recap').append(h5)

							var tableRecap = document.createElement('table')
							$(tableRecap).attr('class', 'bordered striped centered highlight')

							var tbodyRecap = document.createElement('tbody')

							var trNom_Materiel = document.createElement('tr')
							var td_materiel = document.createElement('td')
							$(td_materiel).text('Materiel')
							$(trNom_Materiel).append(td_materiel)
							var tdNom_Materiel = document.createElement('td')
							$(tdNom_Materiel).text(v.Nom_Materiel)
							$(trNom_Materiel).append(tdNom_Materiel)
							$(tbodyRecap).append(trNom_Materiel)

							var trNom_Tracteur = document.createElement('tr')
							var td_materiel = document.createElement('td')
							$(td_materiel).text('Tracteur')
							$(trNom_Tracteur).append(td_materiel)
							var tdNom_Tracteur = document.createElement('td')
							$(tdNom_Tracteur).text(v.Nom_Tracteur)
							$(trNom_Tracteur).append(tdNom_Tracteur)
							$(tbodyRecap).append(trNom_Tracteur)

							var trProfondeur = document.createElement('tr')
							var td_materiel = document.createElement('td')
							$(td_materiel).text('Profondeur (cm)')
							$(trProfondeur).append(td_materiel)
							var tdProfondeur = document.createElement('td')
							$(tdProfondeur).text(v.Profondeur)
							$(trProfondeur).append(tdProfondeur)
							$(tbodyRecap).append(trProfondeur)

							var trSurface_W = document.createElement('tr')
							var td_surface = document.createElement('td')
							$(td_surface).text('Surface travaillée / Surface totale')
							$(trSurface_W).append(td_surface)

							var tdSurface_W = document.createElement('td')
							if (parseInt(v.Surface_W) == 100) {
								$(tdSurface_W).text('{0} Ha / {1} Ha ({2} %)'.format(parseInt(v.surface).toFixed(0), parseInt(v.surface).toFixed(0), parseInt(v.Surface_W).toFixed(0)))
							} else {
								var surface_W = ((parseInt(v.Surface_W) * parseInt(v.surface)) / 100)
								$(tdSurface_W).text('{0} Ha / {1} Ha ({2}%)'.format(surface_W.toFixed(2), parseInt(v.surface).toFixed(0), parseInt(v.Surface_W).toFixed(0)))
							}
							$(trSurface_W).append(tdSurface_W)
							$(tbodyRecap).append(trSurface_W)

							if (v.commentaire != undefined) {
								var trCommentaires = document.createElement('tr')

								var tdCommentaires = document.createElement('td')
								$(tdCommentaires).attr('colspan', 2)

								var commentaires = document.createElement('textarea')
								$(commentaires).text('{0}'.format(v.commentaire))
								$(commentaires).attr('style', 'min-height: 100px;')
								$(commentaires).attr('readonly', true)
								$(tdCommentaires).append(commentaires)
								$(trCommentaires).append(tdCommentaires)

								$(tbodyRecap).append(trCommentaires)
							}

							$(tableRecap).append(tbodyRecap)
							$('#recap').append(tableRecap)
							$('#modalRecap').modal('open')
						})

					$(tbodyHidden).append(tr)
				})
				$(tableHidden).append(tbodyHidden)
				$(divBodyHidden).append(tableHidden)
				$(liHidden).append(divHeaderHidden)
				$(liHidden).append(divBodyHidden)
				$(ul).append(liHidden)
				
				$(ul).collapsible()

				$(ul).collapsible({
					onOpen: function(el) {
						var i = el.find('i')
						i.removeClass('fa-arrow-down')
						i.addClass('fa-arrow-up')
					},
					onClose: function(el) {
						var i = el.find('i')
						i.removeClass('fa-arrow-up')
						i.addClass('fa-arrow-down')
					}
				})

				$('#interventions').append(ul)

				$('.collapsible').collapsible('open', 0)
			}
		})	
	},
	// Fonction d'affichage des succession
	parcelle_successions: function() {
		var that = this

		this.active('parcelle_successions')

		this.photos()

		$('#middle *').remove()

		var dataset = []
		var datas = []
		var pr = app.modules.queries.get('sol/{0}/{1}'.format(app.nom_court, app.currentYear))
		if (arguments.length > 0) {
			pr = app.modules.queries.get('sol/{0}/{1}/{2}'.format(app.nom_court, arguments[0], app.currentYear))
		}
		pr.done(function(data) {			
			if (data.DATA !== 'NOT_FOUND') {
				var top = document.createElement('div')
				$(top).attr('class', 'top')
					
				var pr = app.modules.queries.get('filiations/{0}/{1}'.format(app.nom_court, app.currentYear))
				pr.done(function(data) {
					if (data.DATA != 'NOT_FOUND') {
						var filiations = document.createElement('ul')
						$(filiations).attr('class', 'filiations')

						$.each(data.DATA, function(k, v) {
							var ulParent = document.createElement('ul')
							$(ulParent).attr('id', 'filiations')

							var inputParent = document.createElement('input')
							$(inputParent).attr('type', 'radio')
							$(inputParent).attr('name', 'filiations')
							$(inputParent).attr('class', 'with-gap')
							if (v.parent.gparent != undefined) {
								$(inputParent).attr('id', '{0}_{1}'.format(v.parent.NC_P, v.parent.gparent.NC_GP))
							} else {
								$(inputParent).attr('id', v.parent.NC_P)
							}
							$(ulParent).append(inputParent)

							var labelParent = document.createElement('label')
							$(labelParent).attr('for', v.parent.NC_P)
							$(labelParent).text('{0} ({1} - {2})'.format(v.parent.Parent, v.parent.SYST_P, v.parent.DateD))
							labelParent.addEventListener('click', function() {
								var radio = this
								var nc = radio.getAttribute('for')
								that.parcelle_successions(nc)
								var text = $('#parcelle').text().split('~')[0]
								$('#parcelle').text('{0} ~ {1}'.format(text, nc))
							})
							$(ulParent).append(labelParent)

							var ulGParent = document.createElement('ul')
							$(ulGParent).attr('style', 'margin-left: 20px;')

							if (v.parent.gparent != undefined) {
								var liGP = document.createElement('li')

								var inputGParent = document.createElement('input')
								$(inputGParent).attr('type', 'radio')
								$(inputGParent).attr('name', 'filiation')
								$(inputGParent).attr('class', 'with-gap')
								$(inputGParent).attr('id', v.parent.gparent.NC_GP)
								$(liGP).append(inputGParent)

								var labelGParent = document.createElement('label')
								$(labelGParent).attr('for', v.parent.gparent.NC_GP)
								$(labelGParent).text('{0} ({1} - {2})'.format(v.parent.gparent.GParent, v.parent.gparent.SYST_GP, v.parent.gparent.DateD))
								labelGParent.addEventListener('click', function() {
									var radio = this
									var nc = radio.getAttribute('for')
									that.parcelle_successions(nc)
									var text = $('#parcelle').text().split('~')[0]
									$('#parcelle').text('{0} ~ {1}'.format(text, nc))
								})
								$(liGP).append(labelGParent)

								if (v.parent.gparent.agparent != undefined) {
									var ulAGParent = document.createElement('ul')
									$(ulAGParent).attr('style', 'margin-left: 20px;')

									var liAGP = document.createElement('li')

									var inputAGParent = document.createElement('input')
									$(inputAGParent).attr('type', 'radio')
									$(inputAGParent).attr('name', 'filiation')
									$(inputAGParent).attr('class', 'with-gap')
									$(inputAGParent).attr('id', v.parent.gparent.agparent.NC_AGP)
									$(liAGP).append(inputAGParent)

									var labelAGParent = document.createElement('label')
									$(labelAGParent).attr('for', v.parent.gparent.agparent.NC_AGP)
									$(labelAGParent).text('{0} ({1} - {2})'.format(v.parent.gparent.agparent.AGParent, v.parent.gparent.agparent.SYST_AGP, v.parent.gparent.agparent.DateD))
									labelAGParent.addEventListener('click', function() {
										var radio = this
										var nc = radio.getAttribute('for')
										that.parcelle_successions(nc)
										var text = $('#parcelle').text().split('~')[0]
										$('#parcelle').text('{0} ~ {1}'.format(text, nc))
									})
				
									$(liAGP).append(labelAGParent)
									$(ulAGParent).append(liAGP)
									$(liGP).append(ulAGParent)
								}

								$(ulGParent).append(liGP)
								$(ulParent).append(ulGParent)
							}

							$(filiations).append(ulParent)
							$(top).append(filiations)

							if (currentParent != undefined) {
								if (currentParent.parentP.length > 0) {
									if (document.getElementById(currentParent.parentP) !== null) {
										document.getElementById(currentParent.parentP).checked = true
									}
									if (currentParent.gparent.length != "") {
										if (document.getElementById(currentParent.gparent) !== null) {
											document.getElementById(currentParent.gparent).checked = true
											document.getElementById(currentParent.gparent).checked = true
										}
									}
								}
							}
						})
					}			
				})
				
				var details = document.createElement('div')
				$(details).attr('class', 'details')

				var divSelect = document.createElement('div')
				$(divSelect).attr('class', 'input-field col s12')

				var selectOptions = document.createElement('select')
				$(selectOptions).attr('style', 'display: block;')
				$(selectOptions).attr('id', 'optionSelection')
				$(selectOptions).change(function(){
					app.option = $(this).children('option:selected').val()
					that.createButtons()
				});

				var optionParecelle = document.createElement('option')
				$(optionParecelle).attr('value', 'parcelle')
				$(optionParecelle).attr('id', 'parcelle')
				$(optionParecelle).text('Parcelle')
				$(selectOptions).append(optionParecelle)

				var pr = app.modules.queries.get('parcelle/point_ferti/{0}'.format(app.nom_court))
				pr.done(function(data) {
					if (data.DATA != 'NOT_FOUND') {
						var optionPF = document.createElement('option')
						$(optionPF).attr('value', 'pf')
						$(optionPF).attr('id', 'pf')
						$(optionPF).text('Point ferti.')
						$(selectOptions).append(optionPF)


						$(selectOptions).val(app.option)
					} else {
						$(selectOptions).val(app.option)
					}
				})
				
				$(divSelect).append(selectOptions)
				$(details).append(divSelect)


				$(top).append(details)

				$('#middle').append(top)

				$('#middle section').remove()
				var graphRotation = document.createElement('section')
				$(graphRotation).attr('class', 'graphRotation')
				$(graphRotation).attr('id', 'graphRotation')
				$('#middle').append(graphRotation)

				if (data.DATA.assolement[data.DATA.assolement.length - 1].parentP != undefined) {
					var currentParent = data.DATA.assolement[data.DATA.assolement.length - 1]
					delete data.DATA.assolement[data.DATA.assolement.length - 1]
				}
				console.log(data.DATA.assolement)
				// Creation du graphique de succession
				$.each(data.DATA.assolement, function(k, v) {
					if (v != undefined) {
						dataset.push({
							'label': v.An_Rec,
							'P1': (v.P1 !== null) ? 1 : 0,
							'P2': (v.P2 !== null) ? 1 : 0,
							'P3': (v.P3 !== null) ? 1 : 0,
							'P4': (v.P4 !== null) ? 1 : 0,
							'P5': (v.P5 !== null) ? 1 : 0,
							'P6': (v.P6 !== null) ? 1 : 0,
							'I1': (v.I1 !== null) ? 1 : 0,
							'I2': (v.I2 !== null) ? 1 : 0,
							'S1': (v.S1 !== null) ? 1 : 0,
							'S2': (v.S2 !== null) ? 1 : 0
						})
						datas.push(v)
					}
				})

				var culturesInfos = [];
				$.each(data.DATA.cultures, function(k, v) {
					if (v != undefined) {
						culturesInfos[k] = v
					}
				})

				var margin = {
					top: 20, 
					right: 40, 
					bottom: 30, 
					left: 40
				}

				$('#graphRotation').width($('#middle').width() + (datas.length * ((margin.left + margin.right))))

				var width = ((parseInt(d3.select('section').style('width'), 10) - margin.left) - margin.right)
				var height = ((parseInt(d3.select('section').style('height'), 10) - margin.top) - margin.bottom)


				var x = d3.scale.ordinal().rangeRoundBands([0, width], 0.1)
				var y = d3.scale.linear().rangeRound([height, 0])

				var colorRange = d3.scale.category20()
				var color = d3.scale.ordinal().range(colorRange.range())

				var xAxis = d3.svg.axis().scale(x).orient('bottom')

				$('#middle').scrollLeft(width)
				$('#middle').scrollTop(height)

				var svg = d3.select('section').append('svg')
				.attr('width', ((width + margin.left) + margin.right))
				.attr('height', ((height + margin.top) + margin.bottom))
				.attr("preserveAspectRatio", "xMinYMin meet")
				.attr("viewBox", "0, 0, {0}, {1}".format(((width + margin.left) + margin.right), ((height + margin.top) + margin.bottom)))
				.append('g')
				.attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')

				var divTooltip = d3.select('section').append('div').attr('class', 'toolTip')

				color.domain(d3.keys(dataset[0]).filter(function(key) {
					return (key !== 'label')
				}))

				dataset.forEach(function(d) {
					var y0 = 0
					d.values = color.domain().map(function(name) { 
						return {
							name: name, 
							y0: y0, 
							y1: (y0 += +d[name])
						}
					})
					d.total = d.values[d.values.length - 1].y1
				})

				x.domain(dataset.map(function(d) { 
					return (d.label)
				}))
				y.domain([0, d3.max(dataset, function(d) { 
					return (d.total)
				})])

				svg.append('g')
				.attr('class', 'x axis')
				.attr('transform', 'translate(0,' + height + ')')
				.call(xAxis)

				var bar = svg.selectAll('.label')
				.data(dataset)
				.enter().append('g')
				.attr('transform', function(d) { 
					return 'translate(' + x(d.label) + ', 0)'
				})

				svg.selectAll('.x.axis .tick text')
				// Ajouts evenement clic sur le label de l'axe X
				.on('click', function(e) {
					if (e != app.currentYear) {
						var parcellaire = PARCELLAIRES.find(function(d) {
							return (d == e)
						})
						if (parcellaire != undefined) {
							app.currentYear = e
							$('#year').text(app.currentYear)

							app.modules.changeBackground(parcellaire)
								
							app.map.removeLayer(app.shapeFiles[that.name()])
							app.shapeFiles[that.name()] = undefined
							that.init()

							$('#displayMap').trigger('click')
						}else {
							Materialize.toast('Aucune couche données trouvées pour l\'année {0}'.format(e), 3000)
						}
					}
				})

				var bar_enter = bar.selectAll('rect').data(function(d) { 
					return d.values
				})
				.enter()

				bar_enter.append('rect')
				.attr('width', x.rangeBand())
				.attr('y', function(d) { 
					return y(d.y1)
				})
				.attr('height', function(d) { 
					return (y(d.y0) - y(d.y1))
				})
				.style('fill', function(a, b, c) {
					var color = (culturesInfos[dataset[c].label] != undefined) ? culturesInfos[dataset[c].label][datas[c][a.name]] : undefined
					if (color != undefined) {
						return 'rgb({0})'.format(color.Couleur)
					} else {
						return 'rgb(189, 195, 199)'
					}
				})
				.attr('id', function(a, b, c) {
					if (datas[c][a.name] !== null) {
						return '{0}-{1}-{2}'.format(datas[c].IDPA, datas[c][a.name], dataset[c].label)
					}
				})
				.on('click', function(a) {
					app.params = []
					if (app.option == 'parcelle' || app.option == 'pf') {
						var dataElement = $(this).attr('id')
						if (dataElement != null) {
							dataElement = dataElement.split('-')
							app.params.push({
								IDPA: dataElement[0],
								ANNEE: dataElement[2]
							})
						}
						Materialize.toast('Donnée enregistrée.', 3000)
					} else {
						Materialize.toast('Action impossible !', 3000)
					}				
				})

				bar_enter.append('text')
				.text(function(a, b, c) {
					// Definition du text de l'element
					var text = this
					if (datas[c][a.name] !== null) {
						if ((a.y1 - a.y0) > 0) {
							var name = ''
							switch (a.name) {
								case 'P1':
								case 'P2':
								case 'P3':
								case 'P4':
								case 'P5':
								case 'P6':
									var code = (culturesInfos[dataset[c].label] != undefined) ? culturesInfos[dataset[c].label][datas[c][a.name]] : undefined
									if (code != undefined) name = code.Code_E
									break;

								case 'I1':
								case 'I2':
									name = 'CI'
									break;

								case 'S1':
								case 'S2':
									name = 'SC'
									break;
							}
							return name
						}
					}
				})
				.attr('y', function(d) { 
					return (y(d.y1) + ((y(d.y0) - y(d.y1))) / 1.75)
				})
				.attr('x', x.rangeBand() / 2)
				.attr('text-anchor', 'middle')
				.style('fill', '#000')

				// Ajout d'un evenement de pasage de souris sur un block du graphique
				bar.selectAll('rect').on('mouseover', function(d) {
					var elements = document.querySelectorAll(':hover')
					element = elements[elements.length - 1]

					var dataElement = element.getAttribute('id')
					if (dataElement != null) {
						dataElement = dataElement.split('-')

						divTooltip.style('display', 'inline-block')

						var infos = (culturesInfos[dataElement[2]] != undefined) ? culturesInfos[dataElement[2]][dataElement[1]] : undefined

						if (infos != undefined) {
							divTooltip.style('left', (((d3.event.pageX) - $('section').offset().left)) + 'px')
							divTooltip.style('top', ((d3.event.pageY -  $('section').offset().top) - 25) + 'px')

							var text = "{0} <br />".format(infos.Nom)
							text += "Début : {0} <br />".format((new Date(infos.DateD).toLocaleString()).split(' ')[0])
							text += "Fin : {0}".format((new Date(infos.DateF).toLocaleString()).split(' ')[0])
							divTooltip.html(text)

							currentElement = elements[elements.length - 1]
							currentElement.style.opacity = 0.8
						}
					}					
				})
				.on('mouseleave', function(d) {
					currentElement.style.opacity = 1
					divTooltip.style('display', 'none')
					$('.toolTip').text('')
				})
			} else {
				Materialize.toast('Aucunes données trouvées sur l\'assolement.', 3000)
			}
		})

		if (arguments.length === 0) {
			this.interventions()
		} else {
			this.interventions(arguments[0])
		}
	},
	// Construction et affichage du tableau d'assolement
	displayRecap: function() {
		var that = this

		$('#recap *').remove()

		// on masque le tableau des textures
		if (document.getElementById('texturesSol') !== null) {
			$('#texturesSol').fadeOut('fast')
			$('#texturesSolTitle').fadeOut('fast')
		}

		// si le table n'existe pas on le creer
		if (document.getElementById('assolementDetails') === null) {
			var pr = app.modules.queries.get('assolement/details/{0}'.format(app.currentYear))
			pr.done(function(data) {
				if (data.DATA !== 'NOT_FOUND') {
					var p = data.DATA.p
					var i = data.DATA.i
					var s = data.DATA.s

					var h4 = document.createElement('h4')
					$(h4).text('Assolement')
					$(h4).attr('id', 'assolementDetailsTitle')
					$('#recap').append(h4)

					var total = 0

					if (p !== undefined) {
						$.each(p, function(k, v) {
							total = (total + parseInt(v))
						})
						that.createCollapseDetails('Cultures Principales', p, 2, total)
					}

					if (i !== undefined) {
						that.createCollapseDetails('Cultures Intermédiares', i, 0, total)
					}

					if (s !== undefined) {
						that.createCollapseDetails('Cultures Sous-Couverts', s, 2, total)
					}
				} else {
					Materialize.toast('Aucunes données trouvées', 3000)
				}
			})
		} else {
			// s'il existe, on l'affiche, ça évite les appels a l'api inutiles
			$('#assolementDetails').fadeIn('fast')
			$('#assolementDetailsTitle').fadeIn('fast')
		}
	},
	// Fonction permettant la construction des elements d'affichage de l'assolement
	createCollapseDetails: function(culture, data, fixe, total) {
		var ul = document.createElement('ul')
		$(ul).attr('class', 'collapsible')
		$(ul).attr('id', 'assolementDetails')
		$(ul).attr('data-collapsible', 'accordion')

		var surfaceTotal = 0
		$.each(data, function(k, v) {
			surfaceTotal = (surfaceTotal + parseInt(v))
		})

		var li = document.createElement('li')

		var header = document.createElement('div')
		$(header).attr('class', 'collapsible-header')

		var tableHeader = document.createElement('table')
		$(tableHeader).attr('class', 'centered')

		var tbodyHeader = document.createElement('tbody')
		var trHeader = document.createElement('tr')
		var tdKeyHeader = document.createElement('td')
		$(tdKeyHeader).attr('style', 'width: 50%;')
		$(tdKeyHeader).text(culture)
		$(trHeader).append(tdKeyHeader)

		var tdValueHeader = document.createElement('td')
		$(tdValueHeader).attr('style', 'width: 50%;')
		$(tdValueHeader).text('{0} Ha'.format(surfaceTotal))
		$(trHeader).append(tdValueHeader)

		$(tbodyHeader).append(trHeader)
		$(tableHeader).append(tbodyHeader)
		$(header).append(tableHeader)
		$(li).append(header)

		var body = document.createElement('div')
		$(body).attr('class', 'collapsible-body')
		$(body).attr('style', 'padding: 0;')

		var tableBody = document.createElement('table')
		$(tableBody).attr('class', 'centered striped highlight bordered')
		var tbodyBody = document.createElement('tbody')

		$.each(data, function(k, v) {
			var trBody = document.createElement('tr')

			var tdBodyCulture = document.createElement('td')
			$(tdBodyCulture).text(k)
			$(tdBodyCulture).attr('style', 'width: 33%;')
			$(trBody).append(tdBodyCulture)

			var tBodySurface = document.createElement('td')
			$(tBodySurface).text('{0} Ha'.format(v))
			$(tBodySurface).attr('style', 'width: 33%;')
			$(trBody).append(tBodySurface)

			var tBodyPercent = document.createElement('td')
			var percent = ((v * 100) / total)
			$(tBodyPercent).text('{0} %'.format(percent.toFixed(fixe)))
			$(tBodyPercent).attr('style', 'width: 33%;')
			$(trBody).append(tBodyPercent)

			$(tbodyBody).append(trBody)
		})

		$(tableBody).append(tbodyBody)
		$(body).append(tableBody)

		$(li).append(body)

		$(ul).append(li)
		$(ul).collapsible()
		$('#recap').append(ul)
	},
	// Fonction de création et d'affichage du triangle des textures
	parcelle_sol: function() {
		var that = this

		this.photos('SOL')

		var pr = app.modules.queries.get('parcelle/point_ferti/{0}'.format(app.nom_court))
		pr.done(function(data) {
			that.active('parcelle_sol')

			$('#interventions *').remove()

			that.solDetails()

			if (data.DATA != 'NOT_FOUND') {
				$('#middle *').remove()

				var triangleTexture = document.createElement('div')
				$(triangleTexture).attr('id', 'triangleTexture')
				$(triangleTexture).addClass('triangleTexture')
				$('#middle').append(triangleTexture)

				TTS.build({
					container: '#triangleTexture',
					width: 400,
					height: 400,
					stroke: 'black',
					strokeWidth: 1,
					fill: '#eee',
					fontColor: 'black'
				})

				var pointsFertiTextures = document.createElement('div')
				$(pointsFertiTextures).attr('id', 'pointsFertiTextures')
				$(pointsFertiTextures).addClass('pointsFertiTextures')
				$('#middle').append(pointsFertiTextures)


				var table = document.createElement('table')
				$(table).attr('class', 'bordered striped centered highlight')
				$(table).attr('id', 'tablePF')

				var tbody = document.createElement('tbody')

				var thead = document.createElement('thead')
				var trHeader = document.createElement('tr')

				var thHeaderPF = document.createElement('th')
				$(thHeaderPF).attr('style', 'width: 25%;')
				$(thHeaderPF).text("Nom")
				$(trHeader).append(thHeaderPF)

				var thArgileHeader = document.createElement('th')
				$(thArgileHeader).attr('style', 'width: 25%;')
				$(thArgileHeader).text('Argiles (‰)')
				$(trHeader).append(thArgileHeader)

				var thLimonsHeader = document.createElement('th')
				$(thLimonsHeader).attr('style', 'width: 25%;')
				$(thLimonsHeader).text('Limons (‰)')
				$(trHeader).append(thLimonsHeader)

				var thSablesHeader = document.createElement('th')
				$(thSablesHeader).attr('style', 'width: 25%;')
				$(thSablesHeader).text('Sables (‰)')
				$(trHeader).append(thSablesHeader)

				$(thead).append(trHeader)
				$(table).append(thead)

				$.each(data.DATA, function(k, v) {
					TTS.addPoint(v.limons, v.argile)

					var trPF = document.createElement('tr')
					$(trPF).attr('style', 'cursor: pointer;')
					trPF.addEventListener('click', function() {
						that.pointFerti(k)
						$('#tablePF tr').removeClass('teal lighten-4')
						$('#tableauPointFeri tr').removeClass('teal lighten-4')
						$(this).addClass('teal lighten-4')
					})

					var tdPF = document.createElement('td')
					$(tdPF).attr('style', 'width: 25%;')
					$(tdPF).text('{0}'.format(k))

					$(trPF).append(tdPF)

					var tdArgile = document.createElement('td')
					$(tdArgile).attr('style', 'width: 25%;')
					$(tdArgile).text('{0}'.format(v.argile))
					$(trPF).append(tdArgile)

					var tdLimons = document.createElement('td')
					$(tdLimons).attr('style', 'width: 25%;')
					$(tdLimons).text('{0}'.format(v.limons))
					$(trPF).append(tdLimons)

					var tdSables = document.createElement('td')
					$(tdSables).attr('style', 'width: 25%;')
					$(tdSables).text('{0}'.format(v.sables))
					$(trPF).append(tdSables)

					$(tbody).append(trPF)
				})
					
				$(table).append(tbody)
				$("#pointsFertiTextures").append(table)
			} else {
				Materialize.toast('Aucuns points ferti pour cette parcelle.', 3000)
			}
		})
	},
	// fonction d'arffichage des détails du sol (textures/substratums)
	solDetails: function() {
		var that = this

		var pr = app.modules.queries.get('parcelle/sol/{0}'.format(app.nom_court))
		pr.done(function(data) {
			var ul = document.createElement('ul')
			$(ul).attr('class', 'collapsible')
			$(ul).attr('data-collapsible', 'accordion')

			var textures = that.createCollapseSol(data.DATA.textures, data.DATA.surfaces.textures, 'Textures', ul)
			$(ul).append(textures)

			var substratums = that.createCollapseSol(data.DATA.substratums, data.DATA.surfaces.substratums, 'Substratums', ul)
			$(ul).append(substratums)
			
			$(ul).collapsible()
			$('#interventions').append(ul)

			$('.collapsible').collapsible('open', 0)
		})
	},
	pointFerti: function(pf) {
		var pr = app.modules.queries.get('point_ferti/{0}'.format(pf))
		pr.done(function (data) {
			if (data.DATA !== 'NOT_FOUND') {
				$('#tableauPointFeri').remove()

				var div = document.createElement('div')
				$(div).attr('class', 'tableauPointFeri')
				$(div).attr('id', 'tableauPointFeri')

				var table = document.createElement('table')
				$(table).attr('class', 'bordered striped centered highlight')

				var thead = document.createElement('thead')
				var trHeader = document.createElement('tr')

				var thAnnee = document.createElement('th')
				$(thAnnee).text('Année Analyse')
				$(thAnnee).attr('style', 'width: 33%;')
				$(trHeader).append(thAnnee)

				var thCalcaireA = document.createElement('th')
				$(thCalcaireA).text('Calcaire Actif ({0})'.format(data.DATA[0].calcaire_actif_caco3.unite))
				$(thCalcaireA).attr('style', 'width: 33%;')
				$(trHeader).append(thCalcaireA)

				var thPh = document.createElement('th')
				$(thPh).text('pH')
				$(thPh).attr('style', 'width: 33%;')
				$(trHeader).append(thPh)

				$(thead).append(trHeader)
				$(table).append(thead)

				var tbody = document.createElement('tbody')

				$.each(data.DATA, function(k, v) {
					var tr = document.createElement('tr')
					$(tr).attr('style', 'cursor: pointer;')

					tr.addEventListener('click', function() {
						$('#recap *').remove()
						var h5 = document.createElement('h5')
						$(h5).text('{0}'.format(v.ananaly.value))
						$('#recap').append(h5)

						var table = document.createElement('table')
						$(table).attr('class', 'bordered striped centered highlight')
						var tbody = document.createElement('tbody')

						$.each(data.DATA[k], function(w, x) {
							if (w !== 'pf' && w !== 'ananaly') {
								var tr = document.createElement('tr')
							
								var tdNom = document.createElement('td')
								$(tdNom).text('{0}'.format(x.name))
								$(tr).append(tdNom)

								var tdValeur = document.createElement('td')
								if (x.value === 0) {
									 x.value = parseInt(x.value)
								} else {
									 x.value = parseFloat(x.value).toFixed(3)
								}
								if (x.unite !== undefined) {
									$(tdValeur).text('{0} {1}'.format(((x.value != null) ? x.value : ''), x.unite))
								} else {
									$(tdValeur).text('{0}'.format(((x.value != null) ? x.value : '')))
								}
								$(tr).append(tdValeur)
								
								$(tbody).append(tr)
							}
						})

						$(table).append(tbody)
						$('#recap').append(table)
						
						$('#modalRecap').modal('open')

						$('#tableauPointFeri tr').removeClass('teal lighten-4')
						$(this).addClass('teal lighten-4')					
					})

					var tdAnnee = document.createElement('td')
					$(tdAnnee).attr('style', 'width: 33%;')
					$(tdAnnee).text(v.ananaly.value)
					$(tr).append(tdAnnee)

					var tdCalcaireA = document.createElement('td')
					$(tdCalcaireA).attr('style', 'width: 33%;')
					$(tdCalcaireA).text(parseFloat(v.calcaire_actif_caco3.value).toFixed(2))
					$(tr).append(tdCalcaireA)

					var tdPH = document.createElement('td')
					$(tdPH).attr('style', 'width: 33%;')
					$(tdPH).text((v.ph_eau_sol.value !== null) ? v.ph_eau_sol.value.toFixed(2) : '')
					$(tr).append(tdPH)

					$(tbody).append(tr)
				})

				$(table).append(tbody)

				$(div).append(table)

				$('#middle').append(div)
			} else {
				Materialize.toast('Aucunes infomrations trouvées sur le point ferti.', 3000)
			}
		})
	},
	// fonction de construction du collapsible des textures/substratums de la parcelle
	createCollapseSol: function(data, surfaceTotal, title, ul) {
		var li = document.createElement('li')

		var header = document.createElement('div')
		$(header).attr('class', 'collapsible-header')
		$(header).text(title)
		$(li).append(header)

		var body = document.createElement('div')
		$(body).attr('class', 'collapsible-body')

		var table = document.createElement('table')
		$(table).attr('class', 'bordered striped centered highlight')

		var tbody = document.createElement('tbody')
		$.each(data, function(k, v) {
			var tr = document.createElement('tr')
			if (v.couleur !== undefined) {
				$(tr).attr('style', 'background: rgb({0}); color: rgb({1})'.format(
					v.couleur,
					(v.couleur != '255,255,255') ? '255,255,255' : '0,0,0'
				))
			}
			
			var tdTexture = document.createElement('td')
			$(tdTexture).text(v.nom)
			$(tdTexture).attr('style', 'width: 33%;')
			$(tr).append(tdTexture)

			var tdSurface = document.createElement('td')
			$(tdSurface).text('{0} Ha'.format((parseFloat(v.surface) / 10000).toFixed(2)))
			$(tdSurface).attr('style', 'width: 33%;')
			$(tr).append(tdSurface)

			var tdPercent = document.createElement('td')
			$(tdPercent).text('{0} %'.format(parseFloat(v.pourcentage).toFixed(2)))
			$(tdPercent).attr('style', 'width: 33%;')
			$(tr).append(tdPercent)

			$(tbody).append(tr)
		})
		$(table).append(tbody)

		$(body).append(table)
		$(li).append(body)
		return li
	},
	// fonction d'ajout et de retrait de la class active sur les boutons
	active: function(btn) {
		$('#buttons ul li').removeClass('active')
		$('#btn-{0}'.format(btn)).addClass('active')
	},
	photos: function() {
		var that = this

		var pr = app.modules.queries.get('photos/{0}/{1}'.format(app.nom_court, app.currentYear))
		if (arguments.length > 0) {
			var pr = app.modules.queries.get('photos/{0}/{1}/{2}'.format(app.nom_court, arguments[0], app.currentYear))
		}
		pr.done(function (data) {
			if (data.DATA !== 'NOT_FOUND') {
				$('#displayPhotos').fadeIn('fast')

				$('#displayPhotos').on('click', function(e) {
					e.preventDefault()

					$("#carouselContainer").fadeIn('slow')
					$("#carouselContainer *").remove()

					var carousel = document.createElement('div')
					$(carousel).attr('class', 'carousel carousel-slider center')
					
					$.each(data.DATA, function(k, v) {
						var div = document.createElement('div')
						$(div).attr('class', 'center')

						var a = document.createElement('a')
						$(a).attr('class', 'carousel-item')

						var img = document.createElement('img')
						$(img).attr('src', "{0}/data/photos/{1}".format(location.href, v))
						$(a).append(img)

						var span = document.createElement('span')
						$(span).attr('class', 'btn waves-effect white grey-text darken-text-2')
						$(span).attr('style', 'position: absolute; z-index: 20; top: 20px; left: 20px;')
						$(span).text(v.split('~')[1].split('.')[0])
						$(a).append(span)

						$(div).append(a)

						$(carousel).append(div)
					})

					$(carousel).carousel({
						fullWidth: true,
						indicators: true
					})

					$("#carouselContainer").append(carousel)
				})
				
			} else {
				$('#displayPhotos').fadeOut('fast')
			}
		})
	},
	// Fonction d'affichage des recoltes
	parcelle_recoltes: function() {
		var that = this
		if(app.params.length > 0) {
			// Requetes de récupération de données
			var pr = app.modules.queries.get('recoltes/parcelle/{0}/{1}'.format(app.params[0].IDPA, app.params[0].ANNEE))
			pr.done(function(data) {
				if (data.DATA.recoltes != 'NOT_FOUND') {

					// Met le bouton récoltes en valeur
					that.active('parcelle_recoltes')

					// Supprime l'ancien contenu de middle
					$('#middle *').remove()

					$('#middle section').remove()
					var barChartEffluent = document.createElement('section')
					$(barChartEffluent).attr('class', 'barChartEffluent')
					$(barChartEffluent).attr('id', 'barChartEffluent')
					$('#middle').append(barChartEffluent)

					var datas = data.DATA.recoltes

					var xData = ["DateI"];
				 
					var margin = {
						top: 20,
						right: 50,
						bottom: 30,
						left: 50
					}

					var width = ((parseInt(d3.select('section').style('width'), 10) - margin.left) - margin.right)
					var height = ((parseInt(d3.select('section').style('height'), 10) - margin.top) - margin.bottom)
					 
					var x = d3.scale.ordinal()
							.rangeRoundBands([0, width])
					 
					var y = d3.scale.linear()
							.rangeRound([height, 0])
					 
					var xAxis = d3.svg.axis()
							.scale(x)
							.orient("bottom")

					var yAxis = d3.svg.axis()
							.scale(y)
							.orient("left")
							.ticks(5)

					var svg = d3.select("#barChartEffluent").append("svg")
							.attr("width", width + margin.left + margin.right)
							.attr("height", height + margin.top + margin.bottom)
							.append("g")
							.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
					 
					var dataIntermediate = xData.map(function (c) {
						return datas.map(function (d) {
							return {x: d.DateI, y: d.Qte_tms_ha, color: d.Couleur_Prod, Code_E: d.Code_E}
						});
					});
					 
					var dataStackLayout = d3.layout.stack()(dataIntermediate)
					 
					x.domain(dataStackLayout[0].map(function (d) {
						return d.x
					}));
					 
					y.domain([0,
						d3.max(dataStackLayout[dataStackLayout.length - 1],
							function (d) { return d.y0 + d.y })
						])
					.nice();
					
					var layer = svg.selectAll(".stack")
						.data(dataStackLayout)
						.enter().append("g")
						.attr("class", "stack")
					
					var bar = layer.selectAll("rect")
						.data(function (d) {
							return d
						})
						.enter().append("rect")
						.attr("x", function (d) {
							return x(d.x)
						})
						.attr("y", function (d) {
							return y(d.y + d.y0)
						})
						.attr("height", function (d) {
							return y(d.y0) - y(d.y + d.y0)
						})
						.attr("width", x.rangeBand())
						.style("fill", function(d) {
							return 'rgb(' + d.color + ')'
						})
						.on('click', function(e) {
							$('#middle table').remove()

							app.modules.builder.buildTable({
								// Initialisation de l'entête du tableau
								// Format JSON des données d'entête
								// la clé correspond au nom de la colonne de la base de données 
								// ou au nom donné pendant le traitement des données dans l'API
								// La valeur correspond au texte de l'entête
								header: {
									DateI: 'Date',
									Produit: 'Produit',
									Hpc: 'Humidité (%)',
									Impurete_kg: 'Impurté (kg)',
									Petit_Grain_kg: 'Petit grain (kg)',
									Poids_Specifique: 'Poids Spécifique', 
									Proteinepc: 'Protéine (%)', 
									MSpc: 'Matière sèche (%)'
								},
								// data est un tableau dans le quel nous lui passons les données
								data: data.DATA.recoltesTout[e.x],
								// Nom du conteneur du tableau
								container: '#middle'
							})
						})

					layer.selectAll("text")
						.data(function (d) {
							return d
						})
						.enter()
						.append("text")
						.attr("x", function (d) {
							return (x(d.x) + (x.rangeBand() / 2))
						})
						.attr("y", function (d) {
							return y(d.y + d.y0)
						})
						.attr("dy", "1em")
						.attr("text-anchor", "middle")
						.attr("font-size", "14px")
						.attr("fill", "black")
						.attr("transform", function(d) { return "translate(0, -20)"; })
						.text(function(d) {
							return (d.Code_E != undefined) ? d.Code_E : ''
						})
					 
					svg.append("g")
						.attr("class", "x axis")
						.attr("transform", "translate(0," + height + ")")
						.call(xAxis)

					svg.selectAll('.x.axis .tick text')
						.on('click', function(e) {
							$('#middle table').remove()

							app.modules.builder.buildTable({
								// Initialisation de l'entête du tableau
								// Format JSON des données d'entête
								// la clé correspond au nom de la colonne de la base de données 
								// ou au nom donné pendant le traitement des données dans l'API
								// La valeur correspond au texte de l'entête
								header: {
									DateI: 'Date',
									Produit: 'Produit',
									Hpc: 'Humidité (%)',
									Impurete_kg: 'Impurté (kg)',
									Petit_Grain_kg: 'Petit grain (kg)',
									Poids_Specifique: 'Poids Spécifique', 
									Proteinepc: 'Protéine (%)', 
									MSpc: 'Matière sèche (%)'
								},
								// data est un tableau dans le quel nous lui passons les données
								data: data.DATA.recoltesTout[e],
								// Nom du conteneur du tableau
								container: '#middle'
							})
						})

					svg.append("g")
						.attr("class", "y axis")
						.call(yAxis)

					svg.append("text")
						.attr("x", 20)
						.attr("y", 0)
						.attr("text-anchor", "middle")
						.text("tMS")

					$('#interventions *').remove()
					var ul = document.createElement('ul')
					$(ul).attr('id', 'tabs-swipe')
					$(ul).attr('class', 'tabs')
					$('#interventions').append(ul)

					if (data.DATA.legende != 'NOT_FOUND') {
						var liLegend = document.createElement('li')
						if (data.DATA.recap == 'NOT_FOUND' || data.DATA.recap == undefined) {
							$(liLegend).attr('class', 'tab col s12 active')
						} else {
							$(liLegend).attr('class', 'tab col s6 active')
						}
						var aLegend = document.createElement('a')
						$(aLegend).attr('href', '#swipe-legende')
						$(aLegend).text('Groupes')
						$(liLegend).append(aLegend)
						$(ul).append(liLegend)

						var divLegend = document.createElement('div')
						$(divLegend).attr('id', 'swipe-legende')
						$(divLegend).attr('class', 'col s12')

						var table = document.createElement('table')
						$(table).attr('class', 'bordered striped highlight')
						var tbody = document.createElement('tbody')
						$.each(data.DATA.legende, function(k, v) {
							var tr = document.createElement('tr')
							var td = document.createElement('td')
							var a = document.createElement('a')
							$(a).text('{0}'.format(v.C_Produit))
							$(a).attr('style', 'color: #000;')
							$(tr).attr('style', 'background-color: rgb({0});'.format(v.Couleur))
							$(td).append(a)
							$(tr).append(td)
							$(tbody).append(tr)
						})
						$(table).append(tbody)
						$(divLegend).append(table)
						$('#interventions').append(divLegend)
					}

					if (data.DATA.recap != undefined) {
						var liRecap = document.createElement('li')
						if (data.DATA.recap == 'NOT_FOUND') {
							$(liRecap).attr('class', 'tab col s12 active')
						} else {
							$(liRecap).attr('class', 'tab col s6 active')
						}
						var aCultures = document.createElement('a')
						$(aCultures).attr('href', '#swipe-recap')
						$(aCultures).text('Cultures')
						$(liRecap).append(aCultures)
						$(ul).append(liRecap)

						var divRecap = document.createElement('div')
						$(divRecap).attr('id', 'swipe-recap')
						$(divRecap).attr('class', 'col s12')

						$.each(data.DATA.recap, function(k, v) {
							var table = document.createElement('table')
							$(table).attr('class', 'bordered striped highlight')
							
							var thead = document.createElement('thead')
							var th = document.createElement('th')
							$(th).attr('colspan', '3')
							$(th).attr('style', 'text-align: center !important;')
							var td = document.createElement('td')
							$(td).attr('style', 'text-align: center !important;')
							$(td).text(k)
							$(th).append(td)
							$(thead).append(th)
							$(table).append(thead)

							var tbody = document.createElement('tbody')

							$.each(v, function(k, v) {
								var tr = document.createElement('tr')

								var td = document.createElement('td')
								$(td).attr('style', 'text-align: center !important;')
								$(td).text('{0}'.format(v.Code_E))
								$(tr).append(td)

								var td = document.createElement('td')
								$(td).text('{0}'.format(v.Produit))
								$(td).attr('style', 'text-align: center !important;')
								$(tr).append(td)

								var td = document.createElement('td')
								$(td).text('{0}'.format(parseFloat(v.QteTMS).toFixed(2)))
								$(td).attr('style', 'text-align: center !important;')
								$(tr).append(td)

								$(tbody).append(tr)
							})

							$(table).append(tbody)
							$(divRecap).append(table)
						})
						$('#interventions').append(divRecap)
					}

					$('ul.tabs').tabs()
				} else {
					Materialize.toast('Aucunes données trouvées pour cette parcelle.', 3000)
				}
			})
		} else {
			Materialize.toast('Veuillez sélectionner une culture.', 3000)
		}
	},
	parcelle_effluent: function() {
		var that = this
		if(app.params.length > 0) {
			// Recuperation des informations sur l'API
			var pr = app.modules.queries.get('effluents/{0}/{1}'.format(app.params[0].IDPA, app.params[0].ANNEE))
			pr.done(function(data) {
				if (data.DATA != 'NOT_FOUND') {
					var pr = app.modules.queries.get('effluents/legend')
					pr.done(function(data) {
						if (data.DATA != 'NOT_FOUND') {
							$('#interventions *').remove()

							var table = document.createElement('table')
							$(table).attr('class', 'bordered striped highlight centered')
							// Creation du corps
							var tbody = document.createElement('tbody')

							// Creation des colonnes
							$.each(data.DATA, function(k, v) {
								var tr = document.createElement('tr')

								var td = document.createElement('td')
								$(td).text(v.Nom)
								var color = '#fff'
								if (v.Couleur == '210,214,211') {
									color = '#000'
								}
								$(td).attr('style', 'background-color: rgb({0}); color: {1};'.format(v.Couleur, color))
								$(tr).append(td)

								$(tbody).append(tr)
							})
							$(table).append(tbody)
							$('#interventions').append(table)
						}
					})
				
					// Met le bouton récoltes en valeur
					that.active('parcelle_effluent')

					$('#middle *').remove()

					$('#middle section').remove()
					var barChartEffluent = document.createElement('section')
					$(barChartEffluent).attr('class', 'barChartEffluent')
					$(barChartEffluent).attr('id', 'barChartEffluent')
					$('#middle').append(barChartEffluent)

					var datas = []
					$.each(data.DATA, function(k, v) {
						datas.push({
							name: v.name,
							origin: v.origin,
							uniteS: v.uniteS,
							color: v.color,
							value: parseFloat(v.value).toFixed(2),
							unite: v.unite,
							sw: parseFloat(v.sw).toFixed(2),
							surface: parseFloat(v.surface).toFixed(2)
						})
					})
					 
					var xData = ["value"];

					var margin = {top: 50, right: 50, bottom: 30, left: 50}
					var width = ((parseInt(d3.select('section').style('width'), 10) - margin.left) - margin.right)
					var height = ((parseInt(d3.select('section').style('height'), 10) - margin.top) - margin.bottom)
					 
					var x = d3.scale.ordinal()
							.rangeRoundBands([0, width])
					 
					var y = d3.scale.linear()
							.rangeRound([height, 0])
					 
					var xAxis = d3.svg.axis()
							.scale(x)
							.orient("bottom")

					var yAxis = d3.svg.axis()
							.scale(y)
							.orient("left")
							.ticks(5)

					var svg = d3.select("section").append("svg")
							.attr("width", width + margin.left + margin.right)
							.attr("height", height + margin.top + margin.bottom)
							.append("g")
							.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
					 
					var dataIntermediate = xData.map(function (c) {
						return datas.map(function (d) {
							return {x: d.name, y: d[c]}
						})
					})
					 
					var dataStackLayout = d3.layout.stack()(dataIntermediate)
					 
					x.domain(dataStackLayout[0].map(function (d) {
						return d.x
					}))
					 
					y.domain([0,
						d3.max(dataStackLayout[dataStackLayout.length - 1],
							function (d) { return d.y0 + d.y})
						])
					.nice();
					
					var layer = svg.selectAll(".stack")
						.data(dataStackLayout)
						.enter().append("g")
						.attr("class", "stack")
					
					layer.selectAll("rect")
						.data(function (d) {
							return d
						})
						.enter().append("rect")
						.attr("x", function (d) {
							return x(d.x)
						})
						.attr("y", function (d) {
							return y(d.y + d.y0)
						})
						.attr("height", function (d) {
							return y(d.y0) - y(d.y + d.y0)
						})
						.attr("width", x.rangeBand())
						.style("fill", function (d, i) {
							return 'rgb(' + datas[i].color + ')'
						})
						.attr('id', function(d, i) {
							return 'bar-{0}'.format(i)
						})
						.on('click', function(e, i) {
							$('#middle table').remove()

							var pr = app.modules.queries.get('effluents/details/{0}/{1}'.format(app.params[0].IDPA, app.params[0].ANNEE))
							pr.done(function(data) {
								if (data.DATA != 'NOT_FOUND') {
									// Création du tableau de détails
									app.modules.builder.buildTable({
										// Initialisation de l'entête du tableau
										// Format JSON des données d'entête
										// la clé correspond au nom de la colonne de la base de données 
										// ou au nom donné pendant le traitement des données dans l'API
										// La valeur correspond au texte de l'entête
										header: {
											DateI: 'Intervention', 
											Nom: 'Nom', 
											Origine_Anx: 'Origine', 
											Unite_S: 'Stockage', 
											SW: 'Surface travaillée (Ha)',
											Qte_brute: 'Tonnage'
										},
										// data est un tableau dans le quel nous lui passons les données
										data: data.DATA,
										// Nom du conteneur du tableau
										container: '#middle'
									})
								} else {
									Materialize.toast('Aucunes données trouvées.', 3000)
								}
							})
						})

					svg.selectAll(".bartext")
						.data(datas)
						.enter()
						.append("text")
						.attr('y', function(d, i) {
							return (height - ($('#bar-{0}'.format(i)).attr('height') / 2))
						})
						.attr("x", function(d, i) {
							return (i * (width / datas.length) + (width / datas.length) / 2)
						})
						.attr('width', function(d, i) {
							return $('#bar-{0}'.format(i)).attr('width')
						})
						.attr('text-anchor', 'middle')
						.style('fill', '#fff')
						.style('text-align', 'center')
						.html(function(e, i) {
							return ('{0} {1}'.format(parseFloat(datas[i].value), datas[i].unite))
						})

					svg.append("g")
						.attr("class", "x axis")
						.attr("transform", "translate(0," + height + ")")
						.call(xAxis)

					svg.append("g")
						.attr("class", "y axis")
						.call(yAxis)

					$('#middle').scrollLeft(width)
					$('#middle').scrollTop(height)
				} else {
					Materialize.toast('Aucunes données trouvées pour cette parcelle.', 3000)
				}
			})
		} else {
			Materialize.toast('Veuillez sélectionner une culture.', 3000)
		}
	},
	parcelle_semis: function() {
		var that = this

		if(app.params.length > 0) {
			var pr = app.modules.queries.get('semis/{0}/{1}'.format(app.params[0].IDPA, app.params[0].ANNEE))
			pr.done(function(data) {
				if (data.DATA != 'NOT_FOUND') {
					if (data.DATA.semis != 'NOT_FOUND') {
						// Met le bouton récoltes en valeur
						that.active('parcelle_semis')

						$('#middle .top').remove()
						$('#middle table').remove()

						$('#middle section').remove()
						var pointsChartSemis = document.createElement('section')
						$(pointsChartSemis).attr('class', 'pointsChartSemis')
						$(pointsChartSemis).attr('id', 'pointsChartSemis')
						$('#middle').append(pointsChartSemis)

						var margin = {
							top: 20, 
							right: 200, 
							bottom: 30, 
							left: 0
						}

						var padding = {
							top: 50,
							left: 200
						}

						var width = ((parseInt(d3.select('section').style('width'), 10) - margin.left) - margin.right)
						var height = ((parseInt(d3.select('section').style('height'), 10) - margin.top) - margin.bottom)

						var datas = [{x: 0, y: 0, Espece: '', DateI: ''}]
						$.each(data.DATA.semis, function(k, v) {
							datas.push(v)
						})

						var dates = []
						dates.push()
						$.each(data.DATA.dates, function(k, v) {
							dates.push(v)
						})

						datas.push({x: (dates.length - 1), y: 0, Espece: '', DateI: ''})

						var xValue = function(d) { return d.x }
						var xScale = d3.scale.linear()
							.domain([0, d3.max(datas, function(d) { return d.x })])
							.range([0, width])
						var xMap = function(d) { return xScale(xValue(d)) }

						var xAxis = d3.svg.axis()
							.scale(xScale)
							.orient("bottom")
							.ticks(d3.max(datas, function(d){ return d.x }))
							.tickFormat(function(d) {
								return dates[d]
							})

						var yValue = function(d) { return d.y }
						var yScale = d3.scale.linear()
							.domain([0, d3.max(datas, function(d){ return  d.y })])
							.range([height, 0])
						var yMap = function(d) { return yScale(yValue(d)) }

						var yAxis = d3.svg.axis()
							.scale(yScale)
							.orient("left")
							.innerTickSize(-width)
							.outerTickSize(0)
							.tickPadding(10)
							.ticks(d3.max(datas, function(d){ return d.y }))
							.tickFormat(function(d) {
								return datas[d].Espece
							})

						var svg = d3.select("section").append("svg")
							.attr("width", ((width + margin.left) + margin.right))
							.attr("height", ((height + margin.top) + margin.bottom) + padding.top)
							.attr("transform", "translate(" + margin.left + "," + margin.top + ")")

						svg.append("g")
							.attr("class", "x axis")
							.attr("transform", "translate(" + padding.left + ", " + (height + padding.top) + ")")
							.call(xAxis)

						svg.selectAll('.x.axis .tick text')
						.on('click', function(e) {
							that.detailSemis(app.params[0].IDPA, dates[e].split('/').join('_'))
						})

						svg.append("g")
							.attr("class", "y axis")
							.attr("transform", "translate(" + padding.left + ", " + padding.top + ")")
							.call(yAxis)

						var circles = svg.selectAll("circle")
							.data(datas)
							.enter()
							.append("circle")
							.attr("cx", xMap)
							.attr("cy", yMap)
							.attr("transform", "translate(" + padding.left + ", " + padding.top + ")")
							.attr("r", function (d) { return d.radius })
							.style("fill", function(d) { return d.color })
							.on('click', function(e) {
								that.detailSemis(app.params[0].IDPA, $(e)[0].DateI.split('/').join('_'))
							})
					} else {
						Materialize.toast('Aucuns semis trouvés pour cette culture', 3000)
					}
				} else {
					Materialize.toast('Aucunes données trouvées pour cette culture', 3000)
				}
			})
		} else {
			Materialize.toast('Veuillez sélectionner une culture.', 3000)
		}
	},
	detailSemis: function(idpa, date) {
		var that = this

		var pr = app.modules.queries.get('semis/details/{0}/{1}'.format(idpa, date.split('/').join('_')))
		pr.done(function(data) {
			if (data.DATA != 'NOT_FOUND') {
				$('#middle table').remove()

				app.modules.builder.buildTable({
					// Initialisation de l'entête du tableau
					// Format JSON des données d'entête
					// la clé correspond au nom de la colonne de la base de données 
					// ou au nom donné pendant le traitement des données dans l'API
					// La valeur correspond au texte de l'entête
					header: {
						DateI: 'Intervention',
						Espece: 'Espece', 
						Variete: 'Variete', 
						Origine: 'Origine', 
						SW: 'Surface travaillée (Ha)',
						Qte: 'Quantité',
						QteSurface: 'Qte. / Ha'
					},
					// data est un tableau dans le quel nous lui passons les données
					data: data.DATA,
					// Nom du conteneur du tableau
					container: '#middle'
				})
			} else {
				Materialize.toast('Aucunes données trouvées.', 3000)
			}
		})
	},
	parcelle_adventices: function() {
		this.adventices(3, 'parcelle_adventices')
	},
	pf_adventices: function() {
		this.adventices(2, 'pf_adventices')
	},
	adventices: function(ido, fnc) {
		var that = this

		if(app.params.length > 0) {
			var pr = app.modules.queries.get('adventices/{0}/{1}/{2}'.format(app.params[0].IDPA, app.params[0].ANNEE, ido))
			pr.done(function(data) {
				if (data.DATA != 'NOT_FOUND') {

					if (data.DATA.datas != 'NOT_FOUND') {
						// Met le bouton en valeur
						that.active(fnc)

						$('#middle *').remove()
						$('#interventions *').remove()

						var ul = document.createElement('ul')
						$(ul).attr('id', 'tabs-swipe-graphs')
						$(ul).attr('class', 'tabs')
						$('#middle').append(ul)

						/* Graphique des adventices */
						if (data.DATA.adventices != 'NOT_FOUND') {
							var liAdv = document.createElement('li')
							if (data.DATA.ptsAdvs == 'NOT_FOUND') {
								$(liAdv).attr('class', 'tab col s12 active')
							} else {
								$(liAdv).attr('class', 'tab col s6 active')
							}
							var aAdv = document.createElement('a')
							$(aAdv).attr('href', '#swipe-adv')
							$(aAdv).text('Évolutions Flores')
							$(liAdv).append(aAdv)
							$(ul).append(liAdv)

							var divAdv = document.createElement('div')
							$(divAdv).attr('id', 'swipe-adv')
							$(divAdv).attr('class', 'col s12')

							var pointChartsAdventices = document.createElement('section')
							$(pointChartsAdventices).attr('class', 'pointChartsAdventices')
							$(pointChartsAdventices).attr('id', 'pointChartsAdventices')
							$(divAdv).append(pointChartsAdventices)
							
							$('#middle').append(divAdv)

							var margin = {
								top: 20,
								right: 100,
								bottom: 20,
								left: 0
							}

							var padding = {
								top: 50,
								left: 80
							}

							var width = ((parseInt(d3.select('section').style('width'), 10) - margin.left) - margin.right)
							var height = ((parseInt(d3.select('section').style('height'), 10) - margin.top) - margin.bottom)

							var adventices = [{x: 0, y: 0, radius: 0, color: ''}]
							$.each(data.DATA.adventices, function(k, v) {
								adventices.push(v)
							})

							var groupes = ['']
							$.each(data.DATA.groupes, function(k, v) {
								groupes.push(v)
							})

							var years = ['']
							$.each(data.DATA.years, function(k, v) {
								years.push(v)
							})

							var xValue = function(d) { return d.x }
							var xScale = d3.scale.linear()
								.domain([0, d3.max(adventices, function(d) { return d.x })])
								.range([0, width])
							var xMap = function(d) { return xScale(xValue(d)) }

							var xAxis = d3.svg.axis()
								.scale(xScale)
								.orient("bottom")
								.ticks(d3.max(adventices, function(d){ return d.x }))
								.tickFormat(function(d) {
									return (d == 0) ? '' : d
								})

							var yValue = function(d) { return d.y }
							var yScale = d3.scale.linear()
								.domain([0, d3.max(adventices, function(d){ return d.y })])
								.range([height, 0])
							var yMap = function(d) { return yScale(yValue(d)) }

							var yAxis = d3.svg.axis()
								.scale(yScale)
								.orient("left")
								.innerTickSize(-width)
								.outerTickSize(0)
								.tickPadding(10)
								.ticks(d3.max(adventices, function(d){ return d.y }))
								.tickFormat(function(d) {
									return years[d]
								})

							var svg = d3.select(".pointChartsAdventices").append("svg")
								.attr("width", ((width + margin.left) + margin.right) + padding.left)
								.attr("height", ((height + margin.top) + margin.bottom) + padding.top)
								.attr("transform", "translate(" + margin.left + "," + margin.top + ")")

							svg.append("g")
								.attr("class", "x axis")
								.attr("transform", "translate(" + padding.left + ", " + (height + padding.top) + ")")
								.call(xAxis)
								.selectAll("text")

							svg.append("text")
								.attr("x", width)
								.attr("y", ((height + padding.top) - 10))
								.attr("text-anchor", "middle")
								.attr("transform", "translate(" + padding.left + ", 0)")
								.text("Groupes d'adventices")

							svg.append("g")
								.attr("class", "y axis")
								.attr("transform", "translate(" + padding.left + ", " + padding.top + ")")
								.call(yAxis)

							var circles = svg.selectAll("circle")
								.data(adventices)
								.enter()
								.append("circle")
								.attr("cx", xMap)
								.attr("cy", yMap)
								.attr("r", function(d) { return (d.radius * 5) })
								.attr('stroke', 'black')
								.attr('stroke-width', 1)
								.style("fill", function(d) { return 'rgb({0})'.format(d.color) })
								.attr("transform", "translate(" + padding.left + ", " + padding.top + ")")

							var divTooltip = d3.select('.pointChartsAdventices').append('div').attr('class', 'toolTip')
							circles.on('mouseover', function(d) {
								divTooltip.style('left', (((d3.event.pageX) - $('.pointChartsAdventices').offset().left)) + 'px')
								divTooltip.style('top', ((d3.event.pageY -  $('.pointChartsAdventices').offset().top) - 25) + 'px')
								
								divTooltip.html(d.flore)
								divTooltip.style('display', 'inline-block')
							})
							.on('mouseleave', function(d) {
								currentElement.style.opacity = 1
								divTooltip.style('display', 'none')
								$('.toolTip').text('')
							})
						}

						/* Graphique des flores et groupes */
						if (data.DATA.flores != 'NOT_FOUND') {
							var pointsChartFlores = document.createElement('section')
							$(pointsChartFlores).attr('class', 'pointsChartFlores')
							$(pointsChartFlores).attr('id', 'pointsChartFlores')
							$(divAdv).append(pointsChartFlores)
							$('#middle').append(divAdv)

							var margin = {
								top: 50,
								right: 100,
								bottom: 150,
								left: 0
							}

							var padding = {
								top: 20,
								left: 80
							}

							var width = ((parseInt(d3.select('section').style('width'), 10) - margin.left) - margin.right)
							var height = (parseInt(d3.select('section').style('height'), 10) - margin.top)

							var datas = [{x: 0, y: 0, Groupe_ADV: '', Moyenne: 0}]
							$.each(data.DATA.datas, function(k, v) {
								datas.push(v)
							})

							var flores = ['']
							$.each(data.DATA.flores, function(k, v) {
								flores.push(v)
							})

							var xValue = function(d) { return d.x }
							var xScale = d3.scale.linear()
								.domain([0, d3.max(datas, function(d) { return d.x })])
								.range([0, width])
							var xMap = function(d) { return xScale(xValue(d)) }

							var xAxis = d3.svg.axis()
								.scale(xScale)
								.orient("bottom")
								.ticks(d3.max(datas, function(d){ return d.x }))
								.tickFormat(function(d) {
									return (d == 0) ? '' : d
								})

							var yValue = function(d) { return d.y }
							var yScale = d3.scale.linear()
								.domain([0, d3.max(datas, function(d){ return  d.y})])
								.range([height, 0])
							var yMap = function(d) { return yScale(yValue(d)) }

							var yAxis = d3.svg.axis()
								.scale(yScale)
								.orient("left")
								.innerTickSize(-width)
								.outerTickSize(0)
								.tickPadding(10)
								.ticks(d3.max(datas, function(d){ return d.y }))
								.tickFormat(function(d) {
									return flores[d]
								})

							var svg = d3.select(".pointsChartFlores").append("svg")
								.attr("width", ((width + margin.left) + margin.right) + padding.left)
								.attr("height", ((height + margin.top) + margin.bottom) + padding.top)
								.attr("transform", "translate(" + margin.left + "," + margin.top + ")")

							svg.append("g")
								.attr("class", "x axis")
								.attr("transform", "translate(" + padding.left + ", " + (height + padding.top) + ")")
								.call(xAxis)

							svg.append("text")
								.attr("x", width)
								.attr("y", ((height + padding.top) - 10))
								.attr("text-anchor", "middle")
								.attr("transform", "translate(" + padding.left + ", 0)")
								.text("Groupes d'adventices")

							svg.append("g")
								.attr("class", "y axis")
								.attr("transform", "translate(" + padding.left + ", " + padding.top + ")")
								.call(yAxis)

							var circles = svg.selectAll("circle")
								.data(datas)
								.enter()
								.append("circle")
								.attr("cx", xMap)
								.attr("cy", yMap)
								.attr("r", function (d) { return (d.Moyenne * 5) })
								.attr('stroke', 'black')
								.attr('stroke-width', 1)
								.style("fill", function(d) { return 'rgb({0})'.format(d.Couleur_Flore) })
								.attr("transform", "translate(" + padding.left + ", " + padding.top + ")")
						}

						/* Graphique des points d'adventices */
						if (data.DATA.ptsAdvs != 'NOT_FOUND') {
							var liPtAdv = document.createElement('li')
							if (data.DATA.adventices == 'NOT_FOUND') {
								$(liPtAdv).attr('class', 'tab col s12 active')
							} else {
								$(liPtAdv).attr('class', 'tab col s6 active')
							}
							var aPtAdv = document.createElement('a')
							$(aPtAdv).attr('href', '#swipe-pt-adv')
							$(aPtAdv).text('Points d\'adventices')
							$(liPtAdv).append(aPtAdv)
							$(ul).append(liPtAdv)

							var divPtAdv = document.createElement('div')
							$(divPtAdv).attr('id', 'swipe-pt-adv')
							$(divPtAdv).attr('class', 'col s12')

							var pointsChartsPtsAdventices = document.createElement('section')
							$(pointsChartsPtsAdventices).attr('class', 'pointsChartsPtsAdventices')
							$(pointsChartsPtsAdventices).attr('id', 'pointsChartsPtsAdventices')
							$(divPtAdv).append(pointsChartsPtsAdventices)
							
							$('#middle').append(divPtAdv)

							var pf = data.DATA.pfs[0]

							var d = {
								ptsAdvs: data.DATA.ptsAdvs,
								grpsPtsAdv: data.DATA.grpsPtsAdv
							}
							$.each(data.DATA.pfs, function(k, v) {
								var btnPf = document.createElement('a')
								$(btnPf).attr('class', 'waves-effect waves-light btn centered')
								$(btnPf).text(v)
								$(btnPf).attr('style', 'margin: 10px auto;')
								btnPf.addEventListener('click', function() {
									$('#middle .pointsChartsPtsAdventices svg').remove()
									that.buildPfChart(d, $(this).text())
								})
								$('#middle .pointsChartsPtsAdventices').append(btnPf)
							})

							that.buildPfChart(d, pf)
						}

						var ul = document.createElement('ul')
						$(ul).attr('id', 'tabs-swipe')
						$(ul).attr('class', 'tabs')
					
						$('#interventions').append(ul)

						if (data.DATA.groupes != 'NOT_FOUND') {
							var liLegend = document.createElement('li')
							if (data.DATA.assolement == 'NOT_FOUND') {
								$(liLegend).attr('class', 'tab col s12 active')
							} else {
								$(liLegend).attr('class', 'tab col s6 active')
							}
							var aLegend = document.createElement('a')
							$(aLegend).attr('href', '#swipe-legende')
							$(aLegend).text('Groupes')
							$(liLegend).append(aLegend)
							$(ul).append(liLegend)

							var divLegend = document.createElement('div')
							$(divLegend).attr('id', 'swipe-legende')
							$(divLegend).attr('class', 'col s12')

							var table = document.createElement('table')
							$(table).attr('class', 'bordered striped highlight')
							var tbody = document.createElement('tbody')
							$.each(data.DATA.groupes, function(k, v) {
								var tr = document.createElement('tr')
								var td = document.createElement('td')
								var a = document.createElement('a')
								$(a).text('{0} : {1}'.format((k + 1), v))
								$(a).attr('class', 'tooltipped')
								$(a).attr('data-position', 'right')
								$(a).attr('data-delay', 50)
								$(a).attr('style', 'color: #000;')
								var grps = []
								$.each(data.DATA.grpsDetails[v], function(k, v) {
									grps.push(v.Nom_Commun)
									$(tr).attr('style', 'background-color: rgb({0});'.format(v.Couleur))
								})
								$(a).attr('data-tooltip', grps.join(' - '))
								$(a).tooltip({delay: 50})
								$(td).append(a)
								$(tr).append(td)
								$(tbody).append(tr)
							})
							$(table).append(tbody)
							$(divLegend).append(table)
							$('#interventions').append(divLegend)
						}
						
						if (data.DATA.assolement != 'NOT_FOUND') {
							var liCultures = document.createElement('li')
							if (data.DATA.groupes == 'NOT_FOUND') {
								$(liCultures).attr('class', 'tab col s12 active')
							} else {
								$(liCultures).attr('class', 'tab col s6 active')
							}
							var aCultures = document.createElement('a')
							$(aCultures).attr('href', '#swipe-cultures')
							$(aCultures).text('Cultures')
							$(liCultures).append(aCultures)
							$(ul).append(liCultures)

							var divCultures = document.createElement('div')
							$(divCultures).attr('id', 'swipe-cultures')
							$(divCultures).attr('class', 'col s12')

							var table = document.createElement('table')
							$(table).attr('class', 'bordered striped highlight')
							var tbody = document.createElement('tbody')
							$.each(data.DATA.assolement, function(k, v) {
								var tr = document.createElement('tr')
								var td = document.createElement('td')
								var str = (v.P1 != undefined) ? v.P1 : ''
								str += (v.P2 != undefined) ? (' - ' + v.P2) : ''
								str += (v.P3 != undefined) ? (' - ' + v.P3) : ''
								str += (v.P4 != undefined) ? (' - ' + v.P4) : ''
								str += (v.P5 != undefined) ? (' - ' + v.P5) : ''
								str += (v.P6 != undefined) ? (' - ' + v.P6) : ''
								str += (v.I1 != undefined) ? ((v.P1 != undefined) ? (' - ' + v.I1) : v.I1) : ''
								str += (v.I2 != undefined) ? (' - ' + v.I2) : ''
								str += (v.S1 != undefined) ? (' - ' +v.S1) : ''
								str += (v.S2 != undefined) ? ((v.P1 != undefined) ? (' - ' + v.S2) : v.S2) : ''
								$(td).text('{0} : {1}'.format(v.An_Rec, str))
								$(tr).append(td)
								$(tbody).append(tr)
							})
							$(table).append(tbody)
							$(divCultures).append(table)
							$('#interventions').append(divCultures)
						}

						$('ul.tabs').tabs()
					} else {
						Materialize.toast('Aucuns adventices trouvés pour cette culture', 3000)
					}
				} else {
					Materialize.toast('Aucunes données trouvées pour cette culture', 3000)
				}
			})
		} else {
			Materialize.toast('Veuillez sélectionner une culture.', 3000)
		}
	},
	buildPfChart: function(data, pf) {
		var margin = {
			top: 20,
			right: 100,
			bottom: 150,
			left: 0
		}

		var padding = {
			top: 50,
			left: 120
		}

		var width = ((parseInt(d3.select('section').style('width'), 10) - margin.left) - margin.right)
		var height = (parseInt(d3.select('section').style('height'), 10) - margin.top)

		var ptsAdv = []
		$.each(data.ptsAdvs[pf], function(k, v) {
			ptsAdv.push(v)
		})

		var grpsPtsAdv = []
		$.each(data.grpsPtsAdv, function(k, v) {
			grpsPtsAdv.push(v)
		})
		ptsAdv.push({x: 0, y: grpsPtsAdv.length, color: '', GroupeADV: '', radius: 0})

		var xValue = function(d) { return d.x }
		var xScale = d3.scale.linear()
			.domain([0, d3.max(ptsAdv, function(d) { return d.x })])
			.range([0, width])
		var xMap = function(d) { return xScale(xValue(d)) }

		var xAxis = d3.svg.axis()
			.scale(xScale)
			.orient("bottom")
			.ticks(d3.max(ptsAdv, function(d){ return d.x }))
			.tickFormat(function(d) {
				return (d > 0) ? ('Point {0}'.format(d)) : ''
			})

		var yValue = function(d) { return d.y }
		var yScale = d3.scale.linear()
			.domain([0, d3.max(ptsAdv, function(d){ return  d.y})])
			.range([height, 0])
		var yMap = function(d) { return yScale(yValue(d)) }

		var yAxis = d3.svg.axis()
			.scale(yScale)
			.orient("left")
			.innerTickSize(-width)
			.outerTickSize(0)
			.tickPadding(10)
			.ticks(d3.max(ptsAdv, function(d){ return d.y }))
			.tickFormat(function(d) {
				return grpsPtsAdv[d]
			})

		var svg = d3.select(".pointsChartsPtsAdventices").append("svg")
			.attr("width", ((width + margin.left) + margin.right) + padding.left)
			.attr("height", ((height + margin.top) + margin.bottom) + padding.top)
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")")

		svg.append("g")
			.attr("class", "x axis")
			.attr("transform", "translate(" + padding.left + ", " + (height + padding.top) + ")")
			.call(xAxis)

		svg.append("text")
			.attr("x", width)
			.attr("y", ((height + padding.top) - 10))
			.attr("class", "y axis")
			.attr("transform", "translate(" + padding.left + ", 0)")
			.attr("text-anchor", "middle")
			.text("Points d'adventices")

		svg.append("g")
			.attr("class", "y axis").attr("transform", "translate(" + padding.left + ", " + padding.top + ")")
			.call(yAxis)

		var circles = svg.selectAll("circle")
			.data(ptsAdv)
			.enter()
			.append("circle")
			.attr("cx", xMap)
			.attr("cy", yMap)
			.attr("r", function (d) { return (d.radius * 2) })
			.attr('stroke', 'black')
			.attr('stroke-width', 1)
			.style("fill", function(d) { return 'rgb({0})'.format(d.color) })
			.attr("transform", "translate(" + padding.left + ", " + padding.top + ")")
	},
	parcelle_travail_sol: function() {
		var that = this

		if(app.params.length > 0) {
			this.interventions(app.nom_court)

			var pr = app.modules.queries.get('itk/{0}/{1}'.format(app.params[0].IDPA, app.params[0].ANNEE))
			pr.done(function(data) {
				if (data.DATA != 'NOT_FOUND') {
					if (data.DATA.semis != 'NOT_FOUND') {
						// Met le bouton en valeur
						that.active('parcelle_travail_sol')

						$('#middle *').remove()

						$('#middle section').remove()
						var pointsChartSemis = document.createElement('section')
						$(pointsChartSemis).attr('class', 'pointsChartSemis')
						$(pointsChartSemis).attr('id', 'pointsChartSemis')
						$('#middle').append(pointsChartSemis)

						var margin = {
							top: 20,
							right: 200,
							bottom: 30,
							left: 0
						}

						var padding = {
							top: 50,
							left: 200
						}

						var width = ((parseInt(d3.select('section').style('width'), 10) - margin.left) - margin.right)
						var height = ((parseInt(d3.select('section').style('height'), 10) - margin.top) - margin.bottom)

						var datas = [{x: 0, y: 0, Nom_Operation: '', DateI: ''}]
						$.each(data.DATA.itks, function(k, v) {
							datas.push(v)
						})

						var dates = []
						$.each(data.DATA.dates, function(k, v) {
							dates.push(v)
						})

						datas.push({x: (dates.length - 1), y: 0, Nom_Operation: '', DateI: ''})

						var xValue = function(d) { return d.x }
						var xScale = d3.scale.linear()
							.domain([0, d3.max(datas, function(d) { return d.x })])
							.range([0, width])
						var xMap = function(d) { return xScale(xValue(d)) }

						var xAxis = d3.svg.axis()
							.scale(xScale)
							.orient("bottom")
							.ticks(d3.max(datas, function(d){ return d.x }))
							.tickFormat(function(d) {
								return dates[d]
							})

						var yValue = function(d) { return d.y }
						var yScale = d3.scale.linear()
							.domain([0, d3.max(datas, function(d){ return  d.y})])
							.range([height, 0])
						var yMap = function(d) { return yScale(yValue(d)) }

						var yAxis = d3.svg.axis()
							.scale(yScale)
							.orient("left")
							.innerTickSize(-width)
							.outerTickSize(0)
							.tickPadding(10)
							.ticks(d3.max(datas, function(d){ return d.y }))
							.tickFormat(function(d) {
								return datas[d].Nom_Operation
							})

						var svg = d3.select("section").append("svg")
							.attr("width", ((width + margin.left) + margin.right))
							.attr("height", ((height + margin.top) + margin.bottom) + padding.top)
							.attr("transform", "translate(" + margin.left + "," + margin.top + ")")

						svg.append("g")
							.attr("class", "x axis")
							.attr("transform", "translate(" + padding.left + ", " + (height + padding.top) + ")")
							.call(xAxis)

						svg.selectAll('.x.axis .tick text')
						.on('click', function(e) {
							that.detailItks(app.params[0].IDPA, dates[e].split('/').join('_'))
						})

						svg.append("g")
							.attr("class", "y axis")
							.attr("transform", "translate(" + padding.left + ", " + padding.top + ")")
							.call(yAxis)

						var circles = svg.selectAll("circle")
							.data(datas)
							.enter()
							.append("circle")
							.attr("cx", xMap)
							.attr("cy", yMap)
							.attr("r", function (d) { return d.radius })
							.style("fill", function(d) { return ('rgb({0})').format(d.Couleur) })
							.attr('stroke', 'black')
							.attr('stroke-width', 1)
							.attr("transform", "translate(" + padding.left + ", " + padding.top + ")")
							.on('click', function(e) {
								that.detailItks(app.params[0].IDPA, $(e)[0].DateI.split('/').join('_'))
							})

						var divTooltip = d3.select('section').append('div').attr('class', 'toolTip')
						circles.on('mouseover', function(d) {
							divTooltip.style('left', (((d3.event.pageX) - $('section').offset().left)) + 'px')
							divTooltip.style('top', ((d3.event.pageY -  $('section').offset().top) - 25) + 'px')
							
							if (d.Commentaire != null) {
								divTooltip.html(d.Commentaire)
								divTooltip.style('display', 'inline-block')
							}
						})
						.on('mouseleave', function(d) {
							currentElement.style.opacity = 1
							divTooltip.style('display', 'none')
							$('.toolTip').text('')
						})
					} else {
						Materialize.toast('Aucuns semis trouvés pour cette culture', 3000)
					}
				}
			})
		} else {
			Materialize.toast('Veuillez sélectionner une culture.', 3000)
		}
	},
	detailItks: function(idpa, date) {
		var that = this

		var pr = app.modules.queries.get('itk/details/{0}/{1}'.format(idpa, date.split('/').join('_')))
		pr.done(function(data) {
			if (data.DATA.itks != 'NOT_FOUND') {
				$('#middle table').remove()

				app.modules.builder.buildTable({
					// Initialisation de l'entête du tableau
					// Format JSON des données d'entête
					// la clé correspond au nom de la colonne de la base de données 
					// ou au nom donné pendant le traitement des données dans l'API
					// La valeur correspond au texte de l'entête
					header: {
						DateI: 'Intervention',
						Nom_Operation: 'Operation',
						S_W: 'Surface travaillée (Ha)',
						Profondeur: 'Profondeur', 
						Nom_Materiel: 'Materiel', 
						Nom_Tracteur: 'Tracteur'
					},
					// data est un tableau dans le quel nous lui passons les données
					data: data.DATA,
					// Nom du conteneur du tableau
					container: '#middle'
				})
			} else {
				Materialize.toast('Aucunes données trouvées.', 3000)
			}
		})
	},
	pf_recoltes: function() {
		var that = this

		if(app.params.length > 0) {
			// Requetes de récupération de données
			var pr = app.modules.queries.get('recoltes/pf/{0}/{1}'.format(app.params[0].IDPA, app.params[0].ANNEE))
			pr.done(function(data) {
				if (data.DATA.recoltes != 'NOT_FOUND') {

					// Met le bouton récoltes en valeur
					that.active('pf_recoltes')

					// Supprime l'ancien contenu de middle
					$('#middle *').remove()

					$('#middle section').remove()
					var barChartEffluent = document.createElement('section')
					$(barChartEffluent).attr('class', 'barChartEffluent')
					$(barChartEffluent).attr('id', 'barChartEffluent')
					$('#middle').append(barChartEffluent)

					var datas = data.DATA.recoltes

					var xData = ["RdtA", "RdtB"];
				 
					var margin = {
						top: 20,
						right: 50,
						bottom: 30,
						left: 50
					}

					var width = ((parseInt(d3.select('section').style('width'), 10) - margin.left) - margin.right)
					var height = ((parseInt(d3.select('section').style('height'), 10) - margin.top) - margin.bottom)
					 
					var x = d3.scale.ordinal()
							.rangeRoundBands([0, width])
					 
					var y = d3.scale.linear()
							.rangeRound([height, 0])
					 
					var xAxis = d3.svg.axis()
							.scale(x)
							.orient("bottom")

					var yAxis = d3.svg.axis()
							.scale(y)
							.orient("left")
							.ticks(5)

					var svg = d3.select("#barChartEffluent").append("svg")
							.attr("width", width + margin.left + margin.right)
							.attr("height", height + margin.top + margin.bottom)
							.append("g")
							.attr("transform", "translate(" + margin.left + "," + margin.top + ")")
					 
					var dataIntermediate = xData.map(function (c) {
						return datas.map(function (d) {
							return {x: d.Date, y: parseFloat(d[c]), color: d.Couleur, Code_E: d.Code_E, CODE: d.CODE, Leg_pc: d.Leg_pc}
						});
					});
					 
					var dataStackLayout = d3.layout.stack()(dataIntermediate)
					 
					x.domain(dataStackLayout[0].map(function (d) {
						return d.x
					}));
					 
					y.domain([0,
						d3.max(dataStackLayout[dataStackLayout.length - 1],
							function (d) { return d.y0 + d.y })
						])
					.nice();
					
					var layer = svg.selectAll(".stack")
						.data(dataStackLayout)
						.enter().append("g")
						.attr("class", "stack")
					
					var bar = layer.selectAll("rect")
						.data(function (d) {
							return d
						})
						.enter().append("rect")
						.attr("x", function (d) {
							return x(d.x)
						})
						.attr("y", function (d) {
							return y(d.y + d.y0)
						})
						.attr("height", function (d) {
							return y(d.y0) - y(d.y + d.y0)
						})
						.attr("width", x.rangeBand())
						.style("fill", function(d) {
							return 'rgb(' + d.color + ')'
						})
						.on('click', function(e) {
							$('#middle table').remove()
							var tableHeader = {}
							if (e.CODE.length > 0 && e.CODE == 'PG') {
								tableHeader = {
									Date: 'Date',
									Lieu: 'Lieu',
									surface: 'Surface (Ha)',
									PMG: 'PMG',
									PS: 'PS',
									RdtGrain_TMS_ha: 'Rendement grain TMS/Ha',
									Rdt_Paille_tmsha: 'Rendement paille TMS/Ha',
								}
							}

							if (e.CODE.length > 0 && e.CODE == 'P') {
								tableHeader = {
									Date: 'Date',
									Nom_Court: 'Lieu',
									surface: 'Surface (Ha)',
									TMSHa: 'TMS / Ha',
									Leg_pc: 'Taux de légumineux (%)',
								}
							}
							
							app.modules.builder.buildTable({
								// Initialisation de l'entête du tableau
								// Format JSON des données d'entête
								// la clé correspond au nom de la colonne de la base de données 
								// ou au nom donné pendant le traitement des données dans l'API
								// La valeur correspond au texte de l'entête
								header: tableHeader,
								// data est un tableau dans le quel nous lui passons les données
								data: (e.CODE.length > 0 && e.CODE == 'PG') ? data.DATA.pg[e.x] : data.DATA.p[e.x],
								// Nom du conteneur du tableau
								container: '#middle'
							})
						})

					layer.selectAll("text")
						.data(function (d) {
							return d
						})
						.enter()
						.append("text")
						.attr("x", function (d) {
							return x(d.x) + x.rangeBand() / 2
						})
						.attr("y", function (d) {
							return y(d.y + d.y0)
						})
						.attr("dy", "1em")
						.attr("text-anchor", "middle")
						.attr("font-size", "14px")
						.attr("fill", "black")
						.attr("transform", function(d) { return "translate(0, -20)"; })
						.text(function(d) {
							return (d.Code_E != undefined) ? (d.Code_E + ' ' + (d.Leg_pc.length > 0) ? d.Leg_pc : '') : ''
						})
					 
					svg.append("g")
					   .attr("class", "x axis")
					   .attr("transform", "translate(0," + height + ")")
					   .call(xAxis)

					svg.append("g")
						.attr("class", "y axis")
						.call(yAxis)

					svg.append("text")
						.attr("x", 20)
						.attr("y", 0)
						.attr("text-anchor", "middle")
						.text("tMS")

					if (data.DATA.legende != 'NOT_FOUND') {
						$('#interventions *').remove()
						var ul = document.createElement('ul')
						$(ul).attr('id', 'tabs-swipe')
						$(ul).attr('class', 'tabs')
						$('#interventions').append(ul)

						var liLegend = document.createElement('li')
						$(liLegend).attr('class', 'tab col s12 active')
						var aLegend = document.createElement('a')
						$(aLegend).attr('href', '#swipe-legende')
						$(aLegend).text('Groupes')
						$(liLegend).append(aLegend)
						$(ul).append(liLegend)

						var divLegend = document.createElement('div')
						$(divLegend).attr('id', 'swipe-legende')
						$(divLegend).attr('class', 'col s12')

						var table = document.createElement('table')
						$(table).attr('class', 'bordered striped highlight')
						var tbody = document.createElement('tbody')
						$.each(data.DATA.legende, function(k, v) {
							var tr = document.createElement('tr')
							var td = document.createElement('td')
							var a = document.createElement('a')
							$(a).text('{0}'.format(v.C_Produit))
							$(a).attr('style', 'color: #000;')
							$(tr).attr('style', 'background-color: rgb({0});'.format(v.Couleur))
							$(td).append(a)
							$(tr).append(td)
							$(tbody).append(tr)
						})
						$(table).append(tbody)
						$(divLegend).append(table)
						$('#interventions').append(divLegend)
					}

					$('ul.tabs').tabs()
				} else {
					Materialize.toast('Aucunes données trouvées pour cette parcelle.', 3000)
				}
			})
		} else {
			Materialize.toast('Veuillez sélectionner une culture.', 3000)
		}
	}
}


