// Plugin de la couche zone_de_ferti
app.plg.plugin = {
	// Initialisation du plugin
	init: function() {
		// On masque la couche
		app.layersHide[this.name()] = true
	},
	// Fonction qui retourne le nom du plugin
	name: function() {
		return 'zone_de_ferti'
	},
	// Fonction indiquant si la couche possède un bouton
	// true si oui, false sinon
	button: function() {
		return true
	},
	// Fonction attribuant une couleur au bouton si la couche en possède un
	color: function() {
		return 'red'
	},
	// Fonction d'affichage de la couche
	drawLayer: function(name) {
		var that = this

		$('#openTab').fadeOut('fast')

		// On masque toutes les couches qui peuvent être affichées
		for (var i = 0; i < PLUGINS.length; i++) {
			var nom = PLUGINS[i]
			if (nom !== that.name()) {
				if (app.map.hasLayer(app.shapeFiles[PLUGINS[i]])) {
					app.map.removeLayer(app.shapeFiles[nom])
					app.layersHide[nom] = true
				}
			}
		}

		// si la couche n'existe pas on la créer
		if (app.shapeFiles[that.name()] === undefined) {
			app.shapeFiles[that.name()] = new L.Shapefile(('{0}/{1}/{2}').format(app.shapeFilesDirectory, that.name(), that.name()), {
				style: app.shapeStyle,
				// Affichage des points sur la couche
				pointToLayer: function (feature, latlng) {
					return L.circleMarker(latlng, {
						radius: 8,
						fillColor: "#27ae60",
						fillOpacity: 0.8,
						color: "#000",
						weight: 1,
						opacity: 1
					})
				},
				// Parcour tous les points pour attribuer des fonctions
				onEachFeature: function(feature, layer) {
					if (feature.geometry) {
						// Changement opacité au passage de la souris	
						$(layer).hover(function(e) {
							e.target.setStyle({ fillOpacity: 1 })
						}, function(e) {
							e.target.setStyle({ fillOpacity: 0.8 })
						})
						
						// Affichage de données au clique
						layer.addEventListener('mousedown', function(e) {
							if (app.currentLayer != undefined) {
								app.currentLayer.setStyle({ fillOpacity: 0.2 })
							}

							app.currentLayer = e.target
							e.target.setStyle({ fillOpacity: 1 })

							app.modules.actions.displayPopup()

							var pr = app.modules.queries.get('point_ferti/view/{0}'.format(feature.properties.CODE))
							pr.done(function(data) {
								var p = document.createElement('p')
								$(p).text("Parcelle : {0}".format(data.DATA.Parcelle))
								$('#infosPa').append(p)

								p = document.createElement('p')
								$(p).text("{0} / {1}".format(data.DATA.T_S_descrip, data.DATA.Substr_descrip))
								$('#infosPa').append(p)

								p = document.createElement('p')
								$(p).text("Profondeur : {0}".format(data.DATA.Prof_H_Descrip))
								$('#infosPa').append(p)
							})
							
							return false;
						})
					}
				}
			})
			// Ajout de la couche a la carte
			app.shapeFiles[that.name()].addTo(app.map)
			// Rendre la couche visible
			app.layersHide[that.name()] = false
		} else {
			// si la couche est visible
			if (app.layersHide[that.name()] === false) {
				// On retire la couche de la carte
				app.map.removeLayer(app.shapeFiles[that.name()])
				// On masque la couche
				app.layersHide[that.name()] = true

				// On reajoute le parcellaire et on dit qu'il n'est pas masqué
				app.shapeFiles['parcellaire'].addTo(app.map)
				app.couche = 'parcellaire'
				app.layersHide['parcellaire'] = false
			} else {
				// On ajoute la couche a la crarte
				app.shapeFiles[that.name()].addTo(app.map)
				app.layersHide[that.name()] = false
			}
		}
	}
}