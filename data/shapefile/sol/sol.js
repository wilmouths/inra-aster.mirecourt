// Plugin de la couche sol	
app.plg.plugin = {
	// Initialisation du plugin
	init: function() {
		// On masque la couche
		app.layersHide[this.name()] = true
	},
	// Fonction qui retourne le nom du plugin
	name: function() {
		return 'sol'
	},
	// Fonction indiquant si la couche possède un bouton
	// true si oui, false sinon
	button: function() {
		return true
	},
	// Fonction attribuant une couleur au bouton si la couche en possède un
	color: function() {
		return 'green'
	},
	// Fonction d'affichage de la couche
	drawLayer: function(nme) {
		var that = this

		opacity = 0.8

		$('#openTab').fadeIn('slow')

		// On masque toutes les couches qui peuvent être affichées
		for (var i = 0; i < PLUGINS.length; i++) {
			var nom = PLUGINS[i]
			if (nom !== that.name()) {
				if (app.map.hasLayer(app.shapeFiles[PLUGINS[i]])) {
					app.map.removeLayer(app.shapeFiles[nom])
					app.layersHide[nom] = true
				}
			}
		}

		var sols = this.sol()

		// si la couche n'existe pas on la créer
		if (app.shapeFiles[that.name()] === undefined) {
			// On definit la couche courrante sur sol
			app.couche = 'sol'

			app.shapeFiles[that.name()] = new L.Shapefile(('{0}/{1}/{2}').format(app.shapeFilesDirectory, that.name(), that.name()), {
				style: app.shapeStyle,
				// Parcour tous les points pour attribuer des fonctions
				onEachFeature: function(feature, layer) {
					var text_surf = sols[feature.properties.text_surf]
					var substratum = sols[feature.properties.substratum]

					// Ajout d'un pattern et de sa couleur
					if (text_surf !== undefined && substratum !== undefined) {
						var pattern = null
								
						if (substratum.Fond_Subs === 'LigneH') {
							pattern = new L.StripePattern({
								color: 'rgb({0})'.format(text_surf.Couleur_Text),
								fillColor: 'rgb({0})'.format(text_surf.Couleur_Text),
								fillOpacity: opacity
							})
							pattern.addTo(app.map)
						}
						if (substratum.Fond_Subs === 'Oblique') {
							pattern = new L.StripePattern({
								color: 'rgb({0})'.format(text_surf.Couleur_Text),
								fillColor: 'rgb({0})'.format(text_surf.Couleur_Text),
								angle: 90,
								fillOpacity: opacity
							})
							pattern.addTo(app.map)
						}
						if (substratum.Fond_Subs === 'Croix') {
							var shape = new L.PatternPath({
								d: 'M10,10 l0,10 M5,15 l10,0',
								fill: true,
								color: 'rgb({0})'.format(text_surf.Couleur_Text),
								fillColor: 'rgb({0})'.format(text_surf.Couleur_Text),
								fillOpacity: opacity
							})
							var pattern = new L.Pattern({
								width: 20, 
								height: 25
							})
							pattern.addShape(shape)
							pattern.addTo(app.map)
						}
						if (substratum.Fond_Subs === 'Cadrillage') {
							var shape = new L.PatternRect({
								width: 10,
								height: 10,
								rx: 0,
								ry: 0,
								fill: true,
								color: 'rgb({0})'.format(text_surf.Couleur_Text),
								fillColor: 'rgb({0})'.format(text_surf.Couleur_Text),
								fillOpacity: opacity
							})
							var pattern = new L.Pattern({
								width: 10, 
								height: 10
							})
							pattern.addShape(shape)
							pattern.addTo(app.map)
						}
						if (substratum.Fond_Subs === 'Cercle') {
							var shape = new L.PatternCircle({
								x: 8,
								y: 8,
								radius: 5,
								fill: true,
								color: 'rgb({0})'.format(text_surf.Couleur_Text),
								fillColor: 'rgb({0})'.format(text_surf.Couleur_Text),
								fillOpacity: opacity
							})
							pattern = new L.Pattern({
								width: 25, 
								height: 25
							})
							pattern.addShape(shape)
							pattern.addTo(app.map)
						}
						layer.setStyle(
							{
								fillPattern: pattern,
								fillOpacity: opacity
							}
						)

						if (feature.geometry) {
							// Changement opacite au passage de la souris et affichage de données
							$(layer).hover(function(e) {
								e.target.setStyle({ fillOpacity: 0.65 })

								app.modules.actions.displayPopup()

								Object.keys(feature.properties).map(function(k) {
									if (k === 'LABEL') {
										var p = document.createElement('p')
										$(p).text(("{0} / {1}").format(text_surf.T_S_descrip, substratum.Substr_descrip))
										$('#infosPa').append(p)
									}
								})
							}, function(e) {
								e.target.setStyle({ fillOpacity: 0.8 })
							})
						}
					} else {
						layer.setStyle({ 
							fillOpacity: 0.2,
						})
					}
				}
			})
			// Ajout de la couche a la carte
			app.shapeFiles[that.name()].addTo(app.map)
			// Rendre la couche visible
			app.layersHide[that.name()] = false
		} else {
			// si la couche est visible
			if (app.layersHide[that.name()] === false) {
				// On retire la couche de la carte
				app.map.removeLayer(app.shapeFiles[that.name()])
				// On masque la couche
				app.layersHide[that.name()] = true

				// On reajoute le parcellaire et on dit qu'il n'est pas masqué
				app.shapeFiles['parcellaire'].addTo(app.map)
				app.couche = 'parcellaire'
				app.layersHide['parcellaire'] = false
			} else {
				// On ajoute la couche a la crarte
				app.shapeFiles[that.name()].addTo(app.map)
				app.layersHide[that.name()] = false
				// On definit la couche courrante sur sol
				app.couche = 'sol'
			}
		}		
	},
	// Fonction qui recupere les données du sol (Trame + Couleur)
	sol: function() {
		var datas = []
		var pr = app.modules.queries.get('sol/')
		pr.done(function(data) {
			data.DATA.forEach(function(d) {
				if (d.ID_TS !== undefined) {
					datas[d.Text_surf] = d
				}
				if (d.ID_SUBSTR !== undefined) {
					datas[d.Substratum] = d
				}
			})
		})
		return datas
	},
	// Construction et affichage du tableau de details du sol
	displayRecap: function() {
		$('#recap *').remove()
		
		// on masque le tableau de l'assolement
		if (document.getElementById('assolementDetails') !== null) {
			$('#assolementDetails').fadeOut('fast')
			$('#assolementDetailsTitle').fadeOut('fast')
		}

		// si le table n'existe pas on le creer
		if (document.getElementById('texturesSol') === null) {
			var pr = app.modules.queries.get('sol/details/{0}'.format(app.currentYear))
			pr.done(function(data) {
				var h4 = document.createElement('h4')
				$(h4).text('Textures')
				$(h4).attr('id', 'texturesSolTitle')
				$('#recap').append(h4)

				var ul = document.createElement('ul')
				$(ul).attr('class', 'collapsible')
				$(ul).attr('id', 'texturesSol')
				$(ul).attr('data-collapsible', 'accordion')

				$.each(data.DATA, function(k, v) {
					if (v.surface > 0) {
						var li = document.createElement('li')

						var header = document.createElement('div')
						$(header).attr('class', 'collapsible-header')
						$(header).attr('style', 'background: rgb({0}); color: rgb({1})'.format(
							v.Couleur_Text,
							(v.Couleur_Text != '255,255,255') ? '255,255,255' : '0,0,0'
						))

						var tableHeader = document.createElement('table')
						$(tableHeader).attr('class', 'centered')

						var tbodyHeader = document.createElement('tbody')
						var trHeader = document.createElement('tr')
						var tdKeyHeader = document.createElement('td')
						$(tdKeyHeader).attr('style', 'width: 33%;')
						$(tdKeyHeader).text(v.T_S_descrip)
						$(trHeader).append(tdKeyHeader)

						var tdValueHeader = document.createElement('td')
						$(tdValueHeader).attr('style', 'width: 33%;')
						$(tdValueHeader).text('{0} Ha'.format(v.surface))
						$(trHeader).append(tdValueHeader)

						var tdPercentHeader = document.createElement('td')
						$(tdPercentHeader).attr('style', 'width: 33%;')
						$(tdPercentHeader).text('{0} %'.format(v.percent))
						$(trHeader).append(tdPercentHeader)
						$(tbodyHeader).append(trHeader)
						$(tableHeader).append(tbodyHeader)

						$(header).append(tableHeader)
						$(li).append(header)

						var body = document.createElement('div')
						$(body).attr('class', 'collapsible-body')
						$(body).attr('style', 'padding: 0;')

						var tableBody = document.createElement('table')
						$(tableBody).attr('class', 'centered striped highlight bordered')
						var tbodyBody = document.createElement('tbody')

						$.each(v.textures, function(k, w) {
							if (w > 0) {
								var trBody = document.createElement('tr')

								var tdBodySubstratumNom = document.createElement('td')
								$(tdBodySubstratumNom).text(k)
								$(tdBodySubstratumNom).attr('style', 'width: 33%;')
								$(trBody).append(tdBodySubstratumNom)

								var tdBodySubstratumSurface = document.createElement('td')
								$(tdBodySubstratumSurface).text('{0} Ha'.format(w))
								$(tdBodySubstratumSurface).attr('style', 'width: 33%;')
								$(trBody).append(tdBodySubstratumSurface)

								var tdBodyPercent = document.createElement('td')
								var percent = ((w * 100) / v.surface)
								$(tdBodyPercent).text('{0} %'.format(percent.toFixed(2)))
								$(tdBodyPercent).attr('style', 'width: 33%;')
								$(trBody).append(tdBodyPercent)

								$(tbodyBody).append(trBody)
							}
						})

						$(tableBody).append(tbodyBody)
						$(body).append(tableBody)

						$(li).append(body)
						
						$(ul).append(li)
						$(ul).collapsible()
					}
				})
				$('#recap').append(ul)
			})
		} else {
			// s'il existe, on l'affiche, ça évite les appels a l'api inutiles
			$('#texturesSol').fadeIn('fast')
			$('#texturesSolTitle').fadeIn('fast')
		}
	}
}