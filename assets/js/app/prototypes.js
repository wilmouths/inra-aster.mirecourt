$(document).ready(function() {
	String.prototype.format = function () {
		var str = this
		for (var i = 0; i < arguments.length; i++) {
			str = str.replace('{' + i + '}', arguments[i])
		}
		return str
	}
	String.prototype.ucfirst = function() {
		return this.charAt(0).toUpperCase() + this.slice(1);
	},
	String.prototype.isEmpty = function() {
    	return (this.length === 0 || !this.trim());
	}
})