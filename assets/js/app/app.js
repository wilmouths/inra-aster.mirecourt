// Initialisation de l'application
var app = (function() {
	var shapeFiles, dataDirectory, shapeFilesDirectory, shapeStyle, map, currentLayer, layersHide
	var endpoint
	var lat, lng
	var plg, pluginsLoaded
	var nom_court, parcelle
	var couche
	var option, params
	var currentYear
	return {
		modules: {}
	}
})()

// définitions des premiers modules
app.modules = (function() {
	return {
		// modules d'initialiation
		// affecte des valeurs aux variables
		init: function() {
			app.shapeFilesDirectory = 'data/shapefile'
			app.dataDirectory = 'data'
			app.option = 'parcelle'
			app.shapeFiles = []
			app.layersHide = []
			app.params = []

			// inclusion du fichier de scripts au DOM
			var script = document.createElement('script')
			$(script).attr('src', ('{0}/{1}/parcellaires.js').format(app.shapeFilesDirectory, 'parcellaire'))
			$('body').append(script)

			app.currentYear = PARCELLAIRES[PARCELLAIRES.length - 1]
			$('#year').text(app.currentYear)

			app.endpoint = "{0}api/".format(location.href)

			app.couche = 'parcellaire'

			app.lat = 48.289049
			app.lng = 6.108585

			// Clique au bouton centré
			document.querySelector('#center').addEventListener('click', function() {
				app.map.setView(new L.LatLng(app.lat, app.lng), 14)
			})

			// Clique au bouton fermer de la popup
			document.querySelector('#close').addEventListener('click', function() {
				$('#infosPa').fadeOut('slow')

				app.currentLayer.setStyle({ fillOpacity: 0.8 })
			})

			// Clique au bouton centré
			document.querySelector('#reload').addEventListener('click', function() {
				var year = new Date().getFullYear()
				if (app.currentYear !== year) {
					app.currentYear = year
					app.nom_court = app.parcelle.nom_court
					$('#parcelle').text('{0} ({1}Ha)'.format(app.parcelle.nom, parseFloat(app.parcelle.surface).toFixed(2)))
					$('#year').text(app.currentYear)
					app.modules.changeBackground(PARCELLAIRES[PARCELLAIRES.length - 1])
				}
			})

			// Clique au bouton effacer
			// Recadre la carte, affiche que le parcellaire
			document.querySelector('#effacer').addEventListener('click', function() {
				$('#infosPa').fadeOut('slow')
				$('#openTab').fadeIn('slow')

				app.map.setView(new L.LatLng(app.lat, app.lng), 14)

				for (var i = 0; i < PLUGINS.length; i++) {
					var nom = PLUGINS[i]
					if (app.layersHide[nom] === false) {
						if (app.map.hasLayer(app.shapeFiles[PLUGINS[i]])) {
							app.map.removeLayer(app.shapeFiles[nom])
							app.layersHide[nom] = true
						}
					}
				}

				app.couche = 'parcellaire'
				if (app.layersHide['parcellaire'] === true) {
					app.shapeFiles['parcellaire'].addTo(app.map)
					app.layersHide['parcellaire'] = false
				}
			})

			// Clique au bouton de carte (haut gauche)
			// reviens sur la cartographie
			document.querySelector('#displayMap').addEventListener('click', function(e) {
				e.preventDefault()
				$('#datas').fadeOut('slow')
				$('#displayMap').fadeOut('slow')
				$('#displayPhotos').fadeOut('slow')
				$('#floatingActionButton').fadeIn('slow')
				$('#btnCenter').fadeIn('slow')
				$('#map').fadeIn('slow')
				$('#openTab').fadeIn('slow')
				$('#parcelle').text('')
				$('#systeme').addClass('systeme')
			});

			// Clique au [?] permet d'ouvrir la popup d'assolement et de textures
			document.querySelector("#openTab").addEventListener('click', function(e) {
				e.preventDefault()
				if (app.couche == 'parcellaire') {
					app.pluginsLoaded['parcellaire'].displayRecap()
				} else if (app.couche == 'sol') {
					app.pluginsLoaded['sol'].displayRecap()
				}
			})

			// Initialisation de la carte
			app.map = L.map('map', {
				center: [app.lat, app.lng], 
				zoom: 14
			})

			// Définition du zoom [-] a 14
			app.map.options.minZoom = 14;

			// Définition du style de base des cpuches
			app.shapeStyle = {
				weight: 1,
				opacity: 0.2,
				fillOpacity: 0.2
			}

			// Définition du chemin par défaut des images
			L.Icon.Default.imagePath = 'assets/img';

			// Ajout du fond de carte a la carte
			// http://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}
			L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
				attribution: 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
				maxZoom: 18
			}).addTo(app.map)

			// Ajout d'un parcellaire vierge en fond
			app.shapeStyle.fillOpacity = 0.2
			app.shapeFiles['background'] = new L.Shapefile(
				('{0}/parcellaire/couches/{1}/{2}_{3}').format(app.shapeFilesDirectory, app.currentYear, 'parcellaire', app.currentYear), {
				style: app.shapeStyle
			})
			app.shapeFiles['background'].addTo(app.map)

			// Chargement des plugins
			app.plg = {}
			app.pluginsLoaded = []
			var includes = {
				'plg': {'items': PLUGINS, 'callback': function () {
					for (var plugin in app.plg) {
						app.plg[plugin].init()
						app.pluginsLoaded[app.plg[plugin].name()] = app.plg[plugin]
					}
				}}
			}

			// Iteration des plugins
			// Initialisation des plugins
			for (var packageName in includes) {
				var pkg = includes[packageName]
				var items = pkg.items
				var callback = 'callback' in pkg ? pkg.callback : function() {}

				for (var i = 0; i < items.length; i++) {
					var pluginName = items[i]
					var scriptPath = '{0}/{1}/{2}.js'.format(app.shapeFilesDirectory, pluginName, pluginName)

					var script = document.createElement('script')
					$(script).attr('src', scriptPath)
					$('body').append(script)

					callback()
					if (app.pluginsLoaded[pluginName].button()) {
						app.modules.builder.buildShapeBtn(pluginName)
					}
				}
			}
		},
		changeBackground: function(year) {
			app.map.removeLayer(app.shapeFiles['background'])
			app.shapeFiles['background'] = new L.Shapefile(
				('{0}/parcellaire/couches/{1}/{2}_{3}').format(app.shapeFilesDirectory, year, 'parcellaire', year), {
				style: {
					weight: 1,
					opacity: 0.2,
					fillOpacity: 0.2
				}
			})
			app.shapeFiles['background'].addTo(app.map)
		}
	}
})()

// Modules de construction global
app.modules.builder = (function() {
	return {
		//	Fonction permettant la construction des boutons correspondant aux couches
		buildShapeBtn: function(name) {
			var img = document.createElement('img')
			$(img).attr('src', '{0}/{1}/{2}.png'.format(app.shapeFilesDirectory, name, name))
			$(img).attr('width', '28')
			$(img).addClass('imgBtn')

			var i = document.createElement('i')
			$(i).append(img)

			var a = document.createElement('a')
			$(a).attr('class', 'btn-floating {0} tooltipped'.format(app.pluginsLoaded[name].color()))
			$(a).attr('data-position', 'top')
			$(a).attr('data-delay', '50')

			$(a).attr('data-tooltip', (name.indexOf('_') !== -1) ? name.replace(new RegExp('_', 'g'), ' ').ucfirst() : name.ucfirst())
			$(a).attr('id', name)

			a.addEventListener('click', function() {
				app.pluginsLoaded[name].drawLayer()
			})
			$(a).append(i)

			var li = document.createElement('li')
			$(li).append(a)

			$('#btnShape').prepend(li)
		},
		// Fonction permettant la construction de tableau
		buildTable: function(params) {
			// Creation  de la table
			var table = document.createElement('table')
			$(table).attr('class', 'bordered striped highlight centered')

			// Creation de l'entete
			var thead = document.createElement('thead')
			var tr = document.createElement('tr')

			// Ajouts des entetes
			$.each(params.header, function(k, v) {
				var th = document.createElement('th')
				$(th).text(v)
				$(th).attr('style', 'width: calc(100% / {0});'.format(parseInt(Object.keys(params.header).length)))
				$(tr).append(th)
			})
			$(thead).append(tr)
			$(table).append(thead)
			
			// Creation du corps
			var tbody = document.createElement('tbody')

			// Creation des colonnes
			$.each(params.data, function(k, v) {
				var tr = document.createElement('tr')

				// Ajouts des valeurs
				$.each(params.header, function(e, f) {
					var td = document.createElement('td')
					$(td).text(v[e])
					$(td).attr('style', 'width: calc(100% / {0});'.format(parseInt(Object.keys(params.header).length)))
					$(tr).append(td)
				})

				$(tbody).append(tr)
			})
			$(table).append(tbody)
			
			// Ajout sur la page
			$(params.container).append(table)
		}
	}
})()

// Module de requetage (GET / POST)
app.modules.queries = (function() {
	return {
		// Permet le requetage (GET)
		// Permet la récupération de données sur l'API
		get: function(url) {
			var pr = $.ajax((app.endpoint + url), {
				type: "GET",
				dataType: "json",
				context: this,
				xhrFields: {
					withCredentials: true
				}
			})
			console.log((app.endpoint + url))
			pr.fail(function(jqXHR, status, error) {
				$('#textModal').text('Impossible de charger les données')
				$('#modalNoData').modal('open')
				console.log((app.endpoint + url))
			})
			return pr
		},
		// Permet le requetage (POST)
		// Permet l'insertion de données sur l'API
		post: function(url, data) {
			var pr = $.ajax((app.endpoint + url), {
				type: "POST",
				dataType: "json",
				context: this,
				xhrFields: {
					withCredentials: true
				},
				data: data
			})
			pr.fail(function(jqXHR, status, error) {
				$('#textModal').text('Impossible d\'envoyer les données')
				$('#modalNoData').modal('open')
			})
			return pr
		}
	}
})()

// Module d'actions globales
app.modules.actions = (function() {
	return {
		// Permet l'affichage de la popup (haut droit)
		displayPopup: function() {
			$('#modalButtons a').remove()
			$('#infosPa p').remove()
			$('#infosPa').fadeIn('slow')
		},
		// Permet de masquer la popup (haut droit)
		hidePopup: function() {
			$('#infosPa').fadeOut('slow')
		},
		// Permet d'afficher/masquer différents boutons
		displayData: function() {
			$('#map').fadeOut('slow')
			$('#displayMap').fadeIn('slow')
			$('#floatingActionButton').fadeOut('slow')
			$('#datas').fadeIn('slow')
			$('#infosPa').fadeOut('slow')
		}
	}
})()