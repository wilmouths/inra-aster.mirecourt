﻿using System;
using System.ComponentModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Windows.Forms;

namespace Application_Web___Updater
{
    public partial class Updater : Form
    {
        public Updater()
        {
            InitializeComponent();

            this.maj.Enabled = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;       
        }

        private void fbd_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.DefaultExt = ".zip";
            ofd.Filter = "Zip File (.zip)|*.zip";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                this.path.Text = ofd.FileName;
                this.maj.Enabled = true;
            }
        }

        private void maj_Click(object sender, EventArgs e)
        {
            BackgroundWorker bgWorker = new BackgroundWorker();
            bgWorker.DoWork += new DoWorkEventHandler(bgWorkerDoWorker);
            bgWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgWorkerRunWorkerCompleted);
            bgWorker.WorkerReportsProgress = false;
            bgWorker.WorkerSupportsCancellation = true;

            if (bgWorker.IsBusy != true)
            {
                bgWorker.RunWorkerAsync();
            }

        }

        void bgWorkerRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((e.Cancelled == true))
            {
                MessageBox.Show("L'extraction à été terminée annulée.", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (!(e.Error == null))
            {
                MessageBox.Show("L'extraction est terminée.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show("L'extraction est terminée.", "Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        void bgWorkerDoWorker(object sender, DoWorkEventArgs e)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo("c:\\");
            DirectoryInfo[] folders = directoryInfo.GetDirectories("*wamp*");
            string wamp = folders.First().FullName;

            if (Directory.Exists(String.Format("{0}\\www\\inra-aster.mirecourt", wamp)))
            {
                Directory.Delete(String.Format("{0}\\www\\inra-aster.mirecourt", wamp), true);
            }

            ZipFile.ExtractToDirectory(this.path.Text, String.Format("{0}\\www\\inra-aster.mirecourt", wamp));
        }

        private void path_TextChanged(object sender, EventArgs e)
        {
            if (this.path.Text.Length == 0)
            {
                this.maj.Enabled = false;
            } else
            {
                this.maj.Enabled = true;
            }
        }
    }
}
