﻿namespace Application_Web___Updater
{
    partial class Updater
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Updater));
            this.maj = new System.Windows.Forms.Button();
            this.fbd = new System.Windows.Forms.Button();
            this.path = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // maj
            // 
            this.maj.Location = new System.Drawing.Point(12, 40);
            this.maj.Name = "maj";
            this.maj.Size = new System.Drawing.Size(461, 23);
            this.maj.TabIndex = 0;
            this.maj.Text = "Mettre à jour";
            this.maj.UseVisualStyleBackColor = true;
            this.maj.Click += new System.EventHandler(this.maj_Click);
            // 
            // fbd
            // 
            this.fbd.Location = new System.Drawing.Point(444, 14);
            this.fbd.Name = "fbd";
            this.fbd.Size = new System.Drawing.Size(29, 20);
            this.fbd.TabIndex = 1;
            this.fbd.Text = "...";
            this.fbd.UseVisualStyleBackColor = true;
            this.fbd.Click += new System.EventHandler(this.fbd_Click);
            // 
            // path
            // 
            this.path.Location = new System.Drawing.Point(12, 14);
            this.path.Name = "path";
            this.path.Size = new System.Drawing.Size(426, 20);
            this.path.TabIndex = 2;
            this.path.TextChanged += new System.EventHandler(this.path_TextChanged);
            // 
            // Updater
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 74);
            this.Controls.Add(this.path);
            this.Controls.Add(this.fbd);
            this.Controls.Add(this.maj);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Updater";
            this.Text = "Updater";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button maj;
        private System.Windows.Forms.Button fbd;
        private System.Windows.Forms.TextBox path;
    }
}

