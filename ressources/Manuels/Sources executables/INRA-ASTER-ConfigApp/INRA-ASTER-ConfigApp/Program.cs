﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace INRA_ASTER_ConfigApp
{
    class Program
    {

        static void Main(string[] args)
        {
            String path = "c:";

            //Recherche des dossiers
            DirectoryInfo directoryInfo = new DirectoryInfo(String.Format("{0}\\", path));
            DirectoryInfo[] folders = directoryInfo.GetDirectories("*wamp*");
            String wamp = folders.First().ToString();

            directoryInfo = new DirectoryInfo(String.Format("{0}\\{1}\\bin\\apache", path, wamp));
            folders = directoryInfo.GetDirectories("*apache*");
            foreach(DirectoryInfo apache in folders)
            {
                // Configuration de l'hote virtuel
                configVHost(apache.FullName, wamp);

                // Configuration de la reecriture d'url
                configUrlRewrite(apache.FullName);

                // Redemarrage d'apache
                restartWampapache(apache.FullName, wamp);
            }

            directoryInfo = new DirectoryInfo(String.Format("{0}\\{1}\\bin\\php", path, wamp));
            folders = directoryInfo.GetDirectories("*php*");
            foreach (DirectoryInfo php in folders)
            {
                // Configuration de php
                configPhpPdoOdbc(php.FullName);
            }

            if (!Directory.Exists(String.Format("{0}\\{1}\\www\\inra-aster.mirecourt", path, wamp)))
            {
                Console.WriteLine("Création du dossier inra-aster.mirecourt ...");
                Directory.CreateDirectory(String.Format("{0}\\{1}\\www\\inra-aster.mirecourt", path, wamp));
            }

            Console.WriteLine("Appuyez sur n'importe quelle touche pour fermer le programme.");
            Console.ReadKey();
        }

        private static void configVHost(String path, String wamp)
        {
            Console.WriteLine("Configuration de l'hôte virtuel (apache) ...");

            // Changement dans la config (httpd.conf)
            File.WriteAllText(
                String.Format("{0}\\conf\\httpd.conf", path),
                Regex.Replace(
                    File.ReadAllText(
                        String.Format("{0}\\conf\\httpd.conf", path)
                    ),
                    "#LoadModule vhost_alias_module modules/mod_vhost_alias.so",
                    "LoadModule vhost_alias_module modules/mod_vhost_alias.so"
                )
            );
            File.WriteAllText(
                string.Format("{0}\\conf\\httpd.conf", path),
                Regex.Replace(
                    File.ReadAllText(
                        string.Format("{0}\\conf\\httpd.conf", path)
                    ),
                    "#Include conf/extra/httpd-vhosts.conf",
                    "Include conf/extra/httpd-vhosts.conf"
                )
            );


            if (!File.ReadAllText(String.Format("{0}\\conf\\extra\\httpd-vhosts.conf", path)).Contains("ServerName inra-aster.mirecourt"))
            {
                Console.WriteLine("Ajouts de la configuration du vHost (apache) ...");
                StringBuilder sb = new StringBuilder();
                sb.Append("\r\n\r").Append("<VirtualHost *:80>");
                sb.Append("\r\n").Append("\tServerName inra-aster.mirecourt");
                sb.Append("\r\n").Append(String.Format("\tDocumentRoot {0}/{1}/www/inra-aster.mirecourt", path, wamp));
                sb.Append("\r\n").Append(String.Format("\t<Directory \"{0}/{1}/www/inra-aster.mirecourt/\">", path, wamp));
                sb.Append("\r\n").Append("\t\tOptions +Indexes +Includes +FollowSymLinks +MultiViews");
                sb.Append("\r\n").Append("\t\tAllowOverride All");
                sb.Append("\r\n").Append("\t\tRequire local");
                sb.Append("\r\n").Append("\t</Directory>");
                sb.Append("\r\n").Append("</VirtualHost>");
                File.AppendAllText(
                    String.Format("{0}\\conf\\extra\\httpd-vhosts.conf", path),
                    sb.ToString()
                );
            }

            if (!File.ReadAllText(String.Format("c:\\Windows\\System32\\drivers\\etc\\hosts")).Contains("127.0.0.1 inra-aster.mirecourt"))
            {
                Console.WriteLine("Configuration de l'hôte virtuel (machine) ...");
                // Changement dans le fichier host de la machine
                File.AppendAllText(
                    String.Format("{0}\\Windows\\System32\\drivers\\etc\\hosts", path),
                    "\r\n127.0.0.1 inra-aster.mirecourt"
               );
            }
            
        }

        private static void configUrlRewrite(String path)
        {
            Console.WriteLine("Configuration de la réécriture d'url ...");

            File.WriteAllText(
                String.Format("{0}\\conf\\httpd.conf", path),
                Regex.Replace(
                    File.ReadAllText(
                        String.Format("{0}\\conf\\httpd.conf", path)
                    ),
                    "#LoadModule rewrite_module modules/mod_rewrite.so",
                    "LoadModule rewrite_module modules/mod_rewrite.so"
                )
            );
        }

        private static void restartWampapache(String path, String wamp)
        {
            // Changement de dossier (c:/ -> c:/wamp/bin/apache/apacheX/bin/)
            try
            {
                Console.WriteLine("Redemarrage du serveur apache ...");

                System.Diagnostics.Process process = new System.Diagnostics.Process();
                process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                process.StartInfo.FileName = "cmd.exe";
                process.StartInfo.Arguments = String.Format("/C {0}\\bin\\httpd.exe -k restart -n wampapache{1}", path, wamp.Contains("64") ? "64" : "");
                process.StartInfo.Verb = "runas";
                process.Start();
            }
            catch (DirectoryNotFoundException e)
            {
                Console.WriteLine("Le dossier {0} n'existe pas.", e);
            }
        }

        private static void configPhpPdoOdbc(String path)
        {
            Console.WriteLine("Configuration de php ...");
            File.WriteAllText(
                String.Format("{0}\\php.ini", path),
                Regex.Replace(
                    File.ReadAllText(
                        String.Format("{0}\\php.ini", path)
                    ),
                    ";extension=php_pdo_odbc.dll",
                    "extension=php_pdo_odbc.dll"
                )
            );
        }

    }
}
