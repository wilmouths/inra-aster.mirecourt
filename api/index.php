<?php

use Slim\App;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use inra\controllers\InterventionsController;
use inra\controllers\RecoltesController;
use inra\controllers\SolsController;
use inra\controllers\PointsFertiController;
use inra\controllers\AssolementController;
use inra\controllers\FiliationsController;
use inra\controllers\ParcellesController;
use inra\controllers\PhotosController;
use inra\controllers\EffluentController;
use inra\controllers\SemisController;
use inra\controllers\AdventicesController;

// Inclusion de l'autoloader
require 'vendor/autoload.php';

ini_set('memory_limit', -1);

// Definition de varables globale
define('DEBUG', true);
define('BASE_URL', __DIR__);
define('DS', DIRECTORY_SEPARATOR);
define('SRC', BASE_URL . DS . 'src');
define('DATA', dirname(BASE_URL) . DS . 'data');
define('DATABASE', DATA . DS . 'database');

// Instantiation de Slim
$app = new App([
   'settings' => [
       'displayErrorDetails' => DEBUG,
   ] 
]);
// Recuperation du container de l'application
$container = $app->getContainer();
// Definition de l'erreur 404
$container['notFoundHandler'] = function($container) {
    return function (RequestInterface $request, ResponseInterface $response) use ($container) {
        return $response->withStatus(404)
            ->withHeader('Content-type', 'application/json')
            ->withJson(['ERROR_404']);
    };
};
// Definition de l'erreur 405
$container['notAllowedHandler'] = function ($container) {
    return function (RequestInterface $request, ResponseInterface $response, $methods) use ($container) {
        return $container->withStatus(405)
            ->withHeader('Content-type', 'application/json')
            ->withJson(['ERROR_405']);
    };
};
// Definition de l'erreur 500
if (!DEBUG) {
    $container['phpErrorHandler'] = function ($container) {
        return function (RequestInterface $request, ResponseInterface $response, $error) use ($container) {
            return $response->withStatus(500)
                ->withHeader('Content-type', 'application/json')
                ->withJson(['ERROR_500']);
        };
    };
}

/* ------------------- ROUTES ------------------- */

// Groupement des uri commencant par "sol"
$app->group('/sol', function() {
    // route des sols
    $this->get('/', SolsController::class . ':sols')->setName('sol.sols');

    // route du details du sol
    $this->get('/details/{annee:[0-9]+}', SolsController::class . ':solDetails')->setName('sol.details');

    // route de detail d'un sol selon le nom_court de la parcelle, l'annee
    $this->get('/{NOM_COURT:[a-zA-Z0-9\_-]+}/{annee:[0-9]+}', SolsController::class . ':sol')->setName('sol.sol');

    // route de detail d'un sol selon le nom_court de la parcelle, l'annee
    $this->get('/{NOM_COURT:[a-zA-Z0-9\_-]+}/{NOM_COURT_P:[a-zA-Z0-9\_-]+}/{annee:[0-9]+}', SolsController::class . ':solFiliation')->setName('sol.filiation');
});

// Groupement des uri commencant par "assolement"
$app->group('/assolement', function() {
    // recuperation de l'assolement pour une annee donnee
    $this->get('/{annee:[0-9]+}', AssolementController::class . ':assolement')->setName('assolement.assolement');
    // recuperation du details de l'assolement pour une annee
    $this->get('/details/{annee:[0-9]+}', AssolementController::class . ':assolementDetails')->setName('assolement.details');
});

// recuperation des interventions
$app->get('/interventions/{nom_court:[a-zA-Z0-9\_-]+}[/{annee:[0-9]+}]', InterventionsController::class . ':interventions')->setName('interventions');
$app->group('/itk', function() {
    $this->get('/{IDPA:[0-9]+}/{annee:[0-9]+}', InterventionsController::class . ':itk')->setName('itk');
    $this->get('/details/{IDPA:[0-9]+}/{date:[0-9_]+}', InterventionsController::class . ':details')->setName('details');
});

// recuperation d'infos d'une parcelle
$app->get('/infos/{nom_court:[a-zA-Z0-9\_-]+}', ParcellesController::class . ':infos')->setName('infos');

// recuperation des recoltes
$app->group('/recoltes', function() {
    $this->get('/parcelle/{IDPA:[0-9]+}/{annee:[0-9]+}', RecoltesController::class . ':recoltesParcelle')->setName('recoltesParcelle');
    $this->get('/pf/{IDPA:[0-9]+}/{annee:[0-9]+}', RecoltesController::class . ':recoltesPf')->setName('recoltesPf');
});

$app->group('/parcelle', function() {
    // recuperation des infos du point de ferti
    $this->get('/point_ferti/{nom_court:[a-zA-Z0-9\-_]+}', ParcellesController::class . ':pointsFerti')->setName('parcelle.pointFerti');
    $this->get('/sol/{nom_court:[a-zA-Z0-9\-_]+}', ParcellesController::class . ':sol')->setName('parcelle.sol');
});

$app->group('/point_ferti', function() {
    $this->get('/{PF:[a-zA-Z0-9\-_]+}', PointsFertiController::class . ':infos')->setName('pointFerti.infos');
    $this->get('/view/{PF:[a-zA-Z0-9\-_]+}', PointsFertiController::class . ':view')->setName('pointFerti.view');
});

// recuperation des filiation d'un parcelle
$app->get('/filiations/{NOM_COURT:[a-zA-Z0-9\_-]+}[/{annee:[0-9]+}]', FiliationsController::class . ':filiations')->setName('filiations');

// Recuperation photos
$app->get('/photos/{NOM_COURT:[a-zA-Z0-9\_-]+}/{type:[a-zA-Z]+}/{annee:[0-9]+}', PhotosController::class . ':photosWithType')->setName('photos.photosWithType');

$app->get('/photos/{NOM_COURT:[a-zA-Z0-9\_-]+}/{annee:[0-9]+}', PhotosController::class . ':photos')->setName('photos');


$app->group('/effluents', function() {
    $this->get('/{IDPA:[0-9]+}/{annee:[0-9]+}', EffluentController::class . ':effluents')->setName('effluents');
    $this->get('/details/{IDPA:[0-9]+}/{annee:[0-9]+}', EffluentController::class . ':details')->setName('details');
    $this->get('/legend', EffluentController::class . ':legend')->setName('effluents.legend');
});

$app->group('/semis', function() {
    $this->get('/{IDPA:[0-9]+}/{annee:[0-9]+}', SemisController::class . ':semis')->setName('semis');
    $this->get('/details/{IDPA:[0-9]+}/{date:[0-9_]+}', SemisController::class . ':details')->setName('details');
});

$app->group('/adventices', function() {
    $this->get('/{IDPA:[0-9]+}/{annee:[0-9]+}/{ido:[0-9]+}', AdventicesController::class . ':adventices')->setName('adventices');
});
/* ------------------- ROUTES ------------------- */

$app->run();
