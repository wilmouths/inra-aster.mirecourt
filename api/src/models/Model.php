<?php

namespace inra\models;

use inra\middlewares\Picker;
use \PDO;
use \Exception;

/**
 * Classe parent aux models
 * @author WILMOUTH Steven
 */
class Model {
   
    /*
     * @var array tableau des connexions a la base de donnees
     */
    private static $connections = [];
    
    /*
     * @var string nom de la table
     */
    public $table;
    
    
    /*
     * @var string nom de la cle primaire
     */
    public $primaryKey;
    public $primaryKeyValue;
    
    /*
     * @var PDO instance de la connexion courrant a la base de donnees
     */
    public $db;
    
    /*
     * @var PDOStatement requete courrant executee
     */
    private $query;
    
    /*
     * Constructeur du model
     * Permet de ce connecter a la base de donnees
     * Evite la reconnexion a la base de donnes si une connexion existe deja
     */
    public function __construct() {
        if ($this->table === false) {
            $this->table = strtolower(substr(get_class($this), strrpos(get_class($this), '\\') + 1));
        }
        
        $dbName = Picker::get('config.database.dbName');
        
        if (isset(self::$connections[$dbName])) {
            $this->db = self::$connections[$dbName];
            return true;
        } else {
            try {
                $dbPath = DATABASE . DS . $dbName;
                $user = Picker::get('config.database.user');
                $pass = Picker::get('config.database.pass');
                $plugin = Picker::get('config.database.plugin');
                $pdo = new PDO("odbc:Driver={$plugin}; DBQ={$dbPath}; Uid={$user}; Pwd={$pass};");
                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $pdo->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES utf8");
                $this->db = $pdo;
                self::$connections[$dbName] = $this->db;
            } catch (Exception $e) {
                if (Picker::get('config.database.debug') === true) {
                    die(utf8_encode($e->getMessage()));
                } else {
                    die(Picker::get('text.database.connectionFailed'));
                }
            }
        }
    }
    
    /*
     * Methode permetant la constuction du requuete preparee
     * @param array table permettant la construction
     * @return PDOStatement resultat de la requete
     */
    public function find($req) {
        $sql = 'SELECT ';
        
        if (array_key_exists('limit', $req)) {
            $sql .= ' TOP ' . $req['limit'] . ' ';
        }
        
        if (array_key_exists('fields', $req)) {
            if (is_array($req['fields'])) {
                $sql .= implode(', ', $req['fields']);
            } else {
                $sql .= $req['fields'];
            }
        } else {
            $sql .= '*';
        }
        
        $sql .= ' FROM ' . $this->table;

        if (array_key_exists('conditions', $req)) {
            $sql .= ' WHERE ';
            if (!is_array($req['conditions'])) {
                $sql .= $req['conditions'];
            } else {
                $cond = [];
                foreach ($req['conditions'] as $k => $v) {
                    switch ($k) {
                        case 'lt':
                            $key = current(array_keys($v));
                            $value = current(array_values($v));
                            array_push($cond, "$key < $value");
                            break;

                        case 'lteq':
                            $key = current(array_keys($v));
                            $value = current(array_values($v));
                            array_push($cond, "$key <= $value");
                            break;

                        case 'gt':
                            $key = current(array_keys($v));
                            $value = current(array_values($v));
                            array_push($cond, "$key > $value");
                            break;

                        case 'gteq':
                            $key = current(array_keys($v));
                            $value = current(array_values($v));
                            array_push($cond, "$key >= $value");
                            break;

                        case 'btw':
                            $key = current(array_keys($v));
                            $values = current(array_values($v));
                            $a = current($values);
                            $b = end($values);
                            array_push($cond, "$key BETWEEN $a AND $b");
                            break;
                        
                        default:
                            if (!is_numeric($v)) $v = '\'' . addslashes($v) . '\'';
                            array_push($cond, preg_match('/%/', $v) ? "$k LIKE $v" : "$k = $v");
                            break;
                    }
                }
                $sql .= implode(' AND ', $cond);
            }
        }

        if (array_key_exists('group', $req)) {
            $sql .= ' GROUP BY ' . $req['group'];
        }
        
        if (array_key_exists('order', $req)) {
            $sql .= ' ORDER BY ';
            $cond = [];
            foreach ($req['order'] as $k => $v) {
                array_push($cond, "$k $v");    
            }
            $sql .= implode(', ', $cond);
        }

        try {
            $sql = preg_replace('!\s+!', ' ', $sql);
            $pre = $this->db->prepare($sql);
            $pre->execute();
            $this->query = $pre->fetchAll(PDO::FETCH_OBJ);
        } catch (\PDOException $e) {
        	var_dump($sql);
            die(utf8_encode($e->getMessage()));
        }
        return $this->query;
    }
    
    /*
     * Methode qui permet de trouver la premiere occurence
     * @param array table permettant la construction
     * @return PDOStatement resultat de la requete
     */
    public function first($req) {
        $first = current($this->find($req));
        if (!empty($first)) {
            $tmp = ((array) $first);
            $this->primaryKeyValue = $tmp[$this->primaryKey];
        }
        return $first;
    }
    
    /*
     * Methode permettant de compter le nombre d'occurences ou de ligne d'une table
     * @param array|null tableau pour la construction de la requete
     * @return int valeu du count
     */
    public function count($conditions = null) {
        $res = $this->first([
            'fields' => 'COUNT(' . ((!$this->primaryKey) ? '*' : $this->primaryKey) . ') as count',
            'conditions' => $conditions
        ]);
        return !empty($res->count) ? $res->count : 0;
    }
    
    /*
     * Methode permettant les relation 1 - N
     * @param Model model de la relation
     * @return PDOStatement
     */
    public function hasMany($model, $foreignKey, $req = null) {
        $model = new $model();
        $sql = '';
        $req = $this->db->prepare($sql);
        $req->execute();
        return $req->fetchAll(PDO::FETCH_OBJ);     
    }
    
    /*
     * Methode permettant les relation N - 1
     * @param Model model de la relation
     * @return PDOStatement
     */
    public function belongsTo($model, $foreignKey, $req = null) {        
        $model = new $model();
        $sql = '';
        $req = $this->db->prepare($sql);
        $req->execute();
        return current($req->fetchAll(PDO::FETCH_OBJ));
    }
    
}
