<?php

namespace inra\middlewares;

/**
 * Permet de recuperer n'importe quelle valeur dans des fichiers de configuration
 * @author WILMOUTH Steven
 */
class Picker {
    
    /**
     * Recupere des donnees dans le fichier de config
     */
    public static function get($var) {
        $data = [];
        $var = trim($var);
        $var = explode('.', $var);
        $path = SRC . DS . $var[0] . DS . $var[1] . '.php';
        if (file_exists($path)) {
            $data = require $path;
            if (array_key_exists($var[2], $data)) {
                return $data[$var[2]];
            } else {
                return "{$var[1]} undefined in {$var[1]}.php";
            }
        } else {
            return "{$var[1]}.php doesn't exists";
        }
    }
    
}
