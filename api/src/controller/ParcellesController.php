<?php

namespace inra\controllers;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use inra\models\N_Parcelles;
use inra\models\R_Parcelle_PF;
use inra\models\R_T_Texture_PF;
use inra\models\R_Parc_Sol_Text;
use inra\models\R_Parc_Sol_Substr;

class ParcellesController extends BaseController {

	/*
     * Methode de rendu jSON de donnees
     * Permet de recuperer des information sur une parcelle
     * @param RequestInterface $request Interface de requete
     * @param ResponseInterface $response Interface de reponse
     * @param $args arguments
     */
    public function infos(RequestInterface $request, ResponseInterface $response, $args) {
        $N_Parcelles = new N_Parcelles();
        $infos = $N_Parcelles->first([
            'fields' => 'IDPA, surface, systeme, SDC',
            'conditions' => [
                'Nom_Court' => $args['nom_court']
            ]
        ]);
        return $this->returnJSON($response, $infos);
    }

    public function pointsFerti(RequestInterface $request, ResponseInterface $response, $args) {
        $R_Parcelle_PF = new R_Parcelle_PF();
        $pointsFerti = $R_Parcelle_PF->find([
            'conditions' => [
                'Nom_Court' => $args['nom_court']
            ]
        ]);
        
        $pfData = [];

        if (!empty($pointsFerti)) {
            $pointsFerti = current($pointsFerti);
            
            $pf2 = end($pointsFerti);
            $pf1 = prev($pointsFerti);
            
            $R_T_Texture_PF = new R_T_Texture_PF();
            $pf1Data = $R_T_Texture_PF->first([
                'fields' => 'PF, Argile, Limons, Sables',
                'conditions' => [
                    'PF' => $pf1
                ]
            ]);
            if (!empty($pf1Data)) {
                $pfData[$pf1] = [
                    'argile' => round($pf1Data->Argile),
                    'limons' => round($pf1Data->Limons),
                    'sables' => round($pf1Data->Sables)
                ];
            }

            $R_T_Texture_PF = new R_T_Texture_PF();
            $pf2Data = $R_T_Texture_PF->first([
                'fields' => 'PF, Argile, Limons, Sables',
                'conditions' => [
                    'PF' => $pf2
                ]
            ]);
            if (!empty($pf2Data)) {
                $pfData[$pf2] = [
                    'argile' => round($pf2Data->Argile),
                    'limons' => round($pf2Data->Limons),
                    'sables' => round($pf2Data->Sables)
                ];
            }
        }

        return $this->returnJSON($response, $pfData);
    }

    public function sol(RequestInterface $request, ResponseInterface $response, $args) {
        $R_Parc_Sol_Text = new R_Parc_Sol_Text();
        $textures = $R_Parc_Sol_Text->find([
            'conditions' => [
                'Nom_Court' => $args['nom_court']
            ]
        ]);

        $R_Parc_Sol_Substr = new R_Parc_Sol_Substr();
        $substratums = $R_Parc_Sol_Substr->find([
            'conditions' => [
                'Nom_Court' => $args['nom_court']
            ]
        ]);

        $sol = [
            'textures' => [],
            'substratums' => [],
            'surfaces' => []
        ];

        foreach ($textures as $k => $v) {
            $sol['textures'][$v->ID_Texture] = [
                'nom' => $v->T_S_descrip,
                'surface' => $v->ST_Sol,
                'pourcentage' => $v->PCT_Sol,
                'couleur' => $v->Couleur_Text
            ];
        }
        $sol['surfaces']['textures'] = current($textures)->surface;
        usort($sol['textures'], function($a, $b) {
            return ($b['pourcentage'] - $a['pourcentage']);
        });

        foreach ($substratums as $k => $v) {
            $sol['substratums'][$v->ID_SUBSTR] = [
                'nom' => $v->Substr_descrip,
                'surface' => $v->ST_Sol,
                'pourcentage' => $v->PCT_Sol
            ];
        }
        usort($sol['substratums'], function($a, $b) {
            return ($b['pourcentage'] - $a['pourcentage']);
        });
        $sol['surfaces']['substratums'] = current($substratums)->surface;

        return $this->returnJSON($response, $sol);
    }

}