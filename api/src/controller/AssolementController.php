<?php

namespace inra\controllers;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use inra\models\R_Assolement_Final;
use inra\models\R_Assol_Tout_Couleur;

/**
 * Controller de l'Assolement
 * @author WILMOUTH Steven
 */
class AssolementController extends BaseController {

	 /*
     * Methode de rendu jSON de donnees
     * Permet de recuperer la trame et la couleur de l'assolement pour une annee donnee
     * @param RequestInterface $request Interface de requete
     * @param ResponseInterface $response Interface de reponse
     * @param $args arguments
     */
	public function assolement(RequestInterface $request, ResponseInterface $response, $args) {
		$R_Assolement_Final = new R_Assolement_Final();
		$assolement = $R_Assolement_Final->find([
			'fields', 'IDPA, Nom_Court, Couleur_A, Trame',
			'conditions' => [
				'An_Rec' => $args['annee']
			]
		]);
		return $this->returnJSON($response, $assolement);
	}

	/*
     * Methode de rendu jSON de donnees
     * Permet de recuperer le detail de l'assolement pour une annee donnee
     * @param RequestInterface $request Interface de requete
     * @param ResponseInterface $response Interface de reponse
     * @param $args arguments
     */
	public function assolementDetails(RequestInterface $request, ResponseInterface $response, $args) {
		$R_Assol_Tout_Couleur = new R_Assol_Tout_Couleur();
		$data = [];
		$p = $R_Assol_Tout_Couleur->find([
			'fields' => 'Code_E, Nom',
			'conditions' => [
				'Type_Culture' => 'P',
				'An_Rec' => $args['annee'],
				'Echec' => 0
			],
			'group' => 'Code_E, Nom'
		]);
		if (!empty($p)) {
			foreach ($p as $k => $v) {
				$d = $R_Assol_Tout_Couleur->find([
					'conditions' => [
						'Type_Culture' => 'P',
						'An_Rec' => $args['annee'],
						'Code_E' => $v->Code_E
					]
				]);
				$total = 0;
				foreach ($d as $key => $value) {
					 $total = ($total + $value->surface);
				}
				$data['p'][$v->Code_E] = round($total, 2);
			}
			uasort($data['p'], function($a, $b) {
               return ($b - $a); 
            });
		}

		$i = $R_Assol_Tout_Couleur->find([
			'fields' => 'Code_E, Nom',
			'conditions' => [
				'Type_Culture' => 'I',
				'An_Rec' => $args['annee'],
				'Echec' => 0
			],
			'group' => 'Code_E, Nom'
		]);
		if (!empty($i)) {
			foreach ($i as $k => $v) {
				$d = $R_Assol_Tout_Couleur->find([
					'conditions' => [
						'Type_Culture' => 'I',
						'An_Rec' => $args['annee'],
						'Code_E' => $v->Code_E
					]
				]);
				$total = 0;
				foreach ($d as $key => $value) {
					 $total = ($total + $value->surface);
				}
				$data['i'][$v->Code_E] = round($total, 2);
			}
			uasort($data['i'], function($a, $b) {
               return ($b - $a); 
            });
		}

		$s = $R_Assol_Tout_Couleur->find([
			'fields' => 'Code_E, Nom',
			'conditions' => [
				'Type_Culture' => 'S',
				'An_Rec' => $args['annee'],
				'Echec' => 0
			],
			'group' => 'Code_E, Nom'
		]);
		if (!empty($s)) {
			foreach ($s as $k => $v) {
				$d = $R_Assol_Tout_Couleur->find([
					'conditions' => [
						'Type_Culture' => 'S',
						'An_Rec' => $args['annee'],
						'Code_E' => $v->Code_E
					]
				]);
				$total = 0;
				foreach ($d as $key => $value) {
					 $total = ($total + $value->surface);
				}
				$data['s'][$v->Code_E] = round($total, 2);
			}
			uasort($data['s'], function($a, $b) {
               return ($b - $a); 
            });
		}
		return $this->returnJSON($response, $data);
	}
}
