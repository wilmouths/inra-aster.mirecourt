<?php

namespace inra\controllers;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use inra\models\R_Assol_Tout_Couleur;
use inra\models\R_Recolte_Parcelle;
use inra\models\N_Produit_Rec;
use inra\models\R_Recolte_Parcelle_Recap;
use inra\models\AR_DR_Rec_Prairie_Final;
use inra\models\AR_DR_Rec_Rdt_PG;

/**
 * Controller des Recoltes
 * @author WILMOUTH Steven
 */
class RecoltesController extends BaseController {

    /*
     * Methode de rendu jSON de donnees
     * Permet de recuperer les recoltes pour une parcelle donnee
     * @param RequestInterface $request Interface de requete
     * @param ResponseInterface $response Interface de reponse
     * @param $args arguments
     */
    public function recoltesParcelle(RequestInterface $request, ResponseInterface $response, $args) {
        $R_Assol_Tout_Couleur = new R_Assol_Tout_Couleur();
        $data = $R_Assol_Tout_Couleur->find([
            'conditions' => [
                'IDPA' => $args['IDPA'],
                'An_Rec' => $args['annee']
            ]
        ]);
        usort($data, function($a, $b) {
            return (date('Y-m-d', strtotime($a->DateF)) > date('Y-m-d', strtotime($b->DateF)));
        });

        if (!empty($data)) {
            $start = current($data)->DateD;
            $end = end($data)->DateF;

            $R_Recolte_Parcelle = new R_Recolte_Parcelle();
            $recs = $R_Recolte_Parcelle->find([
                'conditions' => [
                    'IDPA' => $args['IDPA'],
                    'btw' => [
                        'DateI' => [
                            '#' . date('Y-m-d', strtotime($start)) . '#', 
                            '#' . (is_null($end) ? date('Y-m-d') : date('Y-m-d', strtotime($end))) . '#'
                        ]
                    ]
                ]
            ]);
            usort($recs, function($a, $b) {
                return (date('Y-m-d', strtotime($a->DateI)) > date('Y-m-d', strtotime($b->DateI)));
            });

            $dates = [];
            $recoltes = [];
            $recoltesTout = [];
            foreach ($recs as $k => $v) {
                $DateI = date('d/m/Y', strtotime($v->DateI));

                $recoltesTout[$DateI][] = $v;

                if (!in_array(utf8_encode($DateI), $dates)) {
                    $dates[] = utf8_encode($DateI);
                }

                if (!isset($recoltes[utf8_encode($v->Espece)][utf8_encode($v->Produit)])) {
                    $recoltes[utf8_encode($v->Espece)][utf8_encode($v->Produit)]['Couleur_Prod'] = $v->Couleur_Prod;
                    $recoltes[utf8_encode($v->Espece)][utf8_encode($v->Produit)]['Couleur_Esp'] = $v->Couleur_Esp;
                    $recoltes[utf8_encode($v->Espece)][utf8_encode($v->Produit)]['Code_E'] = $v->Code_E;
                    $recoltes[utf8_encode($v->Espece)][utf8_encode($v->Produit)]['Qte_tms_ha'] = $v->Qte_tms_ha;
                    $recoltes[utf8_encode($v->Espece)][utf8_encode($v->Produit)]['DateI'] = $DateI;
                    $recoltes[utf8_encode($v->Espece)][utf8_encode($v->Produit)]['x'] = current(array_keys($dates, $DateI));
                } else {
                    $recoltes[utf8_encode($v->Espece)][utf8_encode($v->Produit)]['Qte_tms_ha'] = $recoltes[utf8_encode($v->Espece)][utf8_encode($v->Produit)]['Qte_tms_ha'] + $v->Qte_tms_ha;
                    $recoltes[utf8_encode($v->Espece)][utf8_encode($v->Produit)]['x'] = current(array_keys($dates, $DateI));
                }
            }
            $recoltesFinal = [];

            $startData = new \stdClass();
            $startData->x = 0;
            $startData->Qte_tms_ha = 0;
            $startData->Couleur_Prod = '';
            $startData->DateI = date('d/m/Y', strtotime($start));
            $recoltesFinal[] = $startData;

            foreach ($recoltes as $key => $value) {
                foreach ($value as $k => $v) {
                    $recoltesFinal[] = $v;
                }
            }

            $endData = new \stdClass();
            $endData->x = 0;
            $endData->Qte_tms_ha = 0;
            $endData->Couleur_Prod = '';
            $endData->DateI = date('d/m/Y', strtotime($end));
            $recoltesFinal[] = $endData;

            $N_Produit_Rec = new N_Produit_Rec();
            $legende = $N_Produit_Rec->find([]);

            $R_Recolte_Parcelle_Recap = new R_Recolte_Parcelle_Recap();
            $recap = $R_Recolte_Parcelle_Recap->find([
                'conditions' => [
                    'IDPA' => $args['IDPA'],
                    'An_Rec' => $args['annee']
                ]
            ]);
            $recaps = [];
            foreach ($recap as $k => $v) {
                $recaps[$v->An_Rec][] = $v;
            }

            foreach ($recoltesTout as $key => $value) {
                foreach ($value as $k => $v) {
                    $v->DateI = date('d/m/Y', strtotime($v->DateI));
                    if (is_null($v->Hpc)) $v->Hpc = 0;
                    if (is_null($v->Impurete_kg)) $v->Impurete_kg = 0;
                    if (is_null($v->Petit_Grain_kg)) $v->Petit_Grain_kg = 0;
                    if (is_null($v->Poids_Specifique)) $v->Poids_Specifique = 0;
                    if (is_null($v->Proteinepc)) $v->Proteinepc = 0;
                    if (is_null($v->MSpc)) $v->MSpc = 0;
                }
            }

            return $this->returnJSON($response, [
                'recoltes' => $recoltesFinal,
                'dates' => $dates,
                'legende' => $legende,
                'recap' => $recaps,
                'recoltesTout' => $recoltesTout
            ]);
        }
    }

    public function recoltesPf(RequestInterface $request, ResponseInterface $response, $args) {
        $R_Assol_Tout_Couleur = new R_Assol_Tout_Couleur();
        $data = $R_Assol_Tout_Couleur->find([
            'conditions' => [
                'IDPA' => $args['IDPA'],
                'An_Rec' => $args['annee']
            ]
        ]);
        usort($data, function($a, $b) {
            return (date('Y-m-d', strtotime($a->DateF)) > date('Y-m-d', strtotime($b->DateF)));
        });

        if (!empty($data)) {
            $start = current($data)->DateD;
            $end = end($data)->DateF;

            $dates = [];

            $dates[] = date('d/m/Y', strtotime($start));

            $AR_DR_Rec_Prairie_Final = new AR_DR_Rec_Prairie_Final();
            $recoltesPrairie = $AR_DR_Rec_Prairie_Final->find([
                'conditions' => [
                    'IDPA' => $args['IDPA'],
                    'btw' => [
                        'DateM' => [
                            '#' . date('Y-m-d', strtotime($start)) . '#', 
                            '#' . (is_null($end) ? date('Y-m-d') : date('Y-m-d', strtotime($end))) . '#'
                        ]
                    ]
                ]
            ]);
            usort($recoltesPrairie, function($a, $b) {
                return (date('Y-m-d', strtotime($a->DateM)) > date('Y-m-d', strtotime($b->DateM)));
            });
            $recoltesP = [];
            $p = [];
            foreach ($recoltesPrairie as $k => $v) {
                $DateM = date('Y-m-d', strtotime($v->DateM));
                if (!in_array(utf8_encode($DateM), $dates)) {
                    $dates[] = $DateM;
                }

                $p[date('d/m/Y', strtotime($v->DateM))][] = $v;

                $d = new \stdClass();
                $d->RdtA = $v->TMSHa;
                $d->RdtB = 0;
                $d->Couleur = $v->Couleur;
                $d->Code_E = $v->Code_E;
                $d->CODE = 'P';
                $d->Date = $DateM;
                $d->Leg_pc = '';
                $d->x = current(array_keys($dates, $DateM));
                $recoltesP[] = $d;
            }

            $AR_DR_Rec_Rdt_PG = new AR_DR_Rec_Rdt_PG();
            $recoltesGrain = $AR_DR_Rec_Rdt_PG->find([
                'conditions' => [
                    'IDPA' => $args['IDPA'],
                    'btw' => [
                        'Date_C' => [
                            '#' . date('Y-m-d', strtotime($start)) . '#', 
                            '#' . (is_null($end) ? date('Y-m-d') : date('Y-m-d', strtotime($end))) . '#'
                        ]
                    ]
                ]
            ]);
            usort($recoltesGrain, function($a, $b) {
                return (date('Y-m-d', strtotime($a->Date_C)) > date('Y-m-d', strtotime($b->Date_C)));
            });
            
            $recoltesG = [];
            $pg = [];
            foreach ($recoltesGrain as $k => $v) {
                $Date_C = date('Y-m-d', strtotime($v->Date_C));
                if (!in_array(utf8_encode($Date_C), $dates)) {
                    $dates[] = $Date_C;
                }

                $pg[date('d/m/Y', strtotime($v->Date_C))][] = $v;

                if (!isset($recoltesG[utf8_encode($v->Espece)])) {
                    $recoltesG[utf8_encode($v->Espece)]['CODE'] = 'PG';
                    $recoltesG[utf8_encode($v->Espece)]['Couleur'] = $v->Couleur;
                    $recoltesG[utf8_encode($v->Espece)]['Code_E'] = $v->Espece;
                    $recoltesG[utf8_encode($v->Espece)]['Leg_pc'] = '';
                    if (!is_null($v->RdtGrain_TMS_ha)) {
                        $recoltesG[utf8_encode($v->Espece)]['RdtA'] = $v->RdtGrain_TMS_ha;
                    } else {
                        $recoltesG[utf8_encode($v->Espece)]['RdtA'] = 0;
                    }
                    if (!is_null($v->Rdt_Paille_tmsha)) {
                        $recoltesG[utf8_encode($v->Espece)]['RdtB'] = $v->Rdt_Paille_tmsha;
                    } else {
                        $recoltesG[utf8_encode($v->Espece)]['RdtB'] = 0;
                    }
                    $recoltesG[utf8_encode($v->Espece)]['Date'] = $Date_C;
                    $recoltesG[utf8_encode($v->Espece)]['x'] = current(array_keys($dates, $Date_C));
                } else {
                    $recoltesG[utf8_encode($v->Espece)]['RdtA'] = $recoltesG[utf8_encode($v->Espece)]['RdtA'] + $v->RdtGrain_TMS_ha;
                    $recoltesG[utf8_encode($v->Espece)]['RdtB'] = $recoltesG[utf8_encode($v->Espece)]['RdtB'] + $v->Rdt_Paille_tmsha;
                    $recoltesG[utf8_encode($v->Espece)]['x'] = current(array_keys($dates, $Date_C));
                }
            }
            $recoltesPG = [];
            foreach ($recoltesG as $k => $v) {
                $recoltesPG[] = (object) $v;
            }

            $datas = array_merge($recoltesP, $recoltesPG);

            $startData = new \stdClass();
            $startData->x = 0;
            $startData->Date = date('Y-m-d', strtotime($start));
            $startData->RdtA = 0;
            $startData->RdtB = 0;
            $startData->Code_E = '';
            $startData->CODE = '';
            $startData->Couleur = '';
            $startData->Leg_pc = '';
            $datas[] = $startData;
            
            $endData = new \stdClass();
            $endData->x = count($datas);
            $endData->Date =  date('Y-m-d', strtotime($end));
            $endData->RdtA = 0;
            $endData->RdtB = 0;
            $endData->Code_E = '';
            $endData->CODE = '';
            $endData->Couleur = '';
            $endData->Leg_pc = '';
            $datas[] = $endData;

            $dates[] = date('Y-m-d', strtotime($end));

            $N_Produit_Rec = new N_Produit_Rec();
            $legende = $N_Produit_Rec->find([]);

            usort($datas, function($a, $b) {
                return strtotime($a->Date) - strtotime($b->Date);
            });

            foreach ($datas as $k => $v) {
                $v->Date = date('d/m/Y', strtotime($v->Date));
            }

            foreach ($pg as $key => $value) {
                foreach ($value as $k => $v) {
                    $v->Date = date('d/m/Y', strtotime($v->Date_C));
                    $v->surface = round($v->surface, 2);
                    $v->RdtGrain_TMS_ha = round($v->RdtGrain_TMS_ha, 2);
                    $v->Rdt_Paille_tmsha = round($v->Rdt_Paille_tmsha, 2);
                }
            }

            foreach ($p as $key => $value) {
                foreach ($value as $k => $v) {
                    $v->Date = date('d/m/Y', strtotime($v->DateM));
                    $v->surface = round($v->surface, 2);
                    $v->TMSHa = round($v->TMSHa, 2);
                    $v->Leg_pc = round($v->Leg_pc, 2);
                }
            }

            return $this->returnJSON($response, [
                'recoltes' => $datas,
                'legende' => $legende,
                'dates' => $dates,
                'p' => $p,
                'pg' => $pg
            ]);
        }
    }

}
