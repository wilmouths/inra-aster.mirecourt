<?php

namespace inra\controllers;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use inra\models\N_Texture;
use inra\models\N_Substratum;
use inra\models\N_Parcelles;
use inra\models\R_Assol_final;
use inra\models\R_Assol_Tout_Couleur;
use inra\models\R_Parcelles_Filiation;
use inra\models\R_Tex_Subst_Sol_CD;
use inra\models\R_Surface_Totale_Assol_ANREC;

/**
 * Controller du Sol
 * @author WILMOUTH Steven
 */
class SolsController extends BaseController {
	
	/*
     * Methode de rendu jSON de donnees
     * Permet de recuperer les textures presentent sur le sol
     * @param RequestInterface $request Interface de requete
     * @param ResponseInterface $response Interface de reponse
     * @param $args arguments
     */
	public function sols(RequestInterface $request, ResponseInterface $response, $args) {
		$N_Texture = new N_Texture();
		$textures = $N_Texture->find([
			'conditions' => 'ID_TS'
		]);
		
		$N_Substratum = new N_Substratum();
		$substratums = $N_Substratum->find([
			'conditions' => 'ID_SUBSTR'
		]);
		
		$sols = array_merge($textures, $substratums);
		
		return $this->returnJSON($response, $sols);
	}
	
	/*
     * Methode de rendu jSON de donnees
     * Permet de recuperer l'assolement final d'une parcelle donnee [et d'une annee donnee]
     * @param RequestInterface $request Interface de requete
     * @param ResponseInterface $response Interface de reponse
     * @param $args arguments
     */
	public function sol(RequestInterface $request, ResponseInterface $response, $args) {
		$assolement = [];
		
		// Recuperation de la parcelle selon le nom court
		$R_Parcelles_Filiation = new R_Parcelles_Filiation();
		$parcelle = $R_Parcelles_Filiation->first([
			'fields' => 'IDPA',
			'conditions' => [
				'Nom_Court' => $args['NOM_COURT'],
			]
		]);

		// Definition de tableaux
		$assolementParent = [];
		$assolementGParent = [];
		$assolementAGParent = [];

		// Si on as une parcelle
		if (!empty($parcelle)) {

			// Recuperation de l'assolement de la parcelle
			$R_Assol_final = new R_Assol_final();
			$assolement = $R_Assol_final->find([
				'conditions' => [
					'IDPA' => $parcelle->IDPA,
					'lteq' => ['An_Rec' => $args['annee']] 
				]
			]);
			
			// Si la parcelle possede un assolement
			if (!empty($assolement)) {

				// Recuperation de la filliation parent
				$R_Parcelles_Filiation = new R_Parcelles_Filiation();
				$filliations = $R_Parcelles_Filiation->find([
					'fields' => 'IDPA, IDPAP, NC_P, IDPA_GP, NC_GP, IDPA_AGP, NC_AGP',
					'conditions' => [
						'IDPA' => $parcelle->IDPA
					]
				]);
				// Si il y a filliation
				if (!empty($filliations)) {
					$filliations = current($filliations);
					// Si il y a un parent
					if (!is_null($filliations->IDPAP)) {
						// Recuperation de l'assolement du parent
						$assolementP = $R_Assol_final->find([
							'conditions' => [
								'IDPA' => $filliations->IDPAP
							]
						]);
						// Si il y a un assolement
						if (!empty($assolementP)) {
							foreach ($assolementP as $k => $v) {
								array_push($assolementParent, $assolementP[$k]);
							}
						}

						// Si il y a un grand parent
						if (!is_null($filliations->IDPA_GP)) {
							// Recuperation de l'assolement du grand parent
							$assolementGP = $R_Assol_final->find([
								'conditions' => [
									'IDPA' => $filliations->IDPA_GP
								]
							]);
							// Si il y a assolement
							if (!empty($assolementGP)) {
								foreach ($assolementGP as $k => $v) {
									array_push($assolementGParent, $assolementGP[$k]);
								}
							}

							// Si il y a un arriere grand parent
							if (!is_null($filliations->IDPA_AGP)) {
								// Recuperation de l'assolement de l'arriere grand parent
								$assolementAGP = $R_Assol_final->find([
									'conditions' => [
										'IDPA' => $filliations->IDPA_AGP
									]
								]);
								// Si il y a assolement
								if (!empty($assolementAGP)) {
									foreach ($assolementAGP as $k => $v) {
										array_push($assolementAGParent, $assolementAGP[$k]);
									}
								}	
							}		
						}
						$assolement = array_merge($assolement, $assolementParent, $assolementGParent, $assolementAGParent);
						usort($assolement, function($a, $b) {
							return ($a->An_Rec - $b->An_Rec); 
						});

						if (!empty($assolement)) {
							$assolement[] = [
								'parentP' => (!empty($filliations)) ? ($filliations->NC_P . ((!is_null($filliations->NC_GP)) ? '_' . $filliations->NC_GP : '')) : "",
								'gparent' => (!empty($assolementGParent)) ? $filliations->NC_GP : ""
							];
						}
					} else {
						$assolement = $R_Assol_final->find([
							'conditions' => [
								'IDPA' => $parcelle->IDPA
							]
						]);
						foreach ($assolement as $key => $value) {
							if (empty($args['annee'])) {
								if ($value->An_Rec > date('Y', strtotime('-2000 day'))) {
									unset($assolement[$key]);
								}
							} else {
								if ($value->An_Rec > date($args['annee'], strtotime('-2000 day'))) {
									unset($assolement[$key]);
								}
							}
						}
						usort($assolement, function($a, $b) {
							return ($a->An_Rec - $b->An_Rec); 
						});
					}
				}
			}
		}

		$cultures = [];
		$assol = (!is_array(end($assolement))) ? $assolement : array_slice($assolement, 0, count($assolement) - 1);
		$R_Assol_Tout_Couleur = new R_Assol_Tout_Couleur();
		foreach ($assol as $k => $v) {
			$data = $R_Assol_Tout_Couleur->find([
				'fields' => 'An_Rec, IDPA, IDA, Couleur, Nom, Code_E, DateD, DateF',
				'conditions' => [
					'IDPA' => $v->IDPA,
					'An_Rec' => $v->An_Rec
				]
			]);
			foreach ($data as $k => $v) {
				$cultures[$v->An_Rec][$v->IDA] = $v;
			}
		}
		return $this->returnJSON($response, [
			'assolement' => $assolement,
			'cultures' => $cultures
		]);
	}

	/*
     * Methode de rendu jSON de donnees
     * Permet de recuperer l'assolement final d'une parcelle + filliation donnee [et d'une annee donnee]
     * @param RequestInterface $request Interface de requete
     * @param ResponseInterface $response Interface de reponse
     * @param $args arguments
     */
	public function solFiliation(RequestInterface $request, ResponseInterface $response, $args) {
		// Recuperation de la parcelle selon le nom court
		$N_Parcelle = new N_Parcelles();
		$parcelle = $N_Parcelle->first([
			'fields' => 'IDPA',
			'conditions' => [
				'Nom_Court' => $args['NOM_COURT'],
			]
		]);

		// Definition de tableaux
		$assolementParent = [];
		$assolementGParent = [];
		$assolementAGParent = [];

		// Si on as une parcelle
		if (!empty($parcelle)) {

			// Recuperation de l'assolement de la parcelle
			$R_Assol_final = new R_Assol_final();
			$assolement = $R_Assol_final->find([
				'conditions' => [
					'IDPA' => $parcelle->IDPA,
					'An_Rec' => $args['annee']
				]
			]);
			
			// Si la parcelle possede un assolement
			if (!empty($assolement)) {

				// Recuperation de la filliation parent
				$R_Parcelles_Filiation = new R_Parcelles_Filiation();
				$filliations = $R_Parcelles_Filiation->first([
					'fields' => 'IDPA, IDPAP, NC_P, IDPA_GP, NC_GP, IDPA_AGP, NC_AGP',
					'conditions' => [
						'IDPA' => $parcelle->IDPA,
						'NC_P' => $args['NOM_COURT_P']
					]
				]);
				if (empty($filliations)) {
					$filliations = $R_Parcelles_Filiation->first([
						'fields' => 'IDPA, IDPAP, NC_P, IDPA_GP, NC_GP, IDPA_AGP, NC_AGP',
						'conditions' => [
							'IDPA' => $parcelle->IDPA,
							'NC_GP' => $args['NOM_COURT_P']
						]
					]);
				}
				// Si il y a filliation
				if (!empty($filliations)) {
					// Si il y a un parent
					if (!is_null($filliations->IDPAP)) {
						// Recuperation de l'assolement du parent
						$assolementP = $R_Assol_final->find([
							'conditions' => [
								'IDPA' => $filliations->IDPAP
							]
						]);
						// Si il y a un assolement
						if (!empty($assolementP)) {
							foreach ($assolementP as $k => $v) {
								array_push($assolementParent, $assolementP[$k]);
							}
						}

						// Si il y a un grand parent
						if (!is_null($filliations->IDPA_GP)) {
							// Recuperation de l'assolement du grand parent
							$assolementGP = $R_Assol_final->find([
								'conditions' => [
									'IDPA' => $filliations->IDPA_GP
								]
							]);
							// Si il y a assolement
							if (!empty($assolementGP)) {
								foreach ($assolementGP as $k => $v) {
									array_push($assolementGParent, $assolementGP[$k]);
								}
							}

							// Si il y a un arriere grand parent
							if (!is_null($filliations->IDPA_AGP)) {
								// Recuperation de l'assolement de l'arriere grand parent
								$assolementAGP = $R_Assol_final->find([
									'conditions' => [
										'IDPA' => $filliations->IDPA_AGP
									]
								]);
								// Si il y a assolement
								if (!empty($assolementAGP)) {
									foreach ($assolementAGP as $k => $v) {
										array_push($assolementAGParent, $assolementAGP[$k]);
									}
								}	
							}		
						}
					}
				}
			}

			$assolement = array_merge($assolementParent, $assolementGParent, $assolementAGParent);
			usort($assolement, function($a, $b) {
				return ($a->An_Rec - $b->An_Rec); 
			});

			if (!empty($assolement)) {
				$assolement[] = [
					'parentP' => $filliations->NC_P . ((!is_null($filliations->NC_GP)) ? '_' . $filliations->NC_GP : ''),
					'gparent' => (!empty($assolementGParent)) ? $filliations->NC_GP : ""
				];
			}
		}
		
		$cultures = [];
		$assol = (!is_array(end($assolement))) ? $assolement : array_slice($assolement, 0, count($assolement) - 1);
		$R_Assol_Tout_Couleur = new R_Assol_Tout_Couleur();
		foreach ($assol as $k => $v) {
			$data = $R_Assol_Tout_Couleur->find([
				'fields' => 'An_Rec, IDPA, IDA, Couleur, Nom, Code_E, DateD, DateF',
				'conditions' => [
					'IDPA' => $v->IDPA,
					'An_Rec' => $v->An_Rec
				]
			]);
			foreach ($data as $k => $v) {
				$cultures[$v->An_Rec][$v->IDA] = $v;
			}
		}
		return $this->returnJSON($response, [
			'assolement' => $assolement,
			'cultures' => $cultures
		]);
	}

	/*
     * Methode de rendu jSON de donnees
     * Permet de recuperer le detail du sol sur une annee donnee
     * @param RequestInterface $request Interface de requete
     * @param ResponseInterface $response Interface de reponse
     * @param $args arguments
     */
	public function solDetails(RequestInterface $request, ResponseInterface $response, $args) {
		$R_Tex_Subst_Sol_CD = new R_Tex_Subst_Sol_CD();
		$textures = $R_Tex_Subst_Sol_CD->find([
			'conditions' => 'ID_TS'
		]);
		$R_Surface_Totale_Assol_ANREC = new R_Surface_Totale_Assol_ANREC();
		$surface = $R_Surface_Totale_Assol_ANREC->first([
			'fields' => 'An_Rec, SURF_ASSOL',
			'conditions' => [
				'An_Rec' => (empty($args) ? date('Y') : intval($args['annee']))
			]
		]);
		foreach ($textures as $k => $v) {
			$data = array_slice((array) $textures[$k], 4);
			$textures[$k]->surface = 0;
			$textures[$k]->textures = [];
			foreach ($data as $i => $j) {
				unset($v->$i);
				$textures[$k]->textures[utf8_encode($i)] = sprintf("%.2f", $j);
				$textures[$k]->surface = ($textures[$k]->surface + $j);
			}
			$textures[$k]->surface = sprintf("%.2f", $textures[$k]->surface);
			$textures[$k]->percent = sprintf("%.2f", ((floatval($textures[$k]->surface) * 100) / $surface->SURF_ASSOL));
		}
		uasort($textures, function($a, $b) {
			return ($b->surface - $a->surface);
		});
		foreach ($textures as $k => $v) {
			uasort($v->textures, function($a, $b) {
				return ($b - $a);
			});
		}
		return $this->returnJSON($response, $textures);
	}

}
