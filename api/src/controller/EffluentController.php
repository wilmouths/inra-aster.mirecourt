<?php

namespace inra\controllers;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use inra\models\R_Effluent;
use inra\models\N_Fertilisant;
use inra\models\R_Assol_Tout_Couleur;

class EffluentController extends BaseController {

	/*
	 * Methode de rendu jSON de donnees
	 * Permet de recuperer les effluents pour une parcelle donnee
	 * @param RequestInterface $request Interface de requete
	 * @param ResponseInterface $response Interface de reponse
	 * @param $args arguments
	 */
	public function effluents(RequestInterface $request, ResponseInterface $response, $args) {
		$R_Assol_Tout_Couleur = new R_Assol_Tout_Couleur();
		$data = $R_Assol_Tout_Couleur->find([
			'conditions' => [
				'IDPA' => $args['IDPA'],
				'An_Rec' => $args['annee']
			]
		]);

		usort($data, function($a, $b) {
			return (strtotime($b->DateF) < strtotime($a->DateF));
		});

		if (!empty($data)) {
			$start = current($data);
			$end = end($data);

			$R_Effluent = new R_Effluent();
			$effluents = $R_Effluent->find([
				'conditions' => [
					'IDPA' => $args['IDPA'],
					'btw' => [
						'DateI' => [
							'#' . date('d/m/Y', strtotime($start->DateD)) . '#', 
							'#' . (is_null($end->DateF) ? date('d/m/Y') : date('d/m/Y', strtotime($end->DateF))) . '#'
						]
					]
				]
			]);
			
			$ef = [];
			foreach ($effluents as $k => $v) {
				if (!isset($ef[utf8_decode($v->Nom) . '_' . $v->Code_F])) {
					$ef[utf8_decode($v->Nom) . '_' . $v->Code_F]['name'] = $v->Nom;
					$ef[utf8_decode($v->Nom) . '_' . $v->Code_F]['color'] = $v->Couleur;
					$ef[utf8_decode($v->Nom) . '_' . $v->Code_F]['value'] = $v->Qte_brute;
					$ef[utf8_decode($v->Nom) . '_' . $v->Code_F]['unite'] = $v->Unite;
				} else {
					$ef[utf8_decode($v->Nom) . '_' . $v->Code_F]['value'] = ($ef[utf8_decode($v->Nom) . '_' . $v->Code_F]['value'] + $v->Qte_brute);
				}
			}
		}

		return $this->returnJSON($response, $ef);
	}

	public function details(RequestInterface $request, ResponseInterface $response, $args) {
		$R_Assol_Tout_Couleur = new R_Assol_Tout_Couleur();
		$data = $R_Assol_Tout_Couleur->find([
			'conditions' => [
				'IDPA' => $args['IDPA'],
				'An_Rec' => $args['annee']
			]
		]);

		usort($data, function($a, $b) {
			return (strtotime($b->DateF) < strtotime($a->DateF));
		});

		if (!empty($data)) {
			$start = current($data);
			$end = end($data);

			$R_Effluent = new R_Effluent();
			$effluents = $R_Effluent->find([
				'conditions' => [
					'IDPA' => $args['IDPA'],
					'btw' => [
						'DateI' => [
							'#' . date('d/m/Y', strtotime($start->DateD)) . '#', 
							'#' . (is_null($end->DateF) ? date('d/m/Y') : date('d/m/Y', strtotime($end->DateF))) . '#'
						]
					]
				]
			]);
			foreach ($effluents as $k => $v) {
				$effluents[$k]->DateI = date('d/m/Y', strtotime($v->DateI));
				$effluents[$k]->Qte_brute = round($v->Qte_brute, 2);
				$effluents[$k]->SW = round($v->SW, 2);
			}
		}

		return $this->returnJSON($response, $effluents);
	}

	public function legend(RequestInterface $request, ResponseInterface $response, $args) {
		$N_Fertilisant = new N_Fertilisant();
		$fertilisants = $N_Fertilisant->find([
			'fields' => 'Nom, Couleur',
			'conditions' => [
				'Type' => 'O'
			]
		]);

		return $this->returnJSON($response, $fertilisants);
	}

}