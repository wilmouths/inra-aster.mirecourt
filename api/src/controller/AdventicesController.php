<?php

namespace inra\controllers;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use inra\models\W_ADV_HCPC_variables;
use inra\models\N_Parcelles;
use inra\models\R_Parcelles_Filiation;
use inra\models\W_ADV_Donnees_Flore_Adventice;
use inra\models\R_Assol_final;
use inra\models\R_Assol_Tout_Couleur;
use inra\models\R_ADV_Groupe_IND;
use inra\models\AR_ADV_Final;
use inra\models\R_Parcelle_PF;
use inra\models\R_ADV_Group;

class AdventicesController extends BaseController {

	/*
	 * Methode de rendu jSON de donnees
	 * Permet de recuperer les adventices pour une parcelle donnee
	 * @param RequestInterface $request Interface de requete
	 * @param ResponseInterface $response Interface de reponse
	 * @param $args arguments
	 */
	public function adventices(RequestInterface $request, ResponseInterface $response, $args) {
		$N_Parcelles = new N_Parcelles();
		$parcelle = $N_Parcelles->first([
			'conditions' => [
				'IDPA' => $args['IDPA']
			]
		]);

		if (!empty($parcelle)) {
			$parcellesNC = [$parcelle->Nom_Court];
			$parcellesIDPA = [$parcelle->IDPA];

			$R_Parcelles_Filiation = new R_Parcelles_Filiation();
			$parcelles = $R_Parcelles_Filiation->first([
				'conditions' => [
					'Nom_Court' => $parcelle->Nom_Court,
				]
			]);
			
			if (!is_null($parcelles->NC_P)) {
				$parcellesNC[] = $parcelles->NC_P;
				$parcellesIDPA[] = $parcelles->IDPAP;
			}

			if (!is_null($parcelles->NC_GP)) {
				$parcellesNC[] = $parcelles->NC_GP;
				$parcellesIDPA[] = $parcelles->IDPA_GP;
			}

			if (!is_null($parcelles->NC_AGP)) {
				$parcellesNC[] = $parcelles->NC_AGP;
				$parcellesIDPA[] = $parcelles->IDPA_AGP;
			};

			$TypeFlore = [];
			$adventices = [];
			$W_ADV_Donnees_Flore_Adventice = new W_ADV_Donnees_Flore_Adventice();
			$endDate = date('Y-m-d', strtotime(date('Y') . ' -' . (date('Y') - $args['annee']) . 'years'));
			foreach ($parcellesNC as $k => $v) {
				$advs = $W_ADV_Donnees_Flore_Adventice->find([
					'conditions' => [
						'IDO' => $args['ido'],
						'Nom_Court' => $v,
						'btw' => [
							'Date_M' => [
								'#' . date('Y-m-d', strtotime($endDate . ' -8 years')) . '#',
								'#' . $endDate . '#'
							]
						]
					]
				]);

				foreach ($advs as $k => $v) {
					$adventices[] = $v;

					if (!in_array($v->TypeFlore, $TypeFlore)) {
						$TypeFlore[] = $v->TypeFlore;
					}
				}
			}
			usort($adventices, function($a, $b) {
				return (date('d/m/Y', strtotime($a->Date_M)) > date('d/m/Y', strtotime($b->Date_M)));
			});

			$W_ADV_HCPC_variables = new W_ADV_HCPC_variables();
			$datas = [];
			$flores = [];
			$groupes = [];
			foreach ($TypeFlore as $k => $v) {
				$data = $W_ADV_HCPC_variables->find([
					'conditions' => [
						'Flore' => $v
					]
				]);

				foreach ($data as $k => $v) {
					if (!in_array($v->Groupe_ADV, $groupes)) {
						$groupes[] = $v->Groupe_ADV;
					}

					$flore = explode('_', $v->Flore);
					$flore = $flore[0] . ' ' . $flore[2];
					if (!in_array($flore, $flores)) {
						$flores[] = $flore;
					}

					$v->x = current(array_keys($groupes, $v->Groupe_ADV)) + 1;
					$v->y = current(array_keys($flores, $flore)) + 1;

					$datas[] = $v;
				}
			}
			$datas[] = [
				'x' => 0,
				'y' => count($flores)
			];

			$years = [];
			foreach ($adventices as $k => $v) {
				$year = date('Y', strtotime($v->Date_M));
				if (!in_array($year, $years)) {
					$years[] = $year;
				}
			}
			usort($years, function($a, $b) {
				return ($a > $b);
			});

			$advs = [];
			foreach ($groupes as $k => $v) {
				foreach ($adventices as $a => $b) {
					$year = date('Y', strtotime($b->Date_M));

					$flore = explode('_', $b->TypeFlore);
					$adv = [
						'x' => current(array_keys($groupes, $v)),
						'y' => current(array_keys($years, $year)) + 1,
						'radius' => empty($b->$v) ? 0 : intval($b->$v),
						'color' => $b->Couleur_Flore,
						'flore' => $flore[0] . ' ' . $flore[2]
					];

					$advs[] = $adv;
				}
			}

			$R_Assol_final = new R_Assol_final();
			$assolement = [];
			foreach ($parcellesIDPA as $k => $v) {
				$assol = $R_Assol_final->find([
					'conditions' => [
						'IDPA' => $v,
						'btw' => [
							'An_Rec' => [
								date('Y', strtotime($args['annee'] . ' -8 years')),
								date('Y', strtotime($args['annee']))
							]
						]
					]
				]);
				
				foreach ($assol as $k => $v) {
					$assolement[] = $v;
				}
			}

			foreach ($assolement as $k => $v) {
				unset($v->IDPA);
				$tmp = $v->An_Rec;
				unset($v->An_Rec);
				foreach ($v as $w => $x) {
					if (is_null($x)) {
						unset($v->$w);
					} else {
						$R_Assol_Tout_Couleur = new R_Assol_Tout_Couleur();
						$culture = $R_Assol_Tout_Couleur->first([
							'conditions' => [
								'IDA' => $x
							]
						]);
						$v->$w = $culture->Code_E;
					}
				}
				$v->An_Rec = $tmp;
			}
			usort($assolement, function($a, $b) {
				return ($a->An_Rec < $b->An_Rec);
			});

			$R_Parcelle_PF = new R_Parcelle_PF();
			$pfs = $R_Parcelle_PF->first([
				'fields' => 'IDPA, [1] as PF1, [2] as PF2', 
				'conditions' => [
					'IDPA' => $parcelle->IDPA
				]
			]);

			$AR_ADV_Final = new AR_ADV_Final();
			$ptAdvs = [];
			$adv = $AR_ADV_Final->find([
				'fields' => 'Lieu, GroupeADV, Couleur, ab1, ab2, ab3, ab4, ab5, ab6, ab7, ab8, ab9',
				'conditions' => [
					'IDO' => $args['ido'],
					'AnRec' => $args['annee'],
					'Lieu' => $args['ido'] == 3 ? $parcelle->Nom_Court : $pfs->PF1
				]
			]);
			if (!empty($adv)) $ptAdvs[$pfs->PF1] = $adv;

			$adv = $AR_ADV_Final->find([
				'fields' => 'Lieu, GroupeADV, Couleur, ab1, ab2, ab3, ab4, ab5, ab6, ab7, ab8, ab9',
				'conditions' => [
					'IDO' => $args['ido'],
					'AnRec' => $args['annee'],
					'Lieu' => $args['ido'] == 3 ? $parcelle->Nom_Court : $pfs->PF2
				]
			]);
			if (!empty($adv)) $ptAdvs[$pfs->PF2] = $adv;

			$ptsAdvs = [];
			$grpsPtsAdv = [''];
			foreach ($ptAdvs as $k => $v) {
				foreach ($v as $w => $x) {
					if (!in_array($x->GroupeADV, $grpsPtsAdv)) {
						$grpsPtsAdv[] = $x->GroupeADV;
					}

					$d = new \stdClass();
					$d->y = current(array_keys($grpsPtsAdv, $x->GroupeADV));
					$d->color = $x->Couleur;
					$d->GroupeADV = $x->GroupeADV;
					$d->x = 1;
					$d->radius = intval($x->ab1);
					$ptsAdvs[$x->Lieu][] = $d;

					$d = new \stdClass();
					$d->y = current(array_keys($grpsPtsAdv, $x->GroupeADV));
					$d->color = $x->Couleur;
					$d->GroupeADV = $x->GroupeADV;
					$d->x = 2;
					$d->radius = intval($x->ab2);
					$ptsAdvs[$x->Lieu][] = $d;

					$d = new \stdClass();
					$d->y = current(array_keys($grpsPtsAdv, $x->GroupeADV));
					$d->color = $x->Couleur;
					$d->GroupeADV = $x->GroupeADV;
					$d->x = 3;
					$d->radius = intval($x->ab3);
					$ptsAdvs[$x->Lieu][] = $d;

					$d = new \stdClass();
					$d->y = current(array_keys($grpsPtsAdv, $x->GroupeADV));
					$d->color = $x->Couleur;
					$d->GroupeADV = $x->GroupeADV;
					$d->x = 4;
					$d->radius = intval($x->ab4);
					$ptsAdvs[$x->Lieu][] = $d;

					$d = new \stdClass();
					$d->y = current(array_keys($grpsPtsAdv, $x->GroupeADV));
					$d->color = $x->Couleur;
					$d->GroupeADV = $x->GroupeADV;
					$d->x = 5;
					$d->radius = intval($x->ab5);
					$ptsAdvs[$x->Lieu][] = $d;

					$d = new \stdClass();
					$d->y = current(array_keys($grpsPtsAdv, $x->GroupeADV));
					$d->color = $x->Couleur;
					$d->GroupeADV = $x->GroupeADV;
					$d->x = 6;
					$d->radius = intval($x->ab6);
					$ptsAdvs[$x->Lieu][] = $d;

					$d = new \stdClass();
					$d->y = current(array_keys($grpsPtsAdv, $x->GroupeADV));
					$d->color = $x->Couleur;
					$d->GroupeADV = $x->GroupeADV;
					$d->x = 7;
					$d->radius = intval($x->ab7);
					$ptsAdvs[$x->Lieu][] = $d;

					$d = new \stdClass();
					$d->y = current(array_keys($grpsPtsAdv, $x->GroupeADV));
					$d->color = $x->Couleur;
					$d->GroupeADV = $x->GroupeADV;
					$d->x = 8;
					$d->radius = intval($x->ab8);
					$ptsAdvs[$x->Lieu][] = $d;

					$d = new \stdClass();
					$d->y = current(array_keys($grpsPtsAdv, $x->GroupeADV));
					$d->color = $x->Couleur;
					$d->GroupeADV = $x->GroupeADV;
					$d->x = 9;
					$d->radius = intval($x->ab9);
					$ptsAdvs[$x->Lieu][] = $d;
				}
			}

			$R_ADV_Group = new R_ADV_Group();
			$groupes = $R_ADV_Group->find([
				'fields' => 'GroupeADV'
			]);
			$grps = [];
			foreach ($groupes as $k => $v) {
				$grps[] = $v->GroupeADV;
			}

			$R_ADV_Groupe_IND = new R_ADV_Groupe_IND();
			$grpsDetails = [];
			foreach ($grps as $k => $v) {
				$groupes = $R_ADV_Groupe_IND->find([
					'conditions' => [
						'GroupeADV' => $v
					]
				]);

				foreach ($groupes as $w => $x) {
					$grpsDetails[$v][] = $x;
				}
			}

			return $this->returnJson($response, [
				'flores' => empty($flores) ? 'NOT_FOUND' : $flores,
				'datas' => empty($datas) ? 'NOT_FOUND' : $datas,
				'groupes' => empty($grps) ? 'NOT_FOUND' : $grps,
				'grpsDetails' => empty($grpsDetails) ? 'NOT_FOUND' : $grpsDetails,
				'years' => empty($years) ? 'NOT_FOUND' : $years,
				'adventices' => empty($advs) ? 'NOT_FOUND' : $advs,
				'assolement' => empty($assolement) ? 'NOT_FOUND' : $assolement,
				'pfs' => empty($ptsAdvs) ? 'NOT_FOUND' : array_keys($ptsAdvs),
				'ptsAdvs' => empty($ptsAdvs) ? 'NOT_FOUND' : $ptsAdvs,
				'grpsPtsAdv' => empty($grpsPtsAdv) ? 'NOT_FOUND' : $grpsPtsAdv
			]);
		} else {
			return $this->returnJson($response, []);
		}
	}

}