<?php

namespace inra\controllers;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use inra\models\R_Semis;
use inra\models\R_Assol_Tout_Couleur;

class SemisController extends BaseController {

	/*
	 * Methode de rendu jSON de donnees
	 * Permet de recuperer les semis pour une parcelle donnee
	 * @param RequestInterface $request Interface de requete
	 * @param ResponseInterface $response Interface de reponse
	 * @param $args arguments
	 */
	public function semis(RequestInterface $request, ResponseInterface $response, $args) {
		$R_Assol_Tout_Couleur = new R_Assol_Tout_Couleur();
		$data = $R_Assol_Tout_Couleur->find([
			'conditions' => [
				'IDPA' => $args['IDPA'],
				'An_Rec' => $args['annee']
			]
		]);
		
		if (!empty($data)) {
			if (count($data) > 1) {
				usort($data, function($a, $b) {
					return date('d/m/Y', strtotime($a->DateD)) < date('d/m/Y', strtotime($b->DateD));
				});
			}

			$start = current($data)->DateD;
			$end = current($data)->DateF;

			if (count($data) > 1) {
				$start = current($data)->DateD;
				$end = end($data)->DateF;
			}

			$R_Semis = new R_Semis();
			$semis = $R_Semis->find([
				'conditions' => [
					'IDPA' => $args['IDPA'],
					'btw' => [
						'DateI' => [
							'#' . date('Y-m-d', strtotime($start)) . '#', 
							'#' . (is_null($end) ? date('Y-m-d') : date('Y-m-d', strtotime($end))) . '#'
						]
					]
				]
			]);

			$especes = [];
			$dates = [""];
			$se = [];

			$dates[] = date('d/m/Y', strtotime($start));
			foreach ($semis as $k => $v) {
				if (!in_array(utf8_encode($v->Espece), $especes)) {
					$especes[] = utf8_encode($v->Espece);
				}

				$DateI = date('d/m/Y', strtotime($v->DateI));
				if (!in_array($DateI, $dates)) {
					$dates[] = $DateI;
				}

				if (in_array(utf8_encode($v->Espece), $especes)) {
					$v->x = current(array_keys($dates, $DateI));
					$v->y = current(array_keys($especes, utf8_encode($v->Espece))) + 1;
					$v->DateI = $DateI;
					$v->radius = 10;
					$v->color = 'blue';
					$se[] = $v;
				}
			}

			$dates[] = date('d/m/Y', strtotime($end));
			$dates[] = [""];

			return $this->returnJSON($response, [
				'dates' => $dates,
				'semis' => (!empty($se) ? $se : 'NOT_FOUND')
			]);
		}
		return $this->returnJSON($response, []);
	}

	public function details(RequestInterface $request, ResponseInterface $response, $args) {
		$R_Semis = new R_Semis();
		$semis = $R_Semis->find([
			'conditions' => [
				'IDPA' => $args['IDPA'],
				'DateI' => '%' . str_replace('_', '/', $args['date']) . '%'
			]
		]);

		foreach ($semis as $k => $v) {
			$v->Qte = round($v->Qte, 2);
			$v->DateI = date('d/m/Y', strtotime($v->DateI));
			$v->QteSurface = round(($v->Qte / $v->SW), 2);
		}

		return $this->returnJSON($response, $semis);
	}

}