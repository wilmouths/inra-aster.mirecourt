<?php

namespace inra\controllers;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use inra\models\R_Analyse_Sol;
use inra\models\R_PF_Parcelle;

/**
 * Controller des Points de ferti
 * @author WILMOUTH Steven
 */
class PointsFertiController extends BaseController {
   
    /*
     * Methode de rendu jSON de donnees
     * Permet de recuperer des informations sur un point de ferti donnee
     * @param RequestInterface $request Interface de requete
     * @param ResponseInterface $response Interface de reponse
     * @param $args arguments
     */
    public function infos(RequestInterface $request, ResponseInterface $response, $args) {
        $R_Analyse_Sol = new R_Analyse_Sol();
        $infos = $R_Analyse_Sol->find([
            'conditions' => [
                'PF' => $args['PF']
            ]
        ]);

        if (!empty($infos)) {
            $infosArray = [];
            foreach ($infos as $k => $v) {
                foreach ($v as $w => $x) {
                    $w = utf8_encode($w);

                    if (strstr(substr($w, -1), 'g')) {
                        $parts = explode('|', $w);
                        $w = $this->slugify($w);
                        $w = explode('_', $w);
                        $name = (count($w) >= 3) ? ($w[0] . '_' . $w[1] . '_' . $w[2]) : $w[0];
                        $infosArray[$k][$name]['name'] = trim(utf8_decode($parts[0]));
                        $infosArray[$k][$name]['unite'] = trim($parts[1]);
                        if ($name === 'ananaly') {
                            $infosArray[$k][$name]['value'] = (!is_null($x)) ? (is_numeric($x) ? floatval($x) : $x) : 0;
                        } else {
                            $infosArray[$k][$name]['value'] = (!is_null($x)) ? (is_numeric($x) ? floatval($x) : $x) : 0;
                        }
                    } else {
                        $fullName = $w;
                        $w = $this->slugify($w);
                        $w = explode('_', $w);
                        $name = (count($w) >= 3) ? ($w[0] . '_' . $w[1] . '_' . $w[2]) : $w[0];
                        $infosArray[$k][$name]['name'] = utf8_decode(trim(explode('|', $fullName)[0]));
                        if ($name === 'ananaly') {
                            $infosArray[$k][$name]['value'] = (!is_null($x)) ? (is_numeric($x) ? floatval($x) : $x) : 0;
                        } else {
                            $infosArray[$k][$name]['value'] = (!is_null($x)) ? (is_numeric($x) ? floatval($x) : $x) : 0;
                        }
                    }
                }
            }
            $infos = $infosArray;
        }
        return $this->returnJSON($response, $infos);
    }

    public function view(RequestInterface $request, ResponseInterface $response, $args) {
        $R_PF_Parcelle = new R_PF_Parcelle();
        $infos = $R_PF_Parcelle->first([
            'fields' => 'P_Ferti, Parcelle, T_S_descrip, Prof_H_Descrip, Substr_descrip',
            'conditions' => [
                'P_Ferti' => $args['PF']
            ]
        ]);
        return $this->returnJSON($response, $infos);
    }
    
}
