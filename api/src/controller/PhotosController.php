<?php

namespace inra\controllers;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use inra\models\R_Parcelles_Filiation;

/**
 * Controller de Filliations
 * @author WILMOUTH Steven
 */

class PhotosController extends BaseController {

    /*
     * Methode de rendu jSON de donnees
     * Permet de recuperer les photos d'une parcelle
     * @param RequestInterface $request Interface de requete
     * @param ResponseInterface $response Interface de reponse
     * @param $args arguments
     */
    public function photosWithType(RequestInterface $request, ResponseInterface $response, $args) {
        $photos = glob(DATA . DS . 'photos/' . $args['NOM_COURT'] . '_' . $args['type'] . '_' . $args['annee'] . '*.jpg');
        foreach ($photos as $k => $v) {
            $photos[$k] = explode('/', $v)[1];
        }
        return $this->returnJSON($response, $photos);
    }


    /*
     * Methode de rendu jSON de donnees
     * Permet de recuperer les photos d'une parcelle
     * @param RequestInterface $request Interface de requete
     * @param ResponseInterface $response Interface de reponse
     * @param $args arguments
     */
    public function photos(RequestInterface $request, ResponseInterface $response, $args) {
        $photosType = glob(DATA . DS . 'photos/' . $args['NOM_COURT'] . '_*_' . $args['annee'] . '*.jpg');
        foreach ($photosType as $k => $v) {
            $photosType[$k] = explode('/', $v)[1];
        }
        $photos = glob(DATA . DS . 'photos/' . $args['NOM_COURT'] . '_' . $args['annee'] . '*.jpg');
        foreach ($photos as $k => $v) {
            $photos[$k] = explode('/', $v)[1];
        }
        $photos = array_merge($photos, $photosType);
        return $this->returnJSON($response, $photos);
    }

}