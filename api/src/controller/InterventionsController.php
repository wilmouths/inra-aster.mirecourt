<?php

namespace inra\controllers;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use inra\models\R_ITK;
use inra\models\N_Parcelles;
use inra\models\R_Parcelles_Filiation;
use inra\models\R_Commentaire_ITK;
use inra\models\R_Assol_Tout_Couleur;

/**
 * Controller des Interventions
 * @author WILMOUTH Steven
 */
class InterventionsController extends BaseController {

	/*
	 * Methode de rendu jSON de donnees
	 * Permet de recuperer les interventions pour une parcelle et une annee donnee
	 * @param RequestInterface $request Interface de requete
	 * @param ResponseInterface $response Interface de reponse
	 * @param $args arguments
	 */
	public function interventions(RequestInterface $request, ResponseInterface $response, $args) {
		$route = $this->container->get('router')->pathFor(
			'sol.sol', [
				'NOM_COURT' => $args['nom_court'], 
				'annee' => (!empty($args['annee'])) ? $args['annee'] : date('Y')
			]
		);
		$data = json_decode(
			file_get_contents('http://' . current($request->getHeader('host')) . $route)
		);
		
		if (!is_null($data) && $data->DATA !== 'NOT_FOUND') {
			$date = $args['annee'];
			if (empty($args['annee'])) {
				$data = $data->DATA;

				unset($data[count($data) - 1]);

				$dates = [];
				foreach ($data as $k => $v) {
					$v = (array) $v;
					foreach ($v as $w => $x) {
						if ($w != 'IDPA' && $w != 'An_Rec') {
							if (!is_null($x)) {
								$route = $this->container->get('router')->pathFor(
									'sol.espece', [
										'IDPA' => $v['IDPA'], 
										'IDA' => $x,
										'annee' => $v['An_Rec']
									]
								);
								$res = current(
									json_decode(
										file_get_contents('http://' . current($request->getHeader('host')) . $route)
									)
								);
								if (!is_null($res->DateF)) {
									$dates[] = $res->DateF;
								}
							}
						}
					}
				}
				usort($dates, function($a, $b) {
					return (strtotime($b) - strtotime($a)); 
				});
				$date = current($dates);
			}

			$interventions = [];

			$R_ITK = new R_ITK();
			$interventions = $R_ITK->find([
				'conditions' => [
					'Nom_Court' => $args['nom_court'],
					'lteq' => [
						'DateI' => '#' . date('Y-m-d', strtotime($date)) . '#'
					]
				]
			]);
			foreach ($interventions as $k => $v) {
				if (intval(explode('-', $v->DateI)[0]) > $date) {
					unset($interventions[$k]);
				}
			}
			
			$N_Parcelles = new N_Parcelles();
			$parcelle = $N_Parcelles->first([
				'conditions' => [
					'Nom_Court' => $args['nom_court']
				]
			]);

			if (!is_null($parcelle)) {

				$R_Parcelles_Filiation = new R_Parcelles_Filiation();
				$filliation_P = $R_Parcelles_Filiation->first([
					'fields' => 'IDPA, IDPAP',
					'conditions' => [
						'IDPA' => $parcelle->IDPA,
						'An_Rec' => (!empty($args['annee'])) ? '%' . $args['annee'] : date('Y')
					]
				]);
				if (!empty($filliation_P)) {
					if (!is_null($filliation_P->IDPAP)) {
						$interventions_P = $R_ITK->find([
							'conditions' => [
								'IDPA' => $filliation_P->IDPAP,
								'gteq' => [
									'DateI' => '#' . date('Y-m-d', strtotime(
										date('Y-m-d', (strtotime($date))) . '-2000 day')
									) . '#', 
								],
								'lteq' => [
									'DateI' => '#' . date('Y-m-d', strtotime($date)) . '#'
								]
							]
						]);
						foreach ($interventions_P as $k => $v) {
							array_push($interventions, $interventions_P[$k]);
						}

						$intervention_GP = $R_Parcelles_Filiation->first([
							'fields' => 'IDPA, IDPA_GP',
							'conditions' => [
								'IDPA' => $parcelle->IDPA,
								'An_Rec' => (!empty($args['annee'])) ? '%' . $args['annee'] : date('Y')
							]
						]);
						if (!is_null($intervention_GP->IDPA_GP)) {
							$interventions_GP = $R_ITK->find([
								'conditions' => [
									'IDPA' => $intervention_GP->IDPA_GP,
									'gteq' => [
										'DateI' => '#' . date('Y-m-d', strtotime(
											date('Y-m-d', (strtotime($date))) . '-2000 day')
										) . '#', 
									],
									'lteq' => [
										'DateI' => '#' . date('Y-m-d', strtotime($date)) . '#'
									]
								]
							]);
							foreach ($interventions_GP as $k => $v) {
								array_push($interventions, $interventions_GP[$k]);
							}
						}
						$R_Commentaire_ITK = new R_Commentaire_ITK();
						foreach ($interventions as $key => $value) {
							$commentaire = $R_Commentaire_ITK->first([
								'fields' => 'IDITK, Rubrique, Commentaires',
								'conditions' => [
									'IDITK' => $value->IDITK
								] 
							]);
							if (!empty($commentaire)) {
								$interventions[$key]->commentaire = '[' . $commentaire->Rubrique . '] : ' . $commentaire->Commentaires;
							}
						}
					}
				} 
			}

			usort($interventions, function($a, $b) {
			   return (strtotime($b->DateI) - strtotime($a->DateI)); 
			});
		}

		return $this->returnJSON($response, $interventions);
	}

	/*
	 * Methode de rendu jSON de donnees
	 * Permet de recuperer le travail du sol pour une parcelle donnee
	 * @param RequestInterface $request Interface de requete
	 * @param ResponseInterface $response Interface de reponse
	 * @param $args arguments
	 */
	public function itk(RequestInterface $request, ResponseInterface $response, $args) {
		$R_Assol_Tout_Couleur = new R_Assol_Tout_Couleur();
		$data = $R_Assol_Tout_Couleur->find([
			'conditions' => [
				'IDPA' => $args['IDPA'],
				'An_Rec' => $args['annee']
			]
		]);

		usort($data, function($a, $b) {
			return (date('Y-m-d', strtotime($a->DateF)) > date('Y-m-d', strtotime($b->DateF)));
		});

		if (!empty($data)) {
			$start = current($data)->DateD;
			$end = end($data)->DateF;

			$R_ITK = new R_ITK();
			$itk = $R_ITK->find([
				'conditions' => [
					'IDPA' => $args['IDPA'],
					'Chantier' => 'WS%',
					'btw' => [
						'DateI' => [
							'#' . date('d-m-Y', strtotime($start)) . '#', 
							'#' . (is_null($end) ? date('d-m-Y') : date('d-m-Y', strtotime($end))) . '#'
						]
					]
				]
			]);

			$operations = [];
			$dates = [];
			$itks = [];

			$dates[] = date('Y-m-d', strtotime($start));
			foreach ($itk as $k => $v) {
				if (!in_array(utf8_encode($v->Nom_Operation), $operations)) {
					$operations[] = utf8_encode($v->Nom_Operation);
				}

				$DateI = date('Y-m-d', strtotime($v->DateI));
				if (!in_array($DateI, $dates)) {
					$dates[] = $DateI;
				}
			}

			usort($dates, function($a, $b) {
				return strtotime($a) - strtotime($b);
			});

			foreach ($itk as $k => $v) {
				if (in_array(utf8_encode($v->Nom_Operation), $operations)) {
					$v->x = current(array_keys($dates, $DateI));
					$v->y = current(array_keys($operations, utf8_encode($v->Nom_Operation))) + 1;
					$v->DateI = date('Y-m-d', strtotime($v->DateI));
					$v->radius = 10;
					$itks[] = $v;
				}
			}

			$dates[] = date('Y-m-d', strtotime($end));

			foreach ($dates as $k => $v) {
				$dates[$k] = date('d/m/Y', strtotime($v));
			}
			array_unshift($dates, '');
			$dates[] = '';
			
			$R_Commentaire_ITK = new R_Commentaire_ITK();
			foreach ($itks as $k => $v) {
				$commentaire = $R_Commentaire_ITK->first([
					'fields' => 'IDITK, Rubrique, Commentaires',
					'conditions' => [
						'IDITK' => $v->IDITK
					] 
				]);
				if (!empty($commentaire)) {
					$itks[$k]->Commentaire = '[' . $commentaire->Rubrique . '] : ' . $commentaire->Commentaires;
				}
			}

			return $this->returnJSON($response, [
				'dates' => $dates,
				'itks' => (!empty($itks) ? $itks : 'NOT_FOUND')
			]);
		} else {
			return $this->returnJSON($response, []);
		}
	}

	public function details(RequestInterface $request, ResponseInterface $response, $args) {
		$R_ITK = new R_ITK();
		$itks = $R_ITK->find([
			'conditions' => [
				'IDPA' => $args['IDPA'],
				'DateI' => '%' . str_replace('_', '/', $args['date']) . '%',
				'Chantier' => 'WS%'
			]
		]);

		foreach ($itks as $k => $v) {
			$v->DateI = date('d/m/Y', strtotime($v->DateI));
			$v->S_W = round($v->S_W, 2);
			$v->Surface_W = round($v->Surface_W, 2);
		}

		return $this->returnJSON($response, $itks);
	}

}
