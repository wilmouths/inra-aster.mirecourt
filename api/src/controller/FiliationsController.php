<?php

namespace inra\controllers;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use inra\models\R_Parcelles_Filiation;

/**
 * Controller de Filiations
 * @author WILMOUTH Steven
 */
class FiliationsController extends BaseController {
   
	/*
	 * Methode de rendu jSON de donnees
	 * Permet de recuperer les filiations pour une parcelle donnee
	 * @param RequestInterface $request Interface de requete
	 * @param ResponseInterface $response Interface de reponse
	 * @param $args arguments
	 */
	public function filiations(RequestInterface $request, ResponseInterface $response, $args) {
		$filiations = [];
		$R_Parcelles_Filiation = new R_Parcelles_Filiation();
		$filles = $R_Parcelles_Filiation->find([
			'fields' => 'IDPA, An_Rec, IDPAP, Parent, NC_P, SYST_P, DD_P, IDPA_GP, GParent, NC_GP, SYST_GP, DD_GP, IDPA_AGP, AGParent, NC_AGP, SYST_AGP, DD_AGP',
			'conditions' => [
				'Nom_Court' => $args['NOM_COURT'],
				'An_Rec' => (empty($args['annee']) ? date('Y') : $args['annee'])
			]
		]);

		if (!is_null($filles)) {
			$i = 0;
			foreach ($filles as $k => $v) {
				if (!is_null($v->IDPAP)) {
					$filiations[$i]['parent'] = [
						'IDPA' => $v->IDPA,
						'IDPAP' => $v->IDPAP,
						'Parent' => $v->Parent,
						'NC_P' => $v->NC_P,
						'SYST_P' => $v->SYST_P,
						'DateD' => date('d/m/Y', strtotime($v->DD_P))
					];
					if (!is_null($v->IDPA_GP)) {
						$filiations[$i]['parent']['gparent'] = [
							'IDP_GP' => $v->IDPA_GP,
							'GParent' => $v->GParent,
							'NC_GP' => $v->NC_GP,
							'SYST_GP' => $v->SYST_GP,
							'DateD' => date('d/m/Y', strtotime($v->DD_GP))
						];
					}
					if (!is_null($v->IDPA_AGP)) {
						$filiations[$i]['parent']['gparent']['agparent'] = [
							'IDP_AGP' => $v->IDPA_AGP,
							'AGParent' => $v->AGParent,
							'NC_AGP' => $v->NC_AGP,
							'SYST_AGP' => $v->SYST_AGP,
							'DateD' => date('d/m/Y', strtotime($v->DD_AGP))
						];
					}
				}
				$i++;
			}
		}
		return $this->returnJSON($response, $filiations);
	}
	
}
